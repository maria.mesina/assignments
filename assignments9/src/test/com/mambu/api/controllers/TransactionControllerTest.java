package com.mambu.api.controllers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mambu.api.domain.Account;
import com.mambu.api.domain.Transaction;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.Matchers.hasKey;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class TransactionControllerTest {
    ObjectMapper objectMapper = new ObjectMapper();
    Transaction transactionTest = new Transaction();

    @BeforeAll
    public static void setup() {
        RestAssured.baseURI = "http://localhost:8080/Gradle___assignments9_war";
    }

    @BeforeEach
    public void setupAccount(){
        transactionTest.setSourceId(1L);
        transactionTest.setDestinationId(2L);
        transactionTest.setAmount(500.0);
    }

    @Test
    public void testAddAccountReturnsId() throws JsonProcessingException {
        RestAssured
                .given()
                .contentType(ContentType.JSON)
                .body(objectMapper.writeValueAsString(transactionTest))

                .post("/transaction")

                .then()
                .statusCode(201)
                .body("$", hasKey("id"))
                .body("sourceId", equalTo(1))
                .body("destinationId", equalTo(2))
                .body("amount", equalTo(500.0f));

    }

    @Test
    public void testAddTransactionAndGet() throws JsonProcessingException {
        Response response = RestAssured
                .given()
                .contentType(ContentType.JSON)
                .body(objectMapper.writeValueAsString(transactionTest))

                .post("/transaction")
                .then().extract().response();

        String location = response.header("location");
        int id = Integer.parseInt(location.split("/transaction/", 2)[1]);
        RestAssured
                .given()
                .contentType(ContentType.JSON)

                .get(location)

                .then()
                .statusCode(200)
                .body("id", equalTo(id))
                .body("sourceId", equalTo(1))
                .body("destinationId", equalTo(2))
                .body("amount", equalTo(500.0f));
    }

    @Test
    public void testPostTransactionAndUpdate() throws JsonProcessingException {
        Response response = RestAssured
                .given()
                .contentType(ContentType.JSON)
                .body(objectMapper.writeValueAsString(transactionTest))

                .post("/transaction")
                .then().extract().response();

        String location = response.header("location");
        int id = Integer.parseInt(location.split("/transaction/", 2)[1]);

        transactionTest.setSourceId(2L);
        transactionTest.setDestinationId(1L);
        transactionTest.setAmount(200.0);

        RestAssured
                .given()
                .contentType(ContentType.JSON)
                .body(objectMapper.writeValueAsString(transactionTest))
                .put(location)

                .then()
                .statusCode(200)
                .body("id", equalTo(id))
                .body("sourceId", equalTo(2))
                .body("destinationId", equalTo(1))
                .body("amount", equalTo(200.0f));
    }

    @Test
    public void testPostAndDelete() throws JsonProcessingException {
        Response response = RestAssured
                .given()
                .contentType(ContentType.JSON)
                .body(objectMapper.writeValueAsString(transactionTest))

                .post("/transaction")
                .then().extract().response();

        String location = response.header("location");

        RestAssured
                .given()
                .contentType(ContentType.JSON)

                .delete(location)

                .then()
                .statusCode(204);
    }

    @Test
    public void testListAccountsAll() throws JsonProcessingException {
        RestAssured
                .given()
                .contentType(ContentType.JSON)
                .body(objectMapper.writeValueAsString(transactionTest))
                .post("/transaction");

        Transaction[] transactions = RestAssured
                .given()
                .get("/transaction/list")

                .then()
                .statusCode(200)
                .extract().as(Transaction[].class);

        assertTrue(transactions.length > 0);
    }

    @Test
    public void testListAccountsFilterBySource() throws JsonProcessingException {
        transactionTest.setSourceId(5L);
        RestAssured
                .given()
                .contentType(ContentType.JSON)
                .body(objectMapper.writeValueAsString(transactionTest))
                .post("/transaction");

        Transaction[] transactions = RestAssured
                .given()
                .param("source", 5)
                .get("/transaction/list")

                .then()
                .statusCode(200)
                .extract().as(Transaction[].class);

        assertEquals(1, transactions.length);
    }

    @Test
    public void testPutNonexistentId() throws JsonProcessingException {
        RestAssured
                .given()
                .contentType(ContentType.JSON)
                .body(objectMapper.writeValueAsString(transactionTest))
                .put("/transaction/9999")

                .then()
                .statusCode(404);
    }

    @Test
    public void testGetNonexistentId(){
        RestAssured
                .given()
                .get("/transaction/9999")

                .then()
                .statusCode(404);
    }

    @Test
    public void testDeleteNonexistentId(){
        RestAssured
                .given()
                .get("/transaction/9999")

                .then()
                .statusCode(404);
    }
}
