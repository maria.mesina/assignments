package com.mambu.api.controllers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mambu.api.domain.Account;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.Matchers.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class AccountControllerTest {
    ObjectMapper objectMapper = new ObjectMapper();
    Account accountTest = new Account();

    @BeforeAll
    public static void setup() {
        RestAssured.baseURI = "http://localhost:8080/Gradle___assignments9_war";
    }

    @BeforeEach
    public void setupAccount(){
        accountTest.setOwner("test");
        accountTest.setBalance(1000.0);
    }

    @Test
    public void testAddAccountReturnsId() throws JsonProcessingException {
        RestAssured
                .given()
                .contentType(ContentType.JSON)
                .body(objectMapper.writeValueAsString(accountTest))

                .post("/account")

                .then()
                .statusCode(201)
                .body("$", hasKey("id"))
                .body("owner", equalTo(accountTest.getOwner()))
                .body("balance", equalTo(1000.0f));
    }

    @Test
    public void testAddAccountAndGet() throws JsonProcessingException {
        Response response = RestAssured
                .given()
                .contentType(ContentType.JSON)
                .body(objectMapper.writeValueAsString(accountTest))

                .post("/account")
                .then().extract().response();

        String location = response.header("location");
        int id = Integer.parseInt(location.split("/account/", 2)[1]);
        RestAssured
                .given()
                .contentType(ContentType.JSON)

                .get(location)

                .then()
                .statusCode(200)
                .body("id", equalTo(id))
                .body("owner", equalTo(accountTest.getOwner()))
                .body("balance", equalTo(1000.0f));
    }

    @Test
    public void testPostAccountAndUpdate() throws JsonProcessingException {
        Response response = RestAssured
                .given()
                .contentType(ContentType.JSON)
                .body(objectMapper.writeValueAsString(accountTest))

                .post("/account")
                .then().extract().response();

        String location = response.header("location");
        int id = Integer.parseInt(location.split("/account/", 2)[1]);

        accountTest.setOwner("Maria");
        RestAssured
                .given()
                .contentType(ContentType.JSON)
                .body(objectMapper.writeValueAsString(accountTest))
                .put(location)

                .then()
                .statusCode(200)
                .body("id", equalTo(id))
                .body("owner", equalTo(accountTest.getOwner()))
                .body("balance", equalTo(1000.0f));
    }

    @Test
    public void testPostAndDelete() throws JsonProcessingException {
        Response response = RestAssured
                .given()
                .contentType(ContentType.JSON)
                .body(objectMapper.writeValueAsString(accountTest))

                .post("/account")
                .then().extract().response();

        String location = response.header("location");

        RestAssured
                .given()
                .contentType(ContentType.JSON)

                .delete(location)

                .then()
                .statusCode(204);
    }

    @Test
    public void testListAccountsAll() throws JsonProcessingException {
        RestAssured
                .given()
                .contentType(ContentType.JSON)
                .body(objectMapper.writeValueAsString(accountTest))
                .post("/account");

        Account[] accounts = RestAssured
                .given()
                .get("/account/list")

                .then()
                .statusCode(200)
                .extract().as(Account[].class);

        assertTrue(accounts.length > 0);
    }

    @Test
    public void testListAccountsFilterByOwner() throws JsonProcessingException {
        accountTest.setOwner("test3");
        RestAssured
                .given()
                .contentType(ContentType.JSON)
                .body(objectMapper.writeValueAsString(accountTest))
                .post("/account");

        Account[] accounts = RestAssured
                .given()
                .param("owner", "test3")
                .get("/account/list")

                .then()
                .statusCode(200)
                .extract().as(Account[].class);

        assertEquals(1, accounts.length);
    }

    @Test
    public void testPutNonexistentId() throws JsonProcessingException {
        RestAssured
                .given()
                .contentType(ContentType.JSON)
                .body(objectMapper.writeValueAsString(accountTest))
                .put("/account/9999")

                .then()
                .statusCode(404);
    }

    @Test
    public void testGetNonexistentId(){
        RestAssured
                .given()
                .get("/account/9999")

                .then()
                .statusCode(404);
    }

    @Test
    public void testDeleteNonexistentId(){
        RestAssured
                .given()
                .get("/account/9999")

                .then()
                .statusCode(404);
    }
}
