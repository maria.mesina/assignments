package com.mambu.api.repositories;

import com.mambu.api.domain.Transaction;

import java.util.ArrayList;
import java.util.List;

public class TransactionRepository {
    private static final List<Transaction> transactions = new ArrayList<>();
    private static long nextId = 1;

    public Transaction add(Transaction transactionToAdd){
        transactionToAdd.setId(nextId);
        nextId++;
        transactions.add(transactionToAdd);

        return transactionToAdd;
    }

    public Transaction get(Long transactionId){
        Transaction transactionToReturn = transactions.stream()
                .filter(transaction -> transaction.getId().equals(transactionId))
                .findFirst().get();

        return transactionToReturn;
    }

    public Transaction update(Transaction newData){
        Transaction transactionToUpdate = transactions.stream()
                .filter(transaction -> transaction.getId().equals(newData.getId()))
                .findFirst().get();

        if(newData.getSourceId() != null){
            transactionToUpdate.setSourceId(newData.getSourceId());
        }
        if(newData.getDestinationId() != null){
            transactionToUpdate.setDestinationId(newData.getDestinationId());
        }

        if(newData.getAmount() != null){
            transactionToUpdate.setAmount(newData.getAmount());
        }

        return transactionToUpdate;
    }

    public boolean delete(Long accountId){
        Transaction transactionToDelete = transactions.stream()
                .filter(account -> account.getId().equals(accountId))
                .findFirst().orElse(null);
        if(transactionToDelete == null){
            return false;
        }

        return transactions.remove(transactionToDelete);
    }

    public List<Transaction> getTransactions() {
        return transactions;
    }
}
