package com.mambu.api.repositories;

import com.mambu.api.domain.Account;

import java.util.ArrayList;
import java.util.List;

public class AccountRepository {
    private static final List<Account> accounts = new ArrayList<>();
    private static long nextId = 1;

    public Account add(Account accountToAdd){
        accountToAdd.setId(nextId);
        nextId++;
        accounts.add(accountToAdd);

        return accountToAdd;
    }

    public Account get(Long accountId){
        Account accountToReturn = accounts.stream().filter(account -> account.getId().equals(accountId))
                .findFirst().get();

        return accountToReturn;
    }

    public Account update(Account newData){
        Account accountToUpdate = accounts.stream().filter(account -> account.getId().equals(newData.getId()))
                .findFirst().get();
        if(newData.getBalance() != null) {
            accountToUpdate.setBalance(newData.getBalance());
        }
        if(newData.getOwner() != null) {
            accountToUpdate.setOwner(newData.getOwner());
        }

        return accountToUpdate;
    }

    public boolean delete(Long accountId){
        Account accountToDelete = accounts.stream().filter(account -> account.getId().equals(accountId))
                .findFirst().orElse(null);
        if(accountToDelete == null){
            return false;
        }

        return accounts.remove(accountToDelete);
    }

    public List<Account> getAccounts(){
        return accounts;
    }
}
