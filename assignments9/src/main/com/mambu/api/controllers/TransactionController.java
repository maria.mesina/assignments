package com.mambu.api.controllers;

import com.mambu.api.domain.Transaction;
import com.mambu.api.services.queries.TransactionQuery;
import com.mambu.api.services.implementations.TransactionServiceImpl;
import com.mambu.api.services.interfaces.TransactionService;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.net.URI;
import java.util.List;
import java.util.NoSuchElementException;

@Path("/transaction")
public class TransactionController {
    private final TransactionService transactionService = new TransactionServiceImpl();

    @GET
    @Path("/{transactionId}")
    @Produces({MediaType.APPLICATION_JSON})
    public Response get(@PathParam("transactionId") Long transactionId){
        try{
            Transaction transactionToReturn = transactionService.getTransaction(transactionId);
            return Response.ok(transactionToReturn).build();
        } catch (NoSuchElementException e){
            return Response.status(404).build();
        }
    }

    @POST
    @Path("/")
    @Produces({MediaType.APPLICATION_JSON})
    public Response add(Transaction transactionToAdd){
        Transaction addedTransaction = transactionService.addTransaction(transactionToAdd);
        String location = "transaction/" + addedTransaction.getId().toString();

        return Response.ok(addedTransaction).status(201).location(URI.create(location)).build();
    }

    @PUT
    @Path("/{transactionId}")
    @Produces({MediaType.APPLICATION_JSON})
    public Response update(@PathParam("transactionId") Long transactionId, Transaction newData){
        newData.setId(transactionId);
        try{
            Transaction updatedTransaction = transactionService.updateTransaction(newData);
            return Response.ok(updatedTransaction).build();
        } catch (NoSuchElementException e){
            return Response.status(404).build();
        }
    }

    @DELETE
    @Path("/{transactionId}")
    @Produces({MediaType.APPLICATION_JSON})
    public Response delete(@PathParam("transactionId") Long transactionId){
        transactionService.deleteTransaction(transactionId);
        return Response.noContent().build();
    }

    @GET
    @Path("/list")
    @Produces({MediaType.APPLICATION_JSON})
    public Response getTransactions(@QueryParam("source") Long sourceId, @QueryParam("destination") Long destinationId) {
        TransactionQuery query = new TransactionQuery(sourceId, destinationId);
        List<Transaction> transactions = transactionService.queryTransactions(query);

        return Response.ok(transactions).build();
    }

}
