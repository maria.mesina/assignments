package com.mambu.api.controllers;

import com.mambu.api.domain.Account;
import com.mambu.api.services.queries.AccountQuery;
import com.mambu.api.services.implementations.AccountServiceImpl;
import com.mambu.api.services.interfaces.AccountService;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.net.URI;
import java.util.List;
import java.util.NoSuchElementException;

@Path("/account")
public class AccountController {
    private final AccountService accountService = new AccountServiceImpl();

    @GET
    @Path("/list")
    @Produces({MediaType.APPLICATION_JSON})
    public Response getAccounts(@QueryParam("owner") String owner, @QueryParam("balance") Double balance,
                                @QueryParam("lessThan") Boolean lessThan){
        AccountQuery query = new AccountQuery(owner, balance, lessThan);
        List<Account> accounts = accountService.queryAccounts(query);

        return Response.ok(accounts).build();
    }

    @GET
    @Path("/{accountId}")
    @Produces({MediaType.APPLICATION_JSON})
    public Response getAccount(@PathParam("accountId") Long accountId){
        try{
            Account accountToReturn = accountService.getAccount(accountId);
            return Response.ok(accountToReturn).build();
        } catch(NoSuchElementException e) {
            return Response.status(404).build();
        }
    }

    @POST
    @Path("/")
    @Produces({MediaType.APPLICATION_JSON})
    public Response addAccount(Account accountToAdd){
        Account addedAccount = accountService.addAccount(accountToAdd);
        String location = "account/" + addedAccount.getId().toString();
        return Response.ok(addedAccount).status(201).location(URI.create(location)).build();
    }

    @PUT
    @Path("/{accountId}")
    @Produces({MediaType.APPLICATION_JSON})
    public Response updateAccount(@PathParam("accountId") Long accountId, Account newData){
        newData.setId(accountId);
        try{
            Account updatedAccount = accountService.updateAccount(newData);
            return Response.ok(updatedAccount).build();
        } catch(NoSuchElementException e){
            return Response.status(404).build();
        }

    }

    @DELETE
    @Path("/{accountId}")
    @Produces({MediaType.APPLICATION_JSON})
    public Response deleteAccount(@PathParam("accountId") Long accountId){
        accountService.deleteAccount(accountId);
        return Response.noContent().build();
    }
}
