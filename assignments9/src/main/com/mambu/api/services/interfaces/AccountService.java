package com.mambu.api.services.interfaces;

import com.mambu.api.domain.Account;
import com.mambu.api.services.queries.AccountQuery;

import java.util.List;

public interface AccountService {
    Account getAccount(Long accountId);
    Account addAccount(Account accountToAdd);
    Account updateAccount(Account newData);
    boolean deleteAccount(Long accountId);
    List<Account> getAccounts();
    List<Account> queryAccounts(AccountQuery query);
}
