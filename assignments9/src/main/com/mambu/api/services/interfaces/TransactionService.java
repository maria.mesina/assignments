package com.mambu.api.services.interfaces;

import com.mambu.api.domain.Transaction;
import com.mambu.api.services.queries.TransactionQuery;

import java.util.List;

public interface TransactionService {
    Transaction getTransaction(Long transactionId);
    Transaction addTransaction(Transaction transactionToAdd);
    Transaction updateTransaction(Transaction newData);
    boolean deleteTransaction(Long transactionId);
    List<Transaction> getTransactions();
    List<Transaction> queryTransactions(TransactionQuery query);
}
