package com.mambu.api.services.queries;

public class AccountQuery {
    public String owner;
    public Double balance;
    public Boolean lessThan;

    public AccountQuery(String owner, Double balance, Boolean lessThan){
        this.owner = owner;
        this.balance = balance;
        this.lessThan = lessThan;
    }
}
