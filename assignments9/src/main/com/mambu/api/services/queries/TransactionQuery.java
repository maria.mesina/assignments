package com.mambu.api.services.queries;

public class TransactionQuery {
    public Long sourceId;
    public Long destinationId;

    public TransactionQuery(Long sourceId, Long destinationId) {
        this.sourceId = sourceId;
        this.destinationId = destinationId;
    }
}
