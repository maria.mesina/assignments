package com.mambu.api.services.implementations;

import com.mambu.api.domain.Transaction;
import com.mambu.api.services.queries.TransactionQuery;
import com.mambu.api.repositories.TransactionRepository;
import com.mambu.api.services.interfaces.TransactionService;

import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class TransactionServiceImpl implements TransactionService {
    private final TransactionRepository transactionRepository = new TransactionRepository();

    @Override
    public Transaction getTransaction(Long transactionId) {
        return transactionRepository.get(transactionId);
    }

    @Override
    public Transaction addTransaction(Transaction transactionToAdd) {
        return transactionRepository.add(transactionToAdd);
    }

    @Override
    public Transaction updateTransaction(Transaction newData) {
        return transactionRepository.update(newData);
    }

    @Override
    public boolean deleteTransaction(Long transactionId) {
        return transactionRepository.delete(transactionId);
    }

    @Override
    public List<Transaction> getTransactions() {
        return transactionRepository.getTransactions();
    }

    @Override
    public List<Transaction> queryTransactions(TransactionQuery query) {
        List<Transaction> transactions = transactionRepository.getTransactions();
        if(query.sourceId != null){
            transactions = filterTransactions(transactions,
                    (transaction -> transaction.getSourceId().equals(query.sourceId)));
        }

        if(query.destinationId != null){
            transactions = filterTransactions(transactions,
                    (transaction)->transaction.getDestinationId().equals(query.destinationId));
        }

        return transactions;
    }

    private List<Transaction> filterTransactions(List<Transaction> transactions, Predicate<Transaction> filter) {
        return transactions.stream().filter(filter).collect(Collectors.toList());
    }
}
