package com.mambu.api.services.implementations;

import com.mambu.api.domain.Account;
import com.mambu.api.services.queries.AccountQuery;
import com.mambu.api.repositories.AccountRepository;
import com.mambu.api.services.interfaces.AccountService;

import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class AccountServiceImpl implements AccountService {
    private final AccountRepository accountRepository = new AccountRepository();

    @Override
    public Account getAccount(Long accountId) {
        return accountRepository.get(accountId);
    }

    @Override
    public Account addAccount(Account accountToAdd) {
        return accountRepository.add(accountToAdd);
    }

    @Override
    public Account updateAccount(Account newData) {
        return accountRepository.update(newData);
    }

    @Override
    public boolean deleteAccount(Long accountId) {
        return accountRepository.delete(accountId);
    }

    @Override
    public List<Account> getAccounts() {
        return accountRepository.getAccounts();
    }

    @Override
    public List<Account> queryAccounts(AccountQuery query) {
        List<Account> accounts = getAccounts();
        if(query.owner != null){
            accounts = filterAccounts(accounts, (account)->account.getOwner().equals(query.owner));
        }

        if(query.balance != null){
            if(query.lessThan){
                accounts = filterAccounts(accounts, (account)->account.getBalance().compareTo(query.balance) < 0);
            }
            else{
                accounts = filterAccounts(accounts, (account)->account.getBalance().compareTo(query.balance) > 0);
            }
        }

        return accounts;
    }

    private List<Account> filterAccounts(List<Account> accounts, Predicate<Account> filter) {
        return accounts.stream().filter(filter).collect(Collectors.toList());
    }
}
