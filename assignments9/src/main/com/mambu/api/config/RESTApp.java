package com.mambu.api.config;

import org.glassfish.jersey.server.ResourceConfig;

public class RESTApp extends ResourceConfig {

    public RESTApp() {
        packages("io.learnstuff.api");
    }
}