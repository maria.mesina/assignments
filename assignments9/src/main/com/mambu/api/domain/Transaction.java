package com.mambu.api.domain;

import java.util.Date;
import java.util.Objects;

public class Transaction {
    private Long id;
    private Long sourceId;
    private Long destinationId;
    private Double amount;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getSourceId() {
        return sourceId;
    }

    public void setSourceId(Long sourceId) {
        this.sourceId = sourceId;
    }

    public Long getDestinationId() {
        return destinationId;
    }

    public void setDestinationId(Long destinationId) {
        this.destinationId = destinationId;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Transaction that = (Transaction) o;
        return sourceId.equals(that.sourceId) && destinationId.equals(that.destinationId) &&
                amount.equals(that.amount);
    }

    @Override
    public int hashCode() {
        return Objects.hash(sourceId, destinationId, amount);
    }
}
