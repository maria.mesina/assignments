-- Alter all tables and add creation_date and created_by columns
ALTER TABLE sbs_albums
ADD COLUMN creation_date DATE,
ADD COLUMN created_by VARCHAR(100);

ALTER TABLE sbs_artist_genres
ADD COLUMN creation_date DATE,
ADD COLUMN created_by VARCHAR(100);

ALTER TABLE sbs_artists
ADD COLUMN creation_date DATE,
ADD COLUMN created_by VARCHAR(100);

ALTER TABLE sbs_genres
ADD COLUMN creation_date DATE,
ADD COLUMN created_by VARCHAR(100);

-- Add a text column to SBS_ALBUMS that represents sold_amount
ALTER TABLE sbs_albums
ADD COLUMN sold_amount VARCHAR(10);

-- Insert at least 1 value in each table, but preferably: 2 artists, 4 genres, assign genres to artists making sure each artist has
-- at least 2 genres, 2 albums for each artist

-- genres
INSERT INTO test.sbs_genres(
	name, description, artist_origin, cultural_origin, first_appeared_in, creation_date, created_by)
	VALUES ('Reggaeton', 'Typically in Spanish.', 
			'Daddy Yankee, Daddy Yankee and Don Omar', 'Puerto Rico', '1995-01-01', curdate(), "db_admin"),
            ('Hip hop', 'A stylized rhythmic music that commonly accompanies rapping.', 
			'Vanilla Ice, Dr. Dre and Snoop Dogg', 'New York City, U.S', '1973-01-01', curdate(), "db_admin"),
            ('Chillhop', 'Combines elements of hip hop and chill-out music.', 
			'Nujabes, J Dilla', 'Japan', '2010-01-01', curdate(), "db_admin"),
            ('K-pop', 'Short for Korean popular music.', 
			'BTS, Blackpink, Psy', 'South Korea', '2005-01-01', curdate(), "db_admin");
            
-- artists
INSERT INTO test.sbs_artists(
	name, origin, details, launch_date, creation_date, created_by)
	VALUES ('BTS', 'Seoul, South Korea', 'Still Active', '2013-01-01', curdate(), "db_admin"),
		   ('Daddy Yankee', 'San Juan, Puerto Rico', 'Singer, songwriter, rapper', '2004-01-01', curdate(), "db_admin");
           
-- artist genres
INSERT INTO test.sbs_artist_genres(
	id_artist, id_genre, creation_date, created_by)
	VALUES ((SELECT id FROM sbs_artists WHERE name like 'BTS'), 
			(SELECT id FROM sbs_genres WHERE name like 'K-pop'), curdate(), "db_admin"),
            ((SELECT id FROM sbs_artists WHERE name like 'BTS'),
            (SELECT id FROM sbs_genres WHERE name like 'Hip hop'), curdate(), "db_admin");
            
INSERT INTO test.sbs_artist_genres(
	id_artist, id_genre, creation_date, created_by)
	VALUES ((SELECT id FROM sbs_artists WHERE name like 'Daddy Yankee'), 
			(SELECT id FROM sbs_genres WHERE name like 'Reggaeton'), curdate(), "db_admin"),
            ((SELECT id FROM sbs_artists WHERE name like 'Daddy Yankee'),
            (SELECT id FROM sbs_genres WHERE name like 'Hip hop'), curdate(), "db_admin");
            
-- albums
INSERT INTO test.sbs_albums(
	title, details, release_date, sold_amount, id_artist, creation_date, created_by)
	VALUES ('Love Yourself: Tear', 'Third Korean studio album (sixth overall)', '2018-05-18', '1500000',
			(SELECT id FROM sbs_artists WHERE name like 'BTS'), curdate(), "db_admin"),
			('Wings', 'Second Korean studio album (fourth overall)', '2016-10-10', '3000000', 
            (SELECT id FROM sbs_artists WHERE name like 'BTS'), curdate(), "db_admin");

INSERT INTO test.sbs_albums(
	title, details, release_date,sold_amount, id_artist, creation_date, created_by)
	VALUES ('Barrio Fino', 'Seventh best selling Latin albums in the United States', '2004-07-12', '5000000', 
			(SELECT id FROM sbs_artists WHERE name like 'Daddy Yankee'), curdate(), "db_admin"),
			('El Cartel: The Big Boss', 'Fourth studio album and eighth overall', '2007-06-05', '1000000', 
            (SELECT id FROM sbs_artists WHERE name like 'Daddy Yankee'), curdate(), "db_admin");
		
-- After you have inserted values for all columns change the type of sold_amount from text to integer;
ALTER TABLE sbs_albums
MODIFY sold_amount INTEGER;

-- Select all artists
SELECT name, origin, details
FROM sbs_artists;

-- Select an artist by id
SELECT name, origin, details
FROM sbs_artists
WHERE id = 3;

-- Select an artist by id and name
SELECT name, origin, details
FROM sbs_artists
WHERE id = 3 and name like 'BTS';

-- Select all albums for an artist based on artist id
SELECT title, details
FROM sbs_albums
WHERE id_artist = (SELECT id FROM sbs_artists WHERE name like 'BTS');

-- Count all albums for an artist based on artist id
SELECT count(*)
FROM sbs_albums
WHERE id_artist = (SELECT id FROM sbs_artists WHERE name like 'BTS');

-- Count all genres for an artist based on artist id
SELECT count(*)
FROM sbs_artist_genres
WHERE id_artist = (SELECT id FROM sbs_artists WHERE name like 'BTS');

-- Sum all sold amounts for each artist id
SELECT sum(sold_amount)
FROM sbs_albums
GROUP BY id_artist;

-- Select artists having sales more than 100
SELECT sum(sold_amount)
FROM sbs_albums
GROUP BY id_artist
HAVING sum(sold_amount) > 100;

-- Select artists sorted by album sales
SELECT name, origin, details
FROM sbs_artists
LEFT JOIN (SELECT id_artist, sum(sold_amount) AS album_sales FROM sbs_albums GROUP BY id_artist) AS temp
ON sbs_artists.id = temp.id_artist
ORDER BY temp.album_sales;

-- Select artists with launch_date between two given dates
SELECT name, origin, details
FROM sbs_artists
WHERE launch_date BETWEEN '1990-01-01' AND '2000-01-01';

-- Select artists with launch_date before a given date
SELECT name, origin, details
FROM sbs_artists
WHERE launch_date < '2010-01-01';

-- Using union - Select all artists with albums and all artists that do not have albums
SELECT sbs_artists.name, sbs_artists.origin, sbs_artists.details
FROM sbs_artists
INNER JOIN sbs_albums
ON sbs_albums.id_artist = sbs_artists.id
UNION
SELECT sbs_artists.name, sbs_artists.origin, sbs_artists.details
FROM sbs_artists
LEFT JOIN sbs_albums
ON sbs_albums.id_artist = sbs_artists.id;

-- Select all artists for a specific genre based on genre name
SELECT sbs_artists.name, sbs_artists.origin, sbs_artists.details
FROM sbs_artists
INNER JOIN sbs_artist_genres
ON sbs_artists.id = sbs_artist_genres.id_artist
INNER JOIN sbs_genres
ON sbs_genres.id = sbs_artist_genres.id_genre
WHERE sbs_genres.name like 'K-pop';

SELECT sbs_artists.name, sbs_artists.origin, sbs_artists.details
FROM sbs_artists, sbs_artist_genres, sbs_genres
WHERE sbs_artists.id = sbs_artist_genres.id_artist
	AND sbs_genres.id = sbs_artist_genres.id_genre
	AND sbs_genres.name like 'K-pop';
    
-- Select all albums for an artist based on artist name
SELECT title, sbs_albums.details, release_date, sold_amount
FROM sbs_albums
INNER JOIN sbs_artists
ON sbs_albums.id_artist = sbs_artists.id
WHERE sbs_artists.name like 'Metallica';

-- Select all albums for each artist
SELECT sbs_artists.name, group_concat(sbs_albums.title) AS "albums" 
FROM sbs_albums
INNER JOIN sbs_artists
ON sbs_albums.id_artist = sbs_artists.id
GROUP BY sbs_artists.id;

-- Count all albums for each artist
SELECT sbs_artists.name, count(*) AS "albums_count"
FROM sbs_albums
INNER JOIN sbs_artists
ON sbs_albums.id_artist = sbs_artists.id
GROUP BY sbs_artists.id;

-- Count all genres for each artist
SELECT sbs_artists.name, count(*) AS "genres_count"
FROM sbs_artists
INNER JOIN sbs_artist_genres
ON sbs_artist_genres.id_artist = sbs_artists.id
GROUP BY sbs_artists.id;

-- Sum all sold amounts for each artist id
SELECT sbs_artists.name, sum(sold_amount) AS "total_amount_sold"
FROM sbs_albums
INNER JOIN sbs_artists
ON sbs_albums.id_artist = sbs_artists.id
GROUP BY sbs_artists.id;

-- Select artists having sales more than 100
SELECT sbs_artists.name, sum(sold_amount) AS "total_amount_sold"
FROM sbs_albums
INNER JOIN sbs_artists
ON sbs_albums.id_artist = sbs_artists.id
GROUP BY sbs_artists.id
HAVING total_amount_sold > 100;

-- Select artists sorted by album sales
SELECT sbs_artists.name, sum(sold_amount) AS "total_amount_sold"
FROM sbs_albums
INNER JOIN sbs_artists
ON sbs_albums.id_artist = sbs_artists.id
GROUP BY sbs_artists.id
ORDER BY total_amount_sold;

-- Select artists, albums and genres with artists launch_date between two given dates
SELECT sbs_artists.name, group_concat(DISTINCT sbs_genres.name), group_concat(DISTINCT sbs_albums.title)
FROM sbs_artists
INNER JOIN sbs_albums
ON sbs_artists.id = sbs_albums.id_artist
INNER JOIN sbs_artist_genres
ON sbs_artist_genres.id_artist = sbs_artists.id
INNER JOIN sbs_genres
ON sbs_genres.id = sbs_artist_genres.id_genre
WHERE sbs_artists.launch_date BETWEEN '2000-01-01' AND '2020-01-01'
GROUP BY sbs_artists.name;

SELECT sbs_artists.name, group_concat(DISTINCT sbs_genres.name), group_concat(DISTINCT sbs_albums.title)
FROM sbs_artists, sbs_albums, sbs_genres, sbs_artist_genres
WHERE sbs_artists.id = sbs_albums.id_artist
	AND sbs_artists.id = sbs_artist_genres.id_artist
    AND sbs_genres.id = sbs_artist_genres.id_genre
	AND sbs_artists.launch_date BETWEEN '1990-01-01' AND '2020-01-01'
GROUP BY sbs_artists.name;

SELECT sbs_artists.name, a_g.genres, a_a.albums
FROM sbs_artists, 
	(SELECT id_artist, group_concat(sbs_genres.name) AS "genres" 
	FROM sbs_genres, sbs_artist_genres
    WHERE sbs_genres.id = sbs_artist_genres.id_artist
    GROUP BY sbs_artist_genres.id_artist) a_g,
    (SELECT id_artist, group_concat(sbs_albums.title) AS "albums" 
	FROM sbs_albums
    GROUP BY sbs_albums.id_artist) a_a
WHERE sbs_artists.id = a_g.id_artist
	AND sbs_artists.id = a_a.id_artist
    AND sbs_artists.launch_date BETWEEN '1990-01-01' AND '2020-01-01';
    
-- Create a view for the previous query
CREATE VIEW artists_info AS
	SELECT sbs_artists.name, group_concat(DISTINCT sbs_genres.name), group_concat(DISTINCT sbs_albums.title)
	FROM sbs_artists, sbs_albums, sbs_genres, sbs_artist_genres
	WHERE sbs_artists.id = sbs_albums.id_artist
		AND sbs_artists.id = sbs_artist_genres.id_artist
		AND sbs_genres.id = sbs_artist_genres.id_genre
		AND sbs_artists.launch_date BETWEEN '1990-01-01' AND '2020-01-01'
	GROUP BY sbs_artists.name;


