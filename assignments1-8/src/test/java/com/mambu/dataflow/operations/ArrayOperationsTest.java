package com.mambu.dataflow.operations;

import org.junit.Test;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assumptions.*;
import java.io.*;
import java.util.Arrays;

public class ArrayOperationsTest {
    private final ArrayOperations arrayOperations = new ArrayOperations();
    private final int[] array = {2, 4, 1, 6};

    @Test
    public void testReadArraySuccess() throws IOException {
        String path = "testReadArraySuccess.txt";
        final File f = new File(path);

        String arrayString = arrayToString(array, " ");
        writeToFile(path, arrayString);

        int[] readArray = arrayOperations.readArray(path);
        if (f.exists()){
            f.delete();
        }
        assertArrayEquals(array, readArray);
    }

    @Test
    public void testReadArrayFileNotExists(){
        String path = "nonexistent-path.txt";

        FileNotFoundException exception = assertThrows(FileNotFoundException.class,
                ()-> arrayOperations.readArray(path));

        assertNotNull(exception);
    }

    @Test
    public void testReadArrayInvalidDelimiter() throws IOException {
        String path = "testReadArrayInvalidDelimiter.txt";
        final File f = new File(path);

        String arrayString = arrayToString(array, ", ");
        writeToFile(path, arrayString);

        NumberFormatException exception = assertThrows(NumberFormatException.class,
                ()-> arrayOperations.readArray(path));

        if (f.exists()){
            f.delete();
        }
        assertNotNull(exception);
    }

    @Test
    public void testReadArrayMoreThanSizeValues() throws IOException {
        String path = "testReadArrayMoreThanSizeValues.txt";
        final File f = new File(path);


        int[] subarray = Arrays.copyOfRange(array, 0, array.length - 2);

        String arrayString = arrayToString(array, " ");
        arrayString = arrayString.replace(array.length + "\n", subarray.length+ "\n");
        writeToFile(path, arrayString);

        assumeTrue(f.exists());
        int[] readArray = arrayOperations.readArray(path);

        if (f.exists()){
            f.delete();
        }
        assertArrayEquals(subarray, readArray);
    }

    @Test
    public void testReadArrayLessThanSizeValues() throws  IOException{
        String path = "testReadArrayLessThanSizeValues.txt";
        final File f = new File(path);

        int[] expandedArray = Arrays.copyOf(array, array.length + 2);
        String arrayString = arrayToString(array, " ");
        arrayString = arrayString.replace(array.length + "\n", expandedArray.length+ "\n");
        writeToFile(path, arrayString);

        int[] readArray = arrayOperations.readArray(path);

        if (f.exists()){
            f.delete();
        }
        assertArrayEquals(expandedArray, readArray);
    }

    @Test
    public void testReadArrayNoSizeLine() throws IOException{
        String path = "testReadArrayNoSizeLine.txt";
        final File f = new File(path);

        String arrayString = arrayToString(array, " ");
        arrayString = arrayString.replace(array.length + "\n", "");
        writeToFile(path, arrayString);

        NumberFormatException exception = assertThrows(NumberFormatException.class,
                ()-> arrayOperations.readArray(path));

        if (f.exists()){
            f.delete();
        }
        assertNotNull(exception);
    }

    @Test
    public void testReadArrayNoValuesLine() throws IOException{
        String path = "testReadArrayNoValuesLine.txt";
        final File f = new File(path);

        String arrayString = arrayToString(array, " ");
        arrayString = arrayString.substring(0, arrayString.indexOf('\n'));
        writeToFile(path, arrayString);

        NullPointerException exception = assertThrows(NullPointerException.class,
                ()-> arrayOperations.readArray(path));

        if (f.exists()){
            f.delete();
        }
        assertNotNull(exception);
    }

    @Test
    public void testWriteArraySuccess() throws IOException {
        String path = "testWriteArraySuccess.txt";
        int[] reversedArray = reverseArray(array);

        arrayOperations.writeArray(path, array);

        String readArrayString = readFromFile(path);
        int[] readReversed = stringToArray(readArrayString, " ");

        final File f = new File(path);
        if (f.exists()){
            f.delete();
        }
        assertArrayEquals(reversedArray, readReversed);
    }

    @Test
    public void testWriteArrayNullInput() {
        String path = "testWriteArrayNullInput.txt";
        int[] array = null;

        NullPointerException exception = assertThrows(NullPointerException.class,
                ()-> arrayOperations.writeArray(path, array));

        final File f = new File(path);
        if (f.exists()){
            f.delete();
        }

        assertNotNull(exception);
    }

    private String arrayToString(int[] array, String delimiter){
        StringBuilder arrayString = new StringBuilder(array.length + "\n");

        for(int i=0; i<array.length - 1; ++i){
            arrayString.append(array[i]).append(delimiter);
        }

        arrayString.append(array[array.length - 1]);
        return arrayString.toString();
    }

    private void writeToFile(String path, String data) throws IOException {
        FileWriter fileWriter = new FileWriter(path);
        BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);

        bufferedWriter.write(data);
        bufferedWriter.flush();

        bufferedWriter.close();
        fileWriter.close();
    }

    private String readFromFile(String path) throws IOException {
        StringBuilder data = new StringBuilder();
        FileReader fileReader = new FileReader(path);
        BufferedReader bufferedReader = new BufferedReader(fileReader);

        String line;

        while((line = bufferedReader.readLine()) != null){
            data.append(line).append('\n');
        }

        data.deleteCharAt(data.length() - 1);
        return data.toString();
    }

    private int[] stringToArray(String arrayString, String delimiter){
        String[] lines = arrayString.split("\n");
        int size = Integer.parseInt(lines[0]);
        int[] array = new int[size];

        String[] values = lines[1].split(delimiter);
        for(int i=0; i<array.length; ++i){
            array[i] = Integer.parseInt(values[i]);
        }

        return array;
    }

    private int[] reverseArray(int[] array){
        int[] reversed = new int[array.length];

        for(int i=0; i<array.length; ++i){
            reversed[i] = array[array.length - 1 - i];
        }

        return reversed;
    }
}
