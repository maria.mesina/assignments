package com.mambu.dataflow.operations;

import org.junit.Test;

import java.io.File;
import java.io.IOException;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assumptions.*;

public class DirectoryOperationsTest {
    private final DirectoryOperations dop = new DirectoryOperations();

    @Test
    public void testCountFilesWithExtensionsSuccess() throws IOException {
        String directoryPath = "./src/test/java/com/mambu/dataflow/operations/testDirSuccess";
        File directory = new File(directoryPath);

        String[] extensions = {"png", "pdf"};
        int[] extensionsCount = {10, 3};
        File[] files = createDirectoryAndFiles(directoryPath, extensions, extensionsCount);

        int[] actualFilesCount = dop.countFilesWithExtension(directory, extensions);

        deleteDirectoryAndFiles(directory);

        assertEquals(extensionsCount[0], actualFilesCount[0]);
        assertEquals(extensionsCount[1], actualFilesCount[1]);
    }

    @Test
    public void testCountFilesWithExtensionsWithSubdirectoriesSuccess() throws IOException {
        String directoryPath = "./src/test/java/com/mambu/dataflow/operations/testDirWithSubdirectories";
        File directory = new File(directoryPath);

        String[] extensions = {"png", "pdf"};
        int[] extensionsCount = {10, 3};
        createDirectoryAndFiles(directoryPath, extensions, extensionsCount);

        String subdirectoryPath = directoryPath + "/png";
        createDirectoryAndFiles(subdirectoryPath, new String[]{"png"}, new int[]{1});

        subdirectoryPath = directoryPath + "/pdf";
        createDirectoryAndFiles(subdirectoryPath, new String[]{"pdf"}, new int[]{1});

        int[] actualFilesCount = dop.countFilesWithExtension(directory, extensions);

        deleteDirectoryAndFiles(directory);

        assertEquals(11, actualFilesCount[0]);
        assertEquals(4, actualFilesCount[1]);

    }

    @Test
    public void testCountFilesFolderWithNontargetExtensions() throws IOException {
        String directoryPath = "./src/test/java/com/mambu/dataflow/operations/testDirWithNontargetExtensions";
        File directory = new File(directoryPath);

        String[] extensions = {"png", "pdf", "txt", "mp3"};
        int[] extensionsCount = {10, 3, 2, 4};
        createDirectoryAndFiles(directoryPath, extensions, extensionsCount);

        String subdirectoryPath = directoryPath + "/png";
        createDirectoryAndFiles(subdirectoryPath, new String[]{"png"}, new int[]{1});

        subdirectoryPath = directoryPath + "/pdf";
        createDirectoryAndFiles(subdirectoryPath, new String[]{"pdf"}, new int[]{1});

        subdirectoryPath = directoryPath + "/txt";
        createDirectoryAndFiles(subdirectoryPath, new String[]{"txt"}, new int[]{1});

        extensions = new String[]{"png", "pdf"};
        int[] actualFilesCount = dop.countFilesWithExtension(directory, extensions);

        deleteDirectoryAndFiles(directory);

        assertEquals(11, actualFilesCount[0]);
        assertEquals(4, actualFilesCount[1]);
    }

    @Test
    public void testCountFilesEmptyFolder(){
        String directoryPath = "./src/test/java/com/mambu/dataflow/operations/testDirEmpty";
        File directory = new File(directoryPath);
        directory.mkdir();

        String[] extensions = {"png", "pdf"};
        int[] actualFilesCount = dop.countFilesWithExtension(directory, extensions);

        deleteDirectoryAndFiles(directory);
        assertEquals(0, actualFilesCount[0]);
        assertEquals(0, actualFilesCount[1]);
    }

    @Test
    public void testCountFilesNullFolder(){
        NullPointerException exception = assertThrows(NullPointerException.class,
                ()-> dop.countFilesWithExtension(null, new String[]{"pdf", "png"}));

        assertNotNull(exception);
    }

    @Test
    public void testCountFilesNullExtensions(){
        String directoryPath = "./src/test/java/com/mambu/dataflow/operations/testDir";
        File directory = new File(directoryPath);
        directory.mkdir();

        NullPointerException exception = assertThrows(NullPointerException.class,
                ()-> dop.countFilesWithExtension(directory, null));

        deleteDirectoryAndFiles(directory);
        assertNotNull(exception);
    }

    @Test
    public void testCountEmptyExtensions(){
        String directoryPath = "./src/test/java/com/mambu/dataflow/operations/testDirEmpty";
        File directory = new File(directoryPath);
        directory.mkdir();

        String[] extensions = {};
        int[] actualFilesCount = dop.countFilesWithExtension(directory, extensions);

        deleteDirectoryAndFiles(directory);
        assertEquals(0, actualFilesCount.length);
    }

    @Test
    public void testCountNonexistentFolder(){
        String directoryPath = "./src/test/java/com/mambu/dataflow/operations/testNonexistentFolder";
        File directory = new File(directoryPath);

        String[] extensions = {"png", "pdf"};
        NullPointerException exception = assertThrows(NullPointerException.class,
                () -> dop.countFilesWithExtension(directory, extensions));

        assertNotNull(exception);
    }

    private File[] createDirectoryAndFiles(String directoryPath, String[] extensions, int[] extensionsCount) throws IOException {
        File directory = new File(directoryPath);
        directory.mkdir();

        int totalCount = 0;
        for(int i=0; i<extensionsCount.length; ++i){
            totalCount += extensionsCount[i];
        }

        File[] files = new File[totalCount];

        int totalIndex = 0;
        for(int i=0; i<extensions.length; ++i){
            for(int j=0; j<extensionsCount[i]; ++j){
                File f = new File(directoryPath + "/" + j + "." + extensions[i]);
                f.createNewFile();
                files[totalIndex] = f;

                assumeTrue(f.exists());
                totalIndex++;
            }
        }

        return files;
    }

    private void deleteDirectoryAndFiles(File directory){
        if (directory.isDirectory()) {
            for (File c : directory.listFiles())
                deleteDirectoryAndFiles(c);
        }
        directory.delete();
    }
}