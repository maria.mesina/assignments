package com.mambu.dataflow.serialization.model;

import com.mambu.dataflow.serialization.model.User;

import java.util.Arrays;

public class UserModelTestData {
    public static User anUser(){
        User user = new User();
        user.setId(1);
        user.setUsername("test");
        user.setRoles(new String[]{"guest", "user"});
        return user;
    }

    public static User anUserWithoutRoles(){
        User user = new User();
        user.setId(1);
        user.setUsername("test");
        return user;
    }

    public static boolean areEquals(User user1, User user2){
        if (user1.getId() == user2.getId()){
            if ( (user1.getUsername() != null && user2.getUsername() != null &&
                    user1.getUsername().equals(user2.getUsername())) ||
                    (user1.getUsername() == null && user2.getUsername() == null)){
                return Arrays.equals(user1.getRoles(), user2.getRoles());
            }
        }

        return false;
    }
}
