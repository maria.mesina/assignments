package com.mambu.dataflow.serialization.implementations;

import com.fasterxml.jackson.databind.ObjectMapper;
import static org.junit.jupiter.api.Assertions.*;

import com.mambu.dataflow.serialization.InvalidInputException;
import com.mambu.dataflow.serialization.model.User;
import com.mambu.dataflow.serialization.model.UserModelTestData;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;

import java.io.*;

@TestInstance(Lifecycle.PER_CLASS)
public class JsonSerializerTest {
    private JsonSerializer jsonSerializer;
    private ObjectMapper objectMapper;
    private final String path = "./src/test/java/com/mambu/dataflow/serialization/implementations";

    @BeforeAll
    public void setup(){
        jsonSerializer = new JsonSerializer();
        objectMapper = new ObjectMapper();
    }

    @Test
    public void testSerializeSuccess() throws IOException {
        File file = new File(path + "/user.json");
        User user = UserModelTestData.anUser();
        jsonSerializer.serialize(user, file);

        User readUser = objectMapper.readValue(file, User.class);

        file.delete();
        assertTrue(UserModelTestData.areEquals(user, readUser));
    }

    @Test
    public void testSerializeNull() {
        File file = new File(path + "/nullUser.json");

        User user = null;
        NullPointerException exception = assertThrows(NullPointerException.class,
                ()->jsonSerializer.serialize(user, file));

        file.delete();
        assertNotNull(exception);
    }

    @Test
    public void testSerializeUserWithoutRoles() throws IOException {
        File file = new File(path + "/user.json");
        User user = UserModelTestData.anUserWithoutRoles();
        jsonSerializer.serialize(user, file);

        User readUser = objectMapper.readValue(file, User.class);

        file.delete();
        assertTrue(UserModelTestData.areEquals(user, readUser));
    }

    @Test
    public void testDeserializeSuccess() throws IOException {
        File file = new File(path + "/user.json");
        User user = UserModelTestData.anUser();
        objectMapper.writeValue(file, user);

        User readUser = jsonSerializer.deserialize(file);
        file.delete();
        assertTrue(UserModelTestData.areEquals(user, readUser));
    }

    @Test
    public void testDeserializeInvalidInputException() throws IOException {
        File file = new File(path +"/invalid.json");
        int[] array = {3, 4, 1, 5};
        objectMapper.writeValue(file, array);

        InvalidInputException exception = assertThrows(InvalidInputException.class,
                ()->jsonSerializer.deserialize(file));

        file.delete();
        assertNotNull(exception);
    }

    @Test
    public void testDeserializeNonexistentFile() {
        File file = new File(path +"/nonexistent.json");

        FileNotFoundException exception = assertThrows(FileNotFoundException.class,
                ()->jsonSerializer.deserialize(file));

        assertNotNull(exception);
    }

    @Test
    public void testDeserializeNull() {
        NullPointerException exception = assertThrows(NullPointerException.class,
                ()->jsonSerializer.deserialize(null));

        assertNotNull(exception);
    }
}
