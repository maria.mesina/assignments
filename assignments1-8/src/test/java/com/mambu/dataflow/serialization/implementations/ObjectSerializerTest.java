package com.mambu.dataflow.serialization.implementations;

import com.mambu.dataflow.serialization.InvalidInputException;
import com.mambu.dataflow.serialization.model.User;
import com.mambu.dataflow.serialization.model.UserModelTestData;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import java.io.*;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class ObjectSerializerTest {
    private ObjectSerializer objectSerializer;
    private final String path = "./src/test/java/com/mambu/dataflow/serialization/implementations";

    @BeforeAll
    public void setup(){
        objectSerializer = new ObjectSerializer();
    }

    @Test
    public void testSerializeSuccess() throws IOException, ClassNotFoundException {
        File file = new File(path + "/user.obj");
        User user = UserModelTestData.anUser();

        objectSerializer.serialize(user, file);

        User readUser = readUserFromFile(file);
        file.delete();
        assertTrue(UserModelTestData.areEquals(user, readUser));
    }

    @Test
    public void testSerializeUserWithoutRoles() throws IOException, ClassNotFoundException {
        File file = new File(path + "/userWithoutRoles.obj");
        User user = UserModelTestData.anUserWithoutRoles();
        objectSerializer.serialize(user, file);

        User readUser = readUserFromFile(file);

        file.delete();
        assertTrue(UserModelTestData.areEquals(user, readUser));
    }

    @Test
    public void testSerializeNull() {
        File file = new File(path + "/nullUser.obj");

        User user = null;
        NullPointerException exception = assertThrows(NullPointerException.class,
                ()->objectSerializer.serialize(user, file));

        file.delete();
        assertNotNull(exception);
    }

    @Test
    public void testDeserializeSuccess() throws IOException {
        File file = new File(path + "/user.obj");
        User user = UserModelTestData.anUser();
        writeToFile(file, user);

        User readUser = objectSerializer.deserialize(file);

        file.delete();
        assertTrue(UserModelTestData.areEquals(user, readUser));
    }

    @Test
    public void testDeserializeInvalidInputException() throws IOException {
        File file = new File(path +"/invalid.obj");
        int[] array = {3, 4, 5, 6};
        writeToFile(file, array);

        InvalidInputException exception = assertThrows(InvalidInputException.class,
                ()->objectSerializer.deserialize(file));

        file.delete();
        assertNotNull(exception);
    }

    @Test
    public void testDeserializeNonexistentFile() {
        File file = new File(path +"/nonexistent.obj");

        FileNotFoundException exception = assertThrows(FileNotFoundException.class,
                ()->objectSerializer.deserialize(file));

        assertNotNull(exception);
    }

    @Test
    public void testDeserializeNull() {
        NullPointerException exception = assertThrows(NullPointerException.class,
                ()->objectSerializer.deserialize(null));

        assertNotNull(exception);
    }

    private User readUserFromFile(File file) throws IOException, ClassNotFoundException {
        FileInputStream fis = new FileInputStream(file);
        ObjectInputStream ois = new ObjectInputStream(fis);
        User readUser = (User) ois.readObject();

        ois.close();
        fis.close();

        return readUser;
    }

    private void writeToFile(File file, Object user) throws IOException {
        FileOutputStream fos = new FileOutputStream(file);
        ObjectOutputStream oos = new ObjectOutputStream(fos);
        oos.writeObject(user);
        oos.flush();
        oos.close();
        fos.close();
    }
}
