package com.mambu.dataflow.serialization.implementations;

import com.mambu.dataflow.serialization.InvalidInputException;
import com.mambu.dataflow.serialization.model.User;
import com.mambu.dataflow.serialization.model.UserModelTestData;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.*;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@TestInstance(Lifecycle.PER_CLASS)
public class XmlSerializerTest {
    private XmlSerializer xmlSerializer;
    private JAXBContext context;
    private Unmarshaller unmarshaller;
    private Marshaller marshaller;
    private final String path = "./src/test/java/com/mambu/dataflow/serialization/implementations";

    @BeforeAll
    public void setup() throws JAXBException {
        xmlSerializer = new XmlSerializer();
        context = JAXBContext.newInstance(User.class);
        unmarshaller = context.createUnmarshaller();
        marshaller = context.createMarshaller();
    }

    @Test
    public void testSerializeSuccess() throws JAXBException {
        File file = new File(path + "/user.xml");
        User user = UserModelTestData.anUser();
        xmlSerializer.serialize(user, file);

        User readUser = (User) unmarshaller.unmarshal(file);

        file.delete();
        assertTrue(UserModelTestData.areEquals(user, readUser));
    }

    @Test
    public void testSerializeNull() {
        File file = new File(path + "/nullUser.xml");

        User user = null;
        NullPointerException exception = assertThrows(NullPointerException.class,
                ()->xmlSerializer.serialize(user, file));

        file.delete();
        assertNotNull(exception);
    }

    @Test
    public void testSerializeUserWithoutRoles() throws JAXBException {
        File file = new File(path + "/userWithoutRoles.xml");
        User user = UserModelTestData.anUserWithoutRoles();
        xmlSerializer.serialize(user, file);

        User readUser = (User) unmarshaller.unmarshal(file);

        file.delete();
        assertTrue(UserModelTestData.areEquals(user, readUser));
    }

    @Test
    public void testDeserializeSuccess() throws JAXBException {
        File file = new File(path + "/user.xml");
        User user = UserModelTestData.anUser();
        marshaller.marshal(user, file);

        User readUser = xmlSerializer.deserialize(file);

        file.delete();
        assertTrue(UserModelTestData.areEquals(user, readUser));
    }

    @Test
    public void testDeserializeInvalidInputException() throws IOException {
        File file = new File(path +"/invalid.json");
        FileWriter fileWriter = new FileWriter(file);
        BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
        String xmlString = "<text>Not a valid string </text>";
        bufferedWriter.write(xmlString);

        InvalidInputException exception = assertThrows(InvalidInputException.class,
                ()->xmlSerializer.deserialize(file));

        file.delete();
        assertNotNull(exception);
    }

    @Test
    public void testDeserializeNonexistentFile() {
        File file = new File(path +"/nonexistent.xml");

        IllegalArgumentException exception = assertThrows(IllegalArgumentException.class,
                ()->xmlSerializer.deserialize(file));

        assertNotNull(exception);
    }

    @Test
    public void testDeserializeNull() {
        NullPointerException exception = assertThrows(NullPointerException.class,
                ()->xmlSerializer.deserialize(null));

        assertNotNull(exception);
    }
}
