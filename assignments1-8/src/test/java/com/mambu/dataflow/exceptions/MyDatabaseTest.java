package com.mambu.dataflow.exceptions;

import com.mambu.dataflow.exceptions.Connection;
import com.mambu.dataflow.exceptions.InvalidConnectionString;
import com.mambu.dataflow.exceptions.MyDatabase;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assumptions.assumeTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
public class MyDatabaseTest {
    MyDatabase database;

    @Mock
    Connection connection;

    @BeforeEach
    public void setup(){
        connection = mock(Connection.class);
        database = new MyDatabase(connection);
    }

    @Test
    public void testConnectSuccess(){
        when(connection.getConnectionString()).thenReturn("localhost\":\"15536\"/\"test");

        database.connect();

        assertTrue(database.isAlive());
    }

    @Test
    public void testConnectInvalidConnection(){
        when(connection.getConnectionString()).thenThrow(InvalidConnectionString.class);

        InvalidConnectionString exception = assertThrows(InvalidConnectionString.class,
                ()->database.connect());

        assertNotNull(exception);
    }

    @Test
    public void testExecuteWhenIsNotAlive(){
        IllegalStateException exception = assertThrows(IllegalStateException.class,
                ()->database.execute("select * from users"));

        assertNotNull(exception);
    }

    @Test
    public void testCloseSuccess(){
        when(connection.getConnectionString()).thenReturn("localhost\":\"15536\"/\"test");
        database.connect();
        assumeTrue(database.isAlive());

        database.close();

        assertFalse(database.isAlive());
    }

    @Test
    public void testCloseWhenIsNotAlive(){
        IllegalStateException exception = assertThrows(IllegalStateException.class,
                ()->database.close());

        assertNotNull(exception);
    }
}
