package com.mambu.dataflow.exceptions.model;

import com.mambu.dataflow.exceptions.Connection;

public class ConnectionTestData {

    public static Connection aValidConnection(){
        Connection c = new Connection();
        c.setPort(134);
        c.setHost("localhost");
        c.setName("myDatabase");

        c.setUsername("test");
        c.setPassword("12345678");

        return c;
    }

    public static Connection aConnectionWithoutPassword(){
        Connection c = new Connection();
        c.setPort(134);
        c.setHost("localhost");
        c.setName("myDatabase");

        c.setUsername("test");

        return c;
    }
}
