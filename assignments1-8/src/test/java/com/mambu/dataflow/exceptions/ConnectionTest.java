package com.mambu.dataflow.exceptions;

import com.mambu.dataflow.exceptions.model.ConnectionTestData;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class ConnectionTest {
    @Test
    public void testGetConnectionStringSuccess(){
        Connection c = ConnectionTestData.aValidConnection();
        String connectionString = c.getConnectionString();

        assertNotNull(connectionString);
        assertTrue(connectionString.length() > 0);
    }

    @Test
    public void testGetConnectionStringNoPassword(){
        Connection c = ConnectionTestData.aConnectionWithoutPassword();

        InvalidConnectionString exception = assertThrows(InvalidConnectionString.class,
                ()->c.getConnectionString());

        assertNotNull(exception);
    }

    @Test
    public void testGetConnectionStringSetPort(){
        Connection c = ConnectionTestData.aValidConnection();

        InvalidConnectionString exception = assertThrows(InvalidConnectionString.class,
                ()->c.setPort(-1));

        assertNotNull(exception);
    }
}
