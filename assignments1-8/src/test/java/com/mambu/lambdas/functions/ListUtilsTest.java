package com.mambu.lambdas.functions;

import com.mambu.lambdas.model.Product;
import com.mambu.lambdas.model.ProductTestData;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class ListUtilsTest {
    private ListUtils listUtils = new ListUtils();

    @Test
    public void testGetMaxEmptyList(){
        List<Integer> list = new LinkedList<>();

        int max = listUtils.getMax(list);

        assertEquals(0, max);
    }

    @Test
    public void testGetMaxOneElement(){
        int element = 3;
        List<Integer> list = Arrays.asList(element);

        int max = listUtils.getMax(list);

        assertEquals(element, max);
    }

    @Test
    public void testGetMaxMultipleElements(){
        int size = 10;
        List<Integer> list = new ArrayList<>();
        for(int i=0; i<size; ++i) {
            list.add(i);
        }

        int max = listUtils.getMax(list);

        assertEquals(size-1, max);
    }

    @Test
    public void testGetMinEmptyList(){
        List<Integer> list = new LinkedList<>();

        int min = listUtils.getMin(list);

        assertEquals(0, min);
    }

    @Test
    public void testGetMinOneElement(){
        int element = 3;
        List<Integer> list = Arrays.asList(element);

        int min = listUtils.getMin(list);

        assertEquals(element, min);
    }

    @Test
    public void testGetMinMultipleElements(){
        int size = 10;
        List<Integer> list = new ArrayList<>();
        for(int i=0; i<size; ++i) {
            list.add(-i);
        }

        int min = listUtils.getMin(list);

        assertEquals(-size+1, min);
    }

    @Test
    public void testGetAvgEmptyList(){
        List<Integer> list = new LinkedList<>();

        double avg = listUtils.getAvg(list);

        assertEquals(0, avg);
    }

    @Test
    public void testGetAvgOneElement(){
        int element = 3;
        List<Integer> list = Arrays.asList(element);

        double avg = listUtils.getAvg(list);

        assertEquals(element, avg);
    }

    @Test
    public void testGetAvgMultipleElements(){
        int size = 10;
        int sum = 0;
        List<Integer> list = new ArrayList<>();
        for(int i=0; i<size; ++i) {
            list.add(i);
            sum += i;
        }

        double avg = listUtils.getAvg(list);

        assertEquals((double) sum / size , avg);
    }

    @Test
    public void testIterateForEachIterable(){
        int size = 5;
        List<Product> products = ProductTestData.anList(size);

        String representation = listUtils.iterateForEachIterable(products);

        assertTrue(checkCorrectStringRepresentation(products, representation));
    }

    @Test
    public void testIterateForEachIterableEmptyList(){
        List<Product> products = new LinkedList<>();

        String representation = listUtils.iterateForEachIterable(products);

        assertTrue(checkCorrectStringRepresentation(products, representation));
    }

    @Test
    public void testIterateForEachStream(){
        int size = 5;
        List<Product> products = ProductTestData.anList(size);

        String representation = listUtils.iterateForEachStream(products);

        assertTrue(checkCorrectStringRepresentation(products, representation));
    }

    @Test
    public void testIterateForEachStreamEmptyList(){
        List<Product> products = new LinkedList<>();

        String representation = listUtils.iterateForEachStream(products);

        assertTrue(checkCorrectStringRepresentation(products, representation));
    }

    @Test
    public void testIterateReduce(){
        int size = 5;
        List<Product> products = ProductTestData.anList(size);

        String representation = listUtils.iterateReduce(products);

        assertTrue(checkCorrectStringRepresentation(products, representation));
    }

    @Test
    public void testIterateReduceEmptyList(){
        List<Product> products = new LinkedList<>();

        String representation = listUtils.iterateReduce(products);

        assertTrue(checkCorrectStringRepresentation(products, representation));
    }

    @Test
    public void testIterateCollectors(){
        int size = 5;
        List<Product> products = ProductTestData.anList(size);

        String representation = listUtils.iterateCollectors(products);

        assertTrue(checkCorrectStringRepresentation(products, representation));
    }

    @Test
    public void testIterateCollectorsEmptyList(){
        List<Product> products = new LinkedList<>();

        String representation = listUtils.iterateCollectors(products);

        assertTrue(checkCorrectStringRepresentation(products, representation));
    }

    private boolean checkCorrectStringRepresentation(List<Product> products, String representation) {
        if(products.isEmpty() && representation.isEmpty()){
            return true;
        }

        String[] lines = representation.split("\n");
        if (products.size() != lines.length){
            return false;
        }

        for(int i=0; i<lines.length; ++i){
            if(!(lines[i].equals(products.get(i).toString()))) {
                return false;
            }
        }

        return true;
    }
}
