package com.mambu.lambdas.functions;

import com.mambu.lambdas.model.Transaction;
import com.mambu.lambdas.model.TransactionTestData;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

import java.time.YearMonth;
import java.util.*;

public class TransactionUtilsTest {
    private TransactionUtils transactionUtils = new TransactionUtils();

    @Test
    public void getAverageMultipleTransactions(){
        List<Transaction> transactions =
                TransactionTestData.aListOfTransactions(3, 3,2, YearMonth.of(2021, 7));
        double expectedAvg = Math.round(getAverageOfTransactions(transactions)*100)/100.0;

        double actualAvg = Math.round(transactionUtils.getAverage(transactions)*100)/100.0;

        assertEquals(expectedAvg, actualAvg);
    }

    @Test
    public void getAverageOneTransaction(){
        List<Transaction> transactions =
                TransactionTestData.aListOfTransactions(1, 1,3, YearMonth.of(2021, 7));
        double expectedAvg = Math.round(getAverageOfTransactions(transactions)*100)/100.0;

        double actualAvg = Math.round(transactionUtils.getAverage(transactions)*100)/100.0;

        assertEquals(expectedAvg, actualAvg);
    }

    @Test
    public void getAverageEmptyList(){
        List<Transaction> transactions = new LinkedList<>();

        double actualAvg = transactionUtils.getAverage(transactions);

        assertEquals(0, actualAvg);
    }

    @Test
    public void getTop3ByAmountMultipleTransactions(){
        List<Transaction> transactions =
                TransactionTestData.aListOfTransactions(4, 3, 2, YearMonth.of(2021, 7));
        Collections.sort(transactions);
        Collections.reverse(transactions);

        transactions = transactions.subList(0, 3);

        List<Transaction> top3 = transactionUtils.getTop3ByAmount(transactions);
        assertEquals(transactions, top3);
    }

    @Test
    public void getTop3ByAmountLessThan3(){
        List<Transaction> transactions =
                TransactionTestData.aListOfTransactions(2, 1,5, YearMonth.of(2021, 7));
        Collections.sort(transactions);
        Collections.reverse(transactions);

        List<Transaction> top = transactionUtils.getTop3ByAmount(transactions);
        assertEquals(transactions, top);
    }

    @Test
    public void getTop3ByAMountEmptyList(){
        List<Transaction> transactions = new LinkedList<>();
        List<Transaction> top = transactionUtils.getTop3ByAmount(transactions);
        assertTrue(top.isEmpty());
    }

    @Test
    public void getAverageOfMonthOneMonth(){
        YearMonth ym = YearMonth.of(2021, 7);
        List<Transaction> transactions =
                TransactionTestData.aListOfTransactions(2, 1,5, ym);
        double expectedAvg = Math.round(getAverageOfTransactions(transactions)*100)/100.0;
        double actualAvg = Math.round(transactionUtils.getAverageOfMonth(transactions, ym)*100)/100.0;

        assertEquals(expectedAvg, actualAvg);
    }

    @Test
    public void getAverageOfMonthMultipleMonths(){
        YearMonth ym = YearMonth.of(2021, 7);
        List<Transaction> transactions =
                TransactionTestData.aListOfTransactions(2, 1,5, ym);
        double expectedAvg = Math.round(getAverageOfTransactions(transactions)*100)/100.0;
        transactions.addAll(TransactionTestData.aListOfTransactions(2, 1,5, ym.plusMonths(1)));

        double actualAvg = Math.round(transactionUtils.getAverageOfMonth(transactions, ym)*100)/100.0;

        assertEquals(expectedAvg, actualAvg);
    }

    @Test
    public void groupTransactionsOneTransactionPerUser(){
        List<Transaction> transactions =
                TransactionTestData.aListOfTransactions(2, 1,5, YearMonth.of(2021, 7));

        Map<String, List<String>> dict = transactionUtils.groupTransactions(transactions);

        assertTrue(dict.containsKey(String.valueOf(transactions.get(0).getUserId())));
        List<String> expectedIds = Arrays.asList(String.valueOf(transactions.get(0).getId()));
        List<String> actualIds = dict.get(String.valueOf(transactions.get(0).getUserId()));
        assertEquals(expectedIds, actualIds);

        assertTrue(dict.containsKey(String.valueOf(transactions.get(1).getUserId())));
        expectedIds = Arrays.asList(String.valueOf(transactions.get(1).getId()));
        actualIds = dict.get(String.valueOf(transactions.get(1).getUserId()));
        assertEquals(expectedIds, actualIds);
    }

    @Test
    public void groupTransactionsMultipleTransactionsPerUser(){
        List<Transaction> transactions =
                TransactionTestData.aListOfTransactions(2, 2,5, YearMonth.of(2021, 7));

        Map<String, List<String>> dict = transactionUtils.groupTransactions(transactions);

        assertTrue(dict.containsKey(String.valueOf(transactions.get(0).getUserId())));
        List<String> expectedIds =
                Arrays.asList(String.valueOf(transactions.get(0).getId()), String.valueOf(transactions.get(1).getId()));
        List<String> actualIds = dict.get(String.valueOf(transactions.get(0).getUserId()));
        assertEquals(expectedIds, actualIds);

        assertTrue(dict.containsKey(String.valueOf(transactions.get(2).getUserId())));
        expectedIds =
                Arrays.asList(String.valueOf(transactions.get(2).getId()), String.valueOf(transactions.get(3).getId()));
        actualIds = dict.get(String.valueOf(transactions.get(2).getUserId()));
        assertEquals(expectedIds, actualIds);
    }

    @Test
    public void  groupTransactionsEmptyList(){
        List<Transaction> transactions = new LinkedList<>();
        Map<String, List<String>> dict = transactionUtils.groupTransactions(transactions);
        assertTrue(dict.isEmpty());
    }

    private double getAverageOfTransactions(List<Transaction> transactions){
        double sum = 0;

        for (Transaction transaction : transactions) {
            for (int j = 0; j < transaction.getProducts().size(); ++j) {
                sum += transaction.getProducts().get(j).getPrice();
            }
        }

        return sum / transactions.size();
    }
}
