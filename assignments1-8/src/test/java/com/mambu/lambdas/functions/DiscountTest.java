package com.mambu.lambdas.functions;

import com.mambu.lambdas.model.Product;
import com.mambu.lambdas.model.ProductTestData;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class DiscountTest {
    private Discount discount = new Discount();

    @Test
    public void testDiscountOneProduct(){
        Product p = ProductTestData.aProduct();
        Product another = ProductTestData.aSpecificProduct(2, "test", "meat", 3);
        List<Product> products = Arrays.asList(p, another);

        List<Product> filteredProducts = discount.applyDiscount(products, "test");

        assertEquals(1, filteredProducts.size());
        assertEquals(p.getPrice()*0.9, filteredProducts.get(0).getPrice());
    }

    @Test
    public void testDiscountMultipleProducts(){
        int size = 5;
        List<Product> products = ProductTestData.anList(size);

        List<Product> filteredProducts = discount.applyDiscount(products, products.get(0).getCategory());

        assertEquals(size, filteredProducts.size());
        for(int i=0; i<size; ++i){
            assertEquals(products.get(i).getPrice()*0.9, filteredProducts.get(i).getPrice());
        }
    }

    @Test
    public void testDiscountNoProductInCategory(){
        Product p = ProductTestData.aProduct();
        List<Product> products = Arrays.asList(p);

        List<Product> filteredProducts = discount.applyDiscount(products, "nonexistent");

        assertEquals(0, filteredProducts.size());
    }

    @Test
    public void testDiscountEmptyList(){
        List<Product> products = new ArrayList<>();

        List<Product> filteredProducts = discount.applyDiscount(products, "test");

        assertEquals(0, filteredProducts.size());
    }

    @Test
    public void testDiscountNullList(){
        List<Product> products = null;

        NullPointerException exception = assertThrows(NullPointerException.class,
                ()->discount.applyDiscount(products, "test"));

        assertNotNull(exception);
    }
}
