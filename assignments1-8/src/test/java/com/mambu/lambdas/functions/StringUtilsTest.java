package com.mambu.lambdas.functions;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public class StringUtilsTest {
    private final StringUtils stringUtils = new StringUtils();
    private final List<String> words = Arrays.asList("a", "an", "pencil", "arbiter", "skirt", "council");

    @Test
    public void filterWordsStartingWithMultipleMatches(){
        List<String> filteredWords = stringUtils.filterWordsStartingWith(words, 'a');

        assertEquals(3, filteredWords.size());
    }

    @Test
    public void filterWordsStartingWithEmptyList(){
        List<String> words = new LinkedList<>();
        List<String> filteredWords = stringUtils.filterWordsStartingWith(words, 'a');

        assertEquals(0, filteredWords.size());
    }

    @Test
    public void filterWordsStartingWithNoMatch(){
        List<String> filteredWords = stringUtils.filterWordsStartingWith(words, 'b');

        assertEquals(0, filteredWords.size());
    }

    @Test
    public void filterWordsEndingWithMultipleMatches(){
        List<String> filteredWords = stringUtils.filterWordsEndingWith(words, 'l');

        assertEquals(2, filteredWords.size());
    }

    @Test
    public void filterWordsEndingWithEmptyList(){
        List<String> words = new LinkedList<>();
        List<String> filteredWords = stringUtils.filterWordsEndingWith(words, 'a');

        assertEquals(0, filteredWords.size());
    }

    @Test
    public void filterWordsEndingWithNoMatch(){
        List<String> filteredWords = stringUtils.filterWordsEndingWith(words, 'b');

        assertEquals(0, filteredWords.size());
    }

    @Test
    public void filterWordsLongerThan0Length(){
        List<String> filteredWords = stringUtils.filterWordsLongerThan(words, 0);

        assertEquals(words.size(), filteredWords.size());
    }

    @Test
    public void filterWordsLongerThanPositiveLength(){
        List<String> filteredWords = stringUtils.filterWordsLongerThan(words, 1);

        assertEquals(words.size() - 1, filteredWords.size());
    }

    @Test
    public void filterWordsLongerThanMaxLength(){
        List<String> filteredWords = stringUtils.filterWordsLongerThan(words, 7);

        assertEquals(0, filteredWords.size());
    }

    @Test
    public void filterWordsLongerThanEmptyList(){
        List<String> words = new LinkedList<>();
        List<String> filteredWords = stringUtils.filterWordsLongerThan(words, 0);

        assertEquals(0, filteredWords.size());
    }

    @Test
    public void filterWordsLongerThanNegativeLength(){
        IllegalArgumentException exception = assertThrows(IllegalArgumentException.class,
                ()->stringUtils.filterWordsLongerThan(words, -1));

        assertNotNull(exception);
    }

    @Test
    public void filterWordsContainingVowelsMultipleMatches(){
        List<String> filteredWords = stringUtils.filterWordsContainingVowels(words, 3);

        assertEquals(2, filteredWords.size());
    }

    @Test
    public void filterWordsContainingVowelsNoMatch(){
        List<String> filteredWords = stringUtils.filterWordsContainingVowels(words, 4);

        assertEquals(0, filteredWords.size());
    }

    @Test
    public void filterWordsContainingVowelsEmptyList(){
        List<String> words = new LinkedList<>();

        List<String> filteredWords = stringUtils.filterWordsContainingVowels(words, 3);

        assertEquals(0, filteredWords.size());
    }

    @Test
    public void joinWordsMultipleWords(){
        String[] capitalizedWords = capitalizeListString(words);

        String joined = stringUtils.joinWords(words, ", ");

        assertArrayEquals(capitalizedWords, joined.split(", "));
    }

    @Test
    public void joinWordsOneWord(){
        List<String> words = Arrays.asList("word");
        String[] capitalizedWords = capitalizeListString(words);

        String joined = stringUtils.joinWords(words, ", ");

        assertArrayEquals(capitalizedWords, joined.split(", "));
    }

    @Test
    public void joinWordsEmptyList(){
        List<String> words = new LinkedList<>();

        String joined = stringUtils.joinWords(words, ", ");

        assertTrue(joined.isEmpty());
    }

    private String[] capitalizeListString(List<String> words){
        String[] capitalizedWords = new String[words.size()];
        for(int i=0; i< capitalizedWords.length; ++i){
            String word = words.get(i);
            capitalizedWords[i] = word.substring(0, 1).toUpperCase() + word.substring(1).toLowerCase();
        }
        return capitalizedWords;
    }
}
