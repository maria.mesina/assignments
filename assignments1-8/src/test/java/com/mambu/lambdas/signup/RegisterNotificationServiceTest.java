package com.mambu.lambdas.signup;

import com.mambu.generics.model.User;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.LinkedList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;

@ExtendWith(MockitoExtension.class)
public class RegisterNotificationServiceTest {
    private RegisterService registerService = new RegisterService();

    @Mock
    private EmailService emailService = mock(EmailService.class);
    private RegisterNotificationService rns = new RegisterNotificationService(emailService);

    @Test
    public void testRegisterOneUser(){
        User user = new User();
        user.setEmail("test@test.com");
        registerService.getPublisher().subscribe(rns);
        registerService.register(user);
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        registerService.getPublisher().close();
        assertEquals(1, rns.getConsumedElements().size());
        assertEquals(user, rns.getConsumedElements().get(0));
    }

    @Test
    public void testRegisterMultipleUsers(){
        List<User> users = aListOfUsers(5);
        registerService.getPublisher().subscribe(rns);
        for(int i=0; i<users.size(); ++i){
            registerService.register(users.get(i));
        }
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        registerService.getPublisher().close();
        assertEquals(users, rns.getConsumedElements());
    }

    private List<User> aListOfUsers(int size){
        List<User> users = new LinkedList<>();
        for(int i=0; i<size; ++i){
            User temp = new User();
            temp.setId(i+1);
            temp.setEmail("test"+(i+1)+"@test.com");
            temp.setName(String.valueOf(i+1));
            temp.setBirthYear(1900+i+1);
            users.add(temp);
        }

        return users;
    }
}
