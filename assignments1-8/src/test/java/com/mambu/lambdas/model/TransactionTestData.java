package com.mambu.lambdas.model;

import java.time.LocalDate;
import java.time.YearMonth;
import java.util.*;

public class TransactionTestData {
    private static List<Product> someProducts = Arrays.asList(
            ProductTestData.aSpecificProduct(1, "Pineapple", "Fruit", 9.89),
            ProductTestData.aSpecificProduct(2, "Beef", "Meat", 23.49),
            ProductTestData.aSpecificProduct(3, "Bananas", "Fruit", 3.00),
            ProductTestData.aSpecificProduct(4, "Croissant", "Pastries", 1.25),
            ProductTestData.aSpecificProduct(5, "Eclair", "Pastries", 5.00),
            ProductTestData.aSpecificProduct(6, "Yogurt", "Dairy", 1.89),
            ProductTestData.aSpecificProduct(7, "Milk", "Dairy", 3.20)
    );

    public static Transaction aTransaction(){
        Transaction t = new Transaction();
        t.setUserId(1);
        t.setId(1);
        t.addProduct(someProducts.get(0));
        t.addProduct(someProducts.get(1));
        return t;
    }

    public static List<Transaction> aListOfTransactions(int users, int transactionsPerUser, int productListSize, YearMonth yearMonth){
        List<Transaction> transactions = new LinkedList<>();

        for(int i=0; i<users; ++i){
            for(int j=0; j<transactionsPerUser; ++j) {
                Transaction t = new Transaction();
                t.setUserId(i);
                t.setId(i*transactionsPerUser + j);
                LocalDate date = LocalDate.of(yearMonth.getYear(), yearMonth.getMonth(), 12);
                t.setDate(date);
                for (int k=0; k<productListSize; ++k) {
                    t.addProduct(someProducts.get(t.getId() % someProducts.size()));
                }
                transactions.add(t);
            }
        }

        return transactions;
    }
}
