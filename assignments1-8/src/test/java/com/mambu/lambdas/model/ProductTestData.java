package com.mambu.lambdas.model;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class ProductTestData {
    public static Product aProduct(){
        Product p = new Product();
        p.setId(1);
        p.setName("test");
        p.setCategory("test");
        p.setPrice(1);

        return p;
    }

    public static Product aSpecificProduct(int id, String name, String category, double price){
        Product p = new Product();
        p.setId(id);
        p.setName(name);
        p.setCategory(category);
        p.setPrice(price);

        return p;
    }

    public static List<Product> anList(int size){
        List<Product> products = new LinkedList<>();
        for(int i=0; i<size; ++i){
            Product tmp = ProductTestData.aSpecificProduct(i, String.valueOf(i), "test", i);
            products.add(tmp);
        }

        return products;
    }
}
