package com.mambu.oop.utils;

import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.params.provider.ValueSource;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.params.provider.Arguments.arguments;

public class TextProcessorTest {
    private static final String testString = "text. This is a text placeholder. text";
    private static final String word = "text";
    private static final String reversedWord = new StringBuilder(word).reverse().toString();
    private static final TextProcessor textProcessor = new TextProcessor();

    @Test
    public void testReverseWordInTextStartSuccess(){
        String expectedResultString = testString.replace(word, reversedWord);

        String actualResultString = textProcessor.reverseWordInText(testString, word);

        assertEquals(expectedResultString, actualResultString);
    }

    @Test
    public void testReverseWordInTextEmptyText(){
        String actualResultString = textProcessor.reverseWordInText("", word);
        assertEquals(0, actualResultString.length());
    }

    @Test
    public void testReverseWordInTextEmptyWord(){
        String actualResultString = textProcessor.reverseWordInText(testString, "");
        assertEquals(testString, actualResultString);
    }

    @Test
    public void testReverseWordInTextNotPresentWord(){
        String word = "slack";

        String actualResultString = textProcessor.reverseWordInText(testString, word);
        assertEquals(testString, actualResultString);
    }

    @Test
    public void testReverseWordInTextNullText(){
        NullPointerException exception = Assertions.assertThrows(NullPointerException.class,
                ()-> textProcessor.reverseWordInText(null, word));

        assertNotNull(exception);
    }

    @Test
    public void testReverseWordInTextNullWord(){
        NullPointerException exception = Assertions.assertThrows(NullPointerException.class,
                ()-> textProcessor.reverseWordInText(testString, null));

        assertNotNull(exception);
    }

    @ParameterizedTest
    @ValueSource(ints = {0, 6, 16, 33, 35})
    public void testReverseWordInTextStartIndex(int startIndex){
        String expectedResultString = testString.substring(startIndex).replace(word, reversedWord);
        expectedResultString = testString.substring(0, startIndex) + expectedResultString;

        String actualResultString = textProcessor.reverseWordInText(testString, word, startIndex);

        assertEquals(expectedResultString, actualResultString);
    }

    @Test
    public void testReverseWordInTextStartIndexEmptyText(){
        String actualResult = textProcessor.reverseWordInText("", word, 0);

        assertEquals(0, actualResult.length());
    }

    @Test
    public void testReverseWordInTextStartIndexEmptyWord(){
        String actualResult = textProcessor.reverseWordInText(testString, "", 0);

        assertEquals(testString, actualResult);
    }

    @Test
    public void testReverseWordInTextStartIndexNotPresentWord(){
        String word = "slack";

        String actualResultString = textProcessor.reverseWordInText(testString, word, 0);
        assertEquals(testString, actualResultString);
    }

    @Test
    public void testReverseWordInTextStartIndexNullText(){
        NullPointerException exception = Assertions.assertThrows(NullPointerException.class,
                ()-> textProcessor.reverseWordInText(null, word, 0));

        assertNotNull(exception);
    }

    @Test
    public void testReverseWordInTextStartIndexNullWord(){
        NullPointerException exception = Assertions.assertThrows(NullPointerException.class,
                ()-> textProcessor.reverseWordInText(testString, null, 0));

        assertNotNull(exception);
    }

    @ParameterizedTest
    @ValueSource(ints = {-1, 40})
    public void testReverseWordInTextStartIndexBoundsExceptions(int startIndex){
        IndexOutOfBoundsException exception = Assertions.assertThrows(IndexOutOfBoundsException.class,
                ()->{textProcessor.reverseWordInText(testString, word, startIndex);}
        );

        assertNotNull(exception);
    }

    @ParameterizedTest
    @MethodSource("getPairOfValidIndexes")
    public void testReverseWordInTextStartEndIndex(int startIndex, int endIndex){
        String expectedResultString = testString.substring(startIndex, endIndex).replace(word, reversedWord);
        expectedResultString = testString.substring(0, startIndex) +
                expectedResultString + testString.substring(endIndex);

        String actualResultString = textProcessor.reverseWordInText(testString, word, startIndex, endIndex);

        assertEquals(expectedResultString, actualResultString);
    }

    private static Stream<Arguments> getPairOfValidIndexes() {
        return Stream.of(
                arguments(0, 0),
                arguments(0, word.length()),
                arguments(6, 16),
                arguments(16, 33),
                arguments(33, 33 + word.length()),
                arguments(35, testString.length()),
                arguments(testString.length() - word.length() - 1, testString.length())
        );
    }

    @Test
    public void testReverseWordInTextStartIndexEndIndexEmptyText(){
        String actualResult = textProcessor.reverseWordInText("", word, 0, 0);

        assertEquals(0, actualResult.length());
    }

    @Test
    public void testReverseWordInTextStartIndexEndIndexEmptyWord(){
        String actualResult = textProcessor.reverseWordInText(testString, "", 0, testString.length());

        assertEquals(testString, actualResult);
    }

    @Test
    public void testReverseWordInTextStartEndIndexNullText(){
        NullPointerException exception = Assertions.assertThrows(NullPointerException.class,
                ()-> textProcessor.reverseWordInText(null, word, 0, 0));

        assertNotNull(exception);
    }

    @Test
    public void testReverseWordInTextStartEndIndexNullWord() {
        NullPointerException exception = Assertions.assertThrows(NullPointerException.class,
                () -> textProcessor.reverseWordInText(testString, null, 0, testString.length()));

        assertNotNull(exception);
    }

    @Test
    public void testReverseWordInTextStartEndIndexNotPresentWord(){
        String word = "slack";

        String actualResultString = textProcessor.reverseWordInText(testString, word, 0, testString.length());
        assertEquals(testString, actualResultString);
    }

    @ParameterizedTest
    @MethodSource("getPairOfInvalidIndexes")
    public void testReverseWordInTextStartEndIndexBoundsExceptions(int startIndex, int endIndex){
        IndexOutOfBoundsException exception = Assertions.assertThrows(IndexOutOfBoundsException.class,
                ()->{textProcessor.reverseWordInText(testString, word, startIndex, endIndex);}
        );

        assertNotNull(exception);
    }

    private static Stream<Arguments> getPairOfInvalidIndexes() {
        return Stream.of(
                arguments(-1, 0),
                arguments(0, -1),
                arguments(testString.length() - word.length(), testString.length() + 1),
                arguments(40, 0),
                arguments(15, 0)
        );
    }
}
