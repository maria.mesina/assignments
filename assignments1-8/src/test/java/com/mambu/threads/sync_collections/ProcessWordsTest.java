package com.mambu.threads.sync_collections;

import com.mambu.generics.collections.MyQueue;
import com.mambu.generics.utils.ListUtils;
import com.mambu.threads.FileContentReader;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

public class ProcessWordsTest {
    private ProcessWords processor;

    @Test
    public void getAllCapitalCase() throws IOException {
        String largeText = FileContentReader.readContent("./src/test/resources/com/mambu/threads/forkjoin/Catching Fire.txt");
        processor = new ProcessWords(largeText);
        MyQueue<String> queue = new MyQueue<>();

        Long timeMultiThread = processor.computeAllCapitalCase();
        Long timeSingleThread = computeAllCapitalCase(largeText, queue);

        assertTrue(timeMultiThread > timeSingleThread);

        List<String> actual = processor.getResult();
        List<String> expected = queue.getALl();

        ListUtils lu = new ListUtils();
        assertTrue(lu.areEquals(actual, expected));
    }

    private Long computeAllCapitalCase(String text, MyQueue<String> words){
        String[] tokens = text.split("\\W+");
        long startTime = System.nanoTime();
        Arrays.stream(tokens, 0, tokens.length)
                .filter(w -> w.toLowerCase().charAt(0) != w.charAt(0))
                .forEach(w->words.add(w));

        long endTime = System.nanoTime();
        return endTime - startTime;
    }
}
