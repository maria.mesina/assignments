package com.mambu.threads;

import org.junit.jupiter.api.BeforeAll;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class FileContentReader {
    public static String readContent(String path) throws IOException {
        FileReader textFile = new FileReader(path);
        BufferedReader bufferedReader = new BufferedReader(textFile);
        StringBuilder stringBuilder = new StringBuilder();
        String line = "";

        while((line=bufferedReader.readLine())!=null){
            stringBuilder.append(line);
        }

        return stringBuilder.toString();
    }
}
