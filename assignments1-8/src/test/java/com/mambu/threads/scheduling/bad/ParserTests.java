package com.mambu.threads.scheduling.bad;

import com.mambu.threads.scheduling.IParser;
import com.mambu.threads.scheduling.ParserTestBase;
import com.mambu.threads.scheduling.bad.Parser;

public class ParserTests extends ParserTestBase {

    @Override
    protected IParser createParser() {
        return new Parser();
    }
}
