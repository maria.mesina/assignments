package com.mambu.threads.scheduling.good;

import com.mambu.threads.scheduling.IParser;
import com.mambu.threads.scheduling.ParserTestBase;

public class ParserTest extends ParserTestBase {
    @Override
    protected IParser createParser() {
        return new Parser();
    }
}
