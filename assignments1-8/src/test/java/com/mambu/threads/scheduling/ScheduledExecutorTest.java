package com.mambu.threads.scheduling;

import org.h2.command.dml.Call;
import org.junit.jupiter.api.Test;

import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.time.temporal.TemporalUnit;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.atomic.AtomicReference;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ScheduledExecutorTest {
    private ScheduledExecutor executor = new ScheduledExecutor();
    private static final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("HH:mm:ss");

    @Test
    public void testOneFunction() throws ExecutionException, InterruptedException {
        CompletableFuture<LocalTime> futureTime = new CompletableFuture<>();
        Runnable testCallable = ()->futureTime.complete(LocalTime.now());

        ScheduledTask task = new ScheduledTask();
        task.setFunction(testCallable);
        LocalTime expectedTime = LocalTime.now().plus(5, ChronoUnit.SECONDS);
        task.setTime(expectedTime);

        executor.addTask(task);
        executor.start();

        LocalTime actualTime = futureTime.get();
        executor.shutdown();
        assertEquals(expectedTime.format(formatter), actualTime.format(formatter));
    }

    @Test
    public void testMultipleFunctions() throws ExecutionException, InterruptedException {
        List<CompletableFuture<LocalTime>> futureTimes = new ArrayList<>();
        List<ScheduledTask> tasks = new ArrayList<>();

        createListOfTasks(futureTimes, tasks, 4, false);

        tasks.forEach(t->executor.addTask(t));

        executor.start();

        for(int i=0; i<4; ++i) {
            assertEquals(tasks.get(i).getTime().format(formatter),
                    futureTimes.get(i).get().format(formatter));
        }

        executor.shutdown();
    }

    @Test
    public void testMultipleFunctionsReverseOrder() throws ExecutionException, InterruptedException {
        List<CompletableFuture<LocalTime>> futureTimes = new ArrayList<>();
        List<ScheduledTask> tasks = new ArrayList<>();

        createListOfTasks(futureTimes, tasks, 4, true);

        tasks.forEach(t->executor.addTask(t));

        executor.start();

        for(int i=0; i<4; ++i) {
            assertEquals(tasks.get(i).getTime().format(formatter),
                    futureTimes.get(i).get().format(formatter));
        }

        executor.shutdown();
    }

    private void createListOfTasks(
            List<CompletableFuture<LocalTime>> futureTimes,
            List<ScheduledTask> tasks, int size, boolean reverseOrder){

        for(int i=0; i<size; ++i) {
            CompletableFuture<LocalTime> futureTime = new CompletableFuture<>();
            Runnable testCallable = ()->futureTime.complete(LocalTime.now());
            ScheduledTask task = new ScheduledTask();
            task.setFunction(testCallable);

            if(!reverseOrder) {
                task.setTime(LocalTime.now().plus( i + 1, ChronoUnit.SECONDS));
            }
            else{
                task.setTime(LocalTime.now().plus(size - i, ChronoUnit.SECONDS));
            }

            futureTimes.add(futureTime);
            tasks.add(task);
        }
    }
}
