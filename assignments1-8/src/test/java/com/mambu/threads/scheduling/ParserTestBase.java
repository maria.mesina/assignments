package com.mambu.threads.scheduling;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public abstract class ParserTestBase {
    private IParser parser;

    protected abstract IParser createParser();

    @BeforeEach
    public void setup(){
        parser = createParser();
    }

    @Test
    public void parseValidScheduleOneEntry() throws IOException {
        String path = "./src/test/resources/com/mambu/threads/scheduling/validScheduleOneEntry.json";

        List<ScheduledTask> tasks = parser.parseSchedule(path);

        assertEquals(1, tasks.size());
    }

    @Test
    public void parseValidScheduleMultipleEntries() throws IOException{
        String path = "./src/test/resources/com/mambu/threads/scheduling/validScheduleMultipleEntries.json";

        List<ScheduledTask> tasks = parser.parseSchedule(path);

        assertEquals(3, tasks.size());
    }

    @Test
    public void parseInvalidJsonFormat(){
        String path = "./src/test/resources/com/mambu/threads/scheduling/invalidJsonFormat.json";

        NullPointerException exception = assertThrows(NullPointerException.class,
                ()->parser.parseSchedule(path));

        assertNotNull(exception);
    }

    @Test
    public void parseInvalidFunctionName() throws IOException{
        String path = "./src/test/resources/com/mambu/threads/scheduling/invalidFunctionName.json";

        NullPointerException exception = assertThrows(NullPointerException.class,
                ()->parser.parseSchedule(path));

        assertNotNull(exception);
    }
}
