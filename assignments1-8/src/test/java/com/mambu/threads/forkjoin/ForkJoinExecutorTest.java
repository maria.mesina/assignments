package com.mambu.threads.forkjoin;

import com.mambu.threads.FileContentReader;
import com.mambu.ListTestData;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class ForkJoinExecutorTest {
    private ForkJoinExecutor executor = new ForkJoinExecutor();

    @Test
    public void testFindElementEmptyArray(){
        List<Integer> list = new ArrayList<>();
        int target = 3;

        int result = executor.findElementInList(list, target);

        assertEquals(-1, result);
    }

    @Test
    public void testFindElementLessThan100ElementsTargetNotPresent(){
        List<Integer> list = ListTestData.aRandomIntegerList(80, Integer.MIN_VALUE, 0);
        int target = 1;

        int result = executor.findElementInList(list, target);

        assertEquals(-1, result);
    }

    @Test
    public void testFindElementLessThan100ElementsTargetPresent(){
        List<Integer> list = ListTestData.aRandomIntegerList(80, Integer.MIN_VALUE, Integer.MAX_VALUE);
        int target = list.get(32);

        int result = executor.findElementInList(list, target);

        assertEquals(32, result);
    }

    @Test
    public void testFindElementMoreThan100ElementsTargetNotPresent(){
        List<Integer> list = ListTestData.aRandomIntegerList(800, Integer.MIN_VALUE, 0);
        int target = 1;

        int result = executor.findElementInList(list, target);

        assertEquals(-1, result);
    }

    @Test
    public void testFindElementMoreThan100ElementsTargetPresent(){
        List<Integer> list = ListTestData.aRandomIntegerList(800, Integer.MIN_VALUE, Integer.MAX_VALUE);
        int target = list.get(325);

        int result = executor.findElementInList(list, target);

        assertEquals(325, result);
    }

    @Test
    public void testFindElement100ElementsTargetPresent(){
        List<Integer> list = ListTestData.aRandomIntegerList(100, Integer.MIN_VALUE, Integer.MAX_VALUE);
        int target = list.get(99);

        int result = executor.findElementInList(list, target);

        assertEquals(99, result);
    }

    @Test
    public void testFindElementTargetElementAtThreshold(){
        List<Integer> list = ListTestData.aRandomIntegerList(800, Integer.MIN_VALUE, Integer.MAX_VALUE);
        int target = list.get(299);

        int result = executor.findElementInList(list, target);

        assertEquals(299, result);
    }

    @Test
    public void testFindElementTargetLastElement(){
        List<Integer> list = ListTestData.aRandomIntegerList(800, Integer.MIN_VALUE, Integer.MAX_VALUE);
        int target = list.get(799);

        int result = executor.findElementInList(list, target);

        assertEquals(799, result);
    }

    @Test
    public void testCountWordEmptyText(){
        String text = "";
        String target = "target";

        Long result = executor.countWordInText(text, target);

        assertEquals(0L, result);
    }

    @Test
    public void testCountWordEmptyTarget(){
        String text = "some text";
        String target = "";

        Long result = executor.countWordInText(text, target);

        assertEquals(0L, result);
    }

    @Test
    public void testCountWordEmptyTextAndEmptyTarget(){
        String text = "";
        String target = "";

        Long result = executor.countWordInText(text, target);

        assertEquals(0L, result);
    }

    @Test
    public void testCountWordLessThan100CharsTargetPresent(){
        String text = "This is some text. Text, TeXt.";
        String target = "text";

        Long result = executor.countWordInText(text, target);

        assertEquals(3, result);
    }

    @Test
    public void testCountWordLessThan100CharsTargetNotPresent(){
        String text = "This is some text. Text, TeXt.";
        String target = "apple";

        Long result = executor.countWordInText(text, target);

        assertEquals(0, result);
    }


    @Test
    public void testCountWordMoreThan100CharsTargetPresent() throws IOException {
        String largeText = FileContentReader.readContent("./src/test/resources/com/mambu/threads/forkjoin/Catching Fire.txt");
        String target = "games";
        Long expectedResult = countOccurrencesOfWord(largeText, target);

        Long result = executor.countWordInText(largeText, target);

        assertEquals(expectedResult, result);
    }

    private Long countOccurrencesOfWord(String text, String target){
        text = text.toLowerCase();
        target = target.toLowerCase();

        int count = Collections.frequency(Arrays.asList(text.split("\\W+")), target);

        return (long) count;
    }
}
