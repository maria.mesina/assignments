package com.mambu.threads.sorting;

import com.mambu.ListTestData;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class QuickSortTest {
    QuickSort qs = new QuickSort();
    List<Integer> list;

    @Test
    public void testEmptyList() throws Exception {
        list = new ArrayList();
        qs.setList(list);
        qs.call();

        assertTrue(list.isEmpty());
    }

    @Test
    public void testNonEmptyList() throws Exception {
        list = ListTestData.aRandomIntegerList(20, Integer.MIN_VALUE, Integer.MAX_VALUE);
        List<Integer> sorted = new ArrayList<>(list);
        Collections.sort(sorted);

        qs.setList(list);
        qs.call();

        assertEquals(sorted, list);
    }
}
