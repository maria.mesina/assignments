package com.mambu.threads.sorting;

import com.mambu.ListTestData;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;

import static org.junit.jupiter.api.Assertions.*;

public class SortingUtilsTest {
    private SortingUtils su;
    List<Integer> list;

    @BeforeEach
    public void setup(){
        su = new SortingUtils();
        su.setExecutor(Executors.newFixedThreadPool(1));
    }

    @Test
    public void sortEmptyWithBubble() throws ExecutionException, InterruptedException {
        list = new ArrayList<>();
        su.setList(list);
        Long executionTime = su.sortWithBubbleSort();
        assertNotEquals(0, executionTime);
    }

    @Test
    public void sortNonEmptyWithBubble() throws ExecutionException, InterruptedException {
        list = ListTestData.aRandomIntegerList(20, Integer.MIN_VALUE, Integer.MAX_VALUE);
        List<Integer> sorted = new ArrayList<>(list);
        Collections.sort(sorted);
        su.setList(list);

        Long executionTime = su.sortWithBubbleSort();

        assertNotEquals(0, executionTime);
        assertEquals(sorted, list);
    }

    @Test
    public void sortEmptyWithQuick() throws ExecutionException, InterruptedException {
        list = new ArrayList<>();
        su.setList(list);
        Long executionTime = su.sortWithQuickSort();
        assertNotEquals(0, executionTime);
    }

    @Test
    public void sortNonEmptyWithQuick() throws ExecutionException, InterruptedException {
        list = ListTestData.aRandomIntegerList(20, Integer.MIN_VALUE, Integer.MAX_VALUE);
        List<Integer> sorted = new ArrayList<>(list);
        Collections.sort(sorted);
        su.setList(list);

        Long executionTime = su.sortWithQuickSort();

        assertNotEquals(0, executionTime);
        assertEquals(sorted, list);
    }
    @Test
    public void sortEmptyWithMerge() throws ExecutionException, InterruptedException {
        list = new ArrayList<>();
        su.setList(list);
        Long executionTime = su.sortWithMergeSort();
        assertNotEquals(0, executionTime);
    }

    @Test
    public void sortNonEmptyWithMerge() throws ExecutionException, InterruptedException {
        list = ListTestData.aRandomIntegerList(20, Integer.MIN_VALUE, Integer.MAX_VALUE);
        List<Integer> sorted = new ArrayList<>(list);
        Collections.sort(sorted);
        su.setList(list);

        Long executionTime = su.sortWithMergeSort();

        assertNotEquals(0, executionTime);
        assertEquals(sorted, list);
    }
}
