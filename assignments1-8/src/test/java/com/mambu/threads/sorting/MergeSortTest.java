package com.mambu.threads.sorting;

import com.mambu.ListTestData;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class MergeSortTest {
    MergeSort ms = new MergeSort();
    List<Integer> list;

    @Test
    public void testEmptyList() throws Exception {
        list = new ArrayList();
        ms.setList(list);
        ms.call();

        assertTrue(list.isEmpty());
    }

    @Test
    public void testNonEmptyList() throws Exception {
        list = ListTestData.aRandomIntegerList(20, Integer.MIN_VALUE, Integer.MAX_VALUE);
        List<Integer> sorted = new ArrayList<>(list);
        Collections.sort(sorted);

        ms.setList(list);
        ms.call();

        assertEquals(sorted, list);
    }
}
