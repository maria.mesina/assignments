package com.mambu.threads.sorting;

import com.mambu.ListTestData;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class BubbleSortTest {
    private BubbleSort bs = new BubbleSort();
    private List<Integer> list;

    @Test
    public void testEmptyList(){
        list = new ArrayList();
        bs.setList(list);
        bs.sort();

        assertTrue(list.isEmpty());
    }

    @Test
    public void testNonEmptyList(){
        list = ListTestData.aRandomIntegerList(20, Integer.MIN_VALUE, Integer.MAX_VALUE);
        List<Integer> sorted = new ArrayList<>(list);
        Collections.sort(sorted);

        bs.setList(list);
        bs.sort();

        assertEquals(sorted, list);
    }
}
