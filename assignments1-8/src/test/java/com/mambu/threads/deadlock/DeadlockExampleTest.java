package com.mambu.threads.deadlock;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertFalse;

public class DeadlockExampleTest {
    private DeadlockExample deadlock;

    @Test
    public void testSwapContentsDeadlock() throws InterruptedException {
        String from = "./src/test/resources/com/mambu/threads/deadlock/from";
        String to = "/src/test/resources/com/mambu/threads/deadlock/to.txt";

        deadlock = new DeadlockExample(from, to);
        assertFalse(deadlock.swapContents());
    }
}
