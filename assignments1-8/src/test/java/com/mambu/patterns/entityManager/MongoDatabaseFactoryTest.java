package com.mambu.patterns.entityManager;

import com.mambu.patterns.entityManager.interfaces.DatabaseConnection;
import com.mambu.patterns.entityManager.interfaces.DatabaseTransaction;
import com.mambu.patterns.entityManager.interfaces.DatatypeConvertor;
import com.mambu.patterns.entityManager.mongodb.MongoConnection;
import com.mambu.patterns.entityManager.mongodb.MongoConvertor;
import com.mambu.patterns.entityManager.mongodb.MongoDatabaseFactory;
import com.mambu.patterns.entityManager.mongodb.MongoTransaction;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class MongoDatabaseFactoryTest {
    private MongoDatabaseFactory mongoFactory = new MongoDatabaseFactory();

    @Test
    public void testCreateConnectionSuccess(){
        DatabaseConnection conn = mongoFactory.createConnection("test");
        assertEquals(MongoConnection.class, conn.getClass());
    }

    @Test
    public void testCreateTransactionSuccess(){
        DatabaseTransaction transaction = mongoFactory.createTransaction();
        assertEquals(MongoTransaction.class, transaction.getClass());
    }

    @Test
    public void testCreateConvertorSuccess(){
        DatatypeConvertor convertor = mongoFactory.createConvertor();
        assertEquals(MongoConvertor.class, convertor.getClass());
    }
}
