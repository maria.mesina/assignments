package com.mambu.patterns.entityManager;

import com.mambu.patterns.entityManager.interfaces.DatabaseConnection;
import com.mambu.patterns.entityManager.interfaces.DatabaseTransaction;
import com.mambu.patterns.entityManager.interfaces.DatatypeConvertor;
import com.mambu.patterns.entityManager.mongodb.MongoConnection;
import com.mambu.patterns.entityManager.mongodb.MongoConvertor;
import com.mambu.patterns.entityManager.mongodb.MongoDatabaseFactory;
import com.mambu.patterns.entityManager.mongodb.MongoTransaction;
import com.mambu.patterns.entityManager.oracle.OracleConnection;
import com.mambu.patterns.entityManager.oracle.OracleConvertor;
import com.mambu.patterns.entityManager.oracle.OracleDatabaseFactory;
import com.mambu.patterns.entityManager.oracle.OracleTransaction;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class OracleDatabaseFactoryTest {
    private OracleDatabaseFactory oracleFactory = new OracleDatabaseFactory();

    @Test
    public void testCreateConnectionSuccess(){
        DatabaseConnection conn = oracleFactory.createConnection("test");
        assertEquals(OracleConnection.class, conn.getClass());
    }

    @Test
    public void testCreateTransactionSuccess(){
        DatabaseTransaction transaction = oracleFactory.createTransaction();
        assertEquals(OracleTransaction.class, transaction.getClass());
    }

    @Test
    public void testCreateConvertorSuccess(){
        DatatypeConvertor convertor = oracleFactory.createConvertor();
        assertEquals(OracleConvertor.class, convertor.getClass());
    }
}
