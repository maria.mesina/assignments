package com.mambu.patterns.entityManager;

import com.mambu.patterns.entityManager.interfaces.DatabaseFactory;
import com.mambu.patterns.entityManager.mongodb.MongoDatabaseFactory;
import com.mambu.patterns.entityManager.oracle.OracleDatabaseFactory;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class DomainSpecificFactoryTest {
    private DomainSpecificFactory domainFactory = new DomainSpecificFactory();

    @Test
    public void testMongoFactory(){
        DatabaseFactory factory = domainFactory.getDomainSpecificFactory("mongodb");

        assertEquals(MongoDatabaseFactory.class, factory.getClass());
    }

    @Test
    public void testOracleFactory(){
        DatabaseFactory factory = domainFactory.getDomainSpecificFactory("oracle");

        assertEquals(OracleDatabaseFactory.class, factory.getClass());
    }

    @Test
    public void testUnsupportedFactory(){
        IllegalArgumentException exception = assertThrows(IllegalArgumentException.class,
                ()->domainFactory.getDomainSpecificFactory("orientdb"));

        assertNotNull(exception);
    }
}
