package com.mambu.patterns.cor.testdata;

import com.mambu.patterns.cor.model.Account;
import com.mambu.patterns.cor.model.Transaction;

import java.time.LocalDateTime;

public class TransactionTestData {
    Transaction transaction = new Transaction();
    private AccountTestData accountTestData = new AccountTestData();

    public TransactionTestData withSource(Account source){
        transaction.setSource(source);
        return this;
    }

    public TransactionTestData withDestination(Account destination){
        transaction.setDestination(destination);
        return this;
    }

    public TransactionTestData withAmount(long amount){
        transaction.setAmount(amount);
        return this;
    }

    public TransactionTestData withDate(LocalDateTime dateTime){
        transaction.setDateTime(dateTime);
        return this;
    }

    public Transaction getTransaction() {
        return transaction;
    }

    public Transaction getValidTransaction(){
        Account destination = accountTestData.getActiveAccount();
        destination.setOwner("test2");

        return this.withSource(accountTestData.getActiveAccount())
                .withDestination(destination)
                .withAmount(500)
                .withDate(LocalDateTime.now()).getTransaction();
    }
}
