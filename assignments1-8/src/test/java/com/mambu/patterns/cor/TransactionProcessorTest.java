package com.mambu.patterns.cor;

import com.mambu.patterns.cor.model.Account;
import com.mambu.patterns.cor.model.Transaction;
import com.mambu.patterns.cor.processors.*;
import com.mambu.patterns.cor.testdata.AccountTestData;
import com.mambu.patterns.cor.testdata.TransactionTestData;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class TransactionProcessorTest {
    TransactionProcessor transactionProcessor;
    TransactionTestData transactionTestData = new TransactionTestData();

    @BeforeEach
    public void setup(){
        AccountStatusChecker accountStatusChecker = new AccountStatusChecker();
        AmountChecker amountChecker = new AmountChecker();
        SourceBalanceChecker sourceBalanceChecker = new SourceBalanceChecker();
        SourceLimitChecker sourceLimitChecker = new SourceLimitChecker();
        AccountBalanceProcessor accountBalanceProcessor = new AccountBalanceProcessor();

        accountStatusChecker.setNext(amountChecker);
        amountChecker.setNext(sourceBalanceChecker);
        sourceBalanceChecker.setNext(sourceLimitChecker);
        sourceLimitChecker.setNext(accountBalanceProcessor);

        transactionProcessor = accountStatusChecker;
    }

    @Test
    public void testAccountStatusCheckerSuccess(){
        Transaction transaction = transactionTestData.getValidTransaction();

        assertTrue(transactionProcessor.apply(transaction));
    }

    @Test
    public void testAccountStatusCheckerFail(){
        AccountTestData accountTestData = new AccountTestData();
        Account inactiveAccount = accountTestData.getInactiveAccount();
        Transaction transaction = transactionTestData.getValidTransaction();
        transaction.setSource(inactiveAccount);

        assertFalse(transactionProcessor.apply(transaction));
    }

    @Test
    public void testAmountCheckerFail(){
        Transaction transaction = transactionTestData.getValidTransaction();
        transaction.setAmount(-4);
        assertFalse(transactionProcessor.apply(transaction));
    }

    @Test
    public void testSourceBalanceCheckerFail(){
        Transaction transaction = transactionTestData.getValidTransaction();
        transaction.getSource().setBalance(transaction.getAmount() - 100);
        assertFalse(transactionProcessor.apply(transaction));
    }

    @Test
    public void testSourceLimitCheckerFail(){
        Transaction transaction = transactionTestData.getValidTransaction();
        transaction.getSource().setTransactionLimit(transaction.getAmount() - 100);
        assertFalse(transactionProcessor.apply(transaction));
    }

    @Test
    public void testAccountBalanceProcessor(){
        Transaction transaction = transactionTestData.getValidTransaction();
        long initialBalanceSource = transaction.getSource().getBalance();
        long initialBalanceDestination = transaction.getDestination().getBalance();

        transactionProcessor.apply(transaction);
        assertEquals(initialBalanceSource - transaction.getAmount(), transaction.getSource().getBalance());
        assertEquals(initialBalanceDestination + transaction.getAmount(), transaction.getDestination().getBalance());
    }
}
