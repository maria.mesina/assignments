package com.mambu.patterns.cor.testdata;

import com.mambu.patterns.cor.model.Account;

public class AccountTestData {
    private Account account = new Account();

    public AccountTestData withId(long id){
        account.setId(id);
        return this;
    }

    public AccountTestData withOwner(String owner){
        account.setOwner(owner);
        return this;
    }

    public AccountTestData withBalance(long balance){
        account.setBalance(balance);
        return this;
    }

    public AccountTestData withTransactionLimit(long transactionLimit){
        account.setTransactionLimit(transactionLimit);
        return this;
    }

    public AccountTestData withCurrency(String currency){
        account.setCurrency(currency);
        return this;
    }

    public AccountTestData withStatus(boolean active){
        account.setActive(active);
        return this;
    }

    public Account getAccount(){
        return this.account;
    }

    public Account getActiveAccount(){
        AccountTestData account = new AccountTestData();
        return account.withId(1).withOwner("test").withBalance(3000)
        .withTransactionLimit(Long.MAX_VALUE).withCurrency("RON").withStatus(true).getAccount();
    }

    public Account getInactiveAccount(){
        AccountTestData account = new AccountTestData();

        return account.withId(2).withOwner("test2").withBalance(3000)
        .withTransactionLimit(Long.MAX_VALUE).withCurrency("RON").withStatus(false).getAccount();
    }
}
