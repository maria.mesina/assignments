package com.mambu.patterns.facade;

import com.mambu.patterns.cor.model.Transaction;
import com.mambu.patterns.cor.testdata.TransactionTestData;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class PaymentProcessorTest {
    private PaymentProcessor paymentProcessor = new PaymentProcessor();
    TransactionTestData transactionTestData = new TransactionTestData();

    @Test
    public void testPaySourceNull(){
        Transaction transaction = transactionTestData.getValidTransaction();
        transaction.setSource(null);

        NullPointerException exception = assertThrows(NullPointerException.class,
                ()->paymentProcessor.pay(transaction));

        assertNotNull(exception);
    }

    @Test
    public void testPayDestinationNull(){
        Transaction transaction = transactionTestData.getValidTransaction();
        transaction.setDestination(null);

        NullPointerException exception = assertThrows(NullPointerException.class,
                ()->paymentProcessor.pay(transaction));

        assertNotNull(exception);
    }

    @Test
    public void testPaySameSourceAndDestination(){
        Transaction transaction = transactionTestData.getValidTransaction();
        transaction.setDestination(transaction.getSource());

        IllegalArgumentException exception = assertThrows(IllegalArgumentException.class,
                ()->paymentProcessor.pay(transaction));

        assertNotNull(exception);
    }

    @Test
    public void testPayInvalidTransaction(){
        Transaction transaction = transactionTestData.getValidTransaction();
        transaction.setAmount(-4);

        IllegalArgumentException exception = assertThrows(IllegalArgumentException.class,
                ()->paymentProcessor.pay(transaction));

        assertNotNull(exception);
    }

    @Test
    public void testPayCheckBalanceAfterPayment(){
        Transaction transaction = transactionTestData.getValidTransaction();
        long initialBalanceSource = transaction.getSource().getBalance();
        long initialBalanceDestination = transaction.getDestination().getBalance();

        paymentProcessor.pay(transaction);
        assertEquals(initialBalanceSource - transaction.getAmount(), transaction.getSource().getBalance());
        assertEquals(initialBalanceDestination + transaction.getAmount(), transaction.getDestination().getBalance());
    }
}
