package com.mambu.patterns.facade;

import com.mambu.patterns.cor.testdata.TransactionTestData;
import com.mambu.patterns.cor.model.Transaction;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class BalanceManagerTest {
    BalanceManager balanceManager = new BalanceManager();
    TransactionTestData transactionTestData = new TransactionTestData();

    @Test
    public void testBalanceChanged(){
        Transaction transaction = transactionTestData.getValidTransaction();
        long initialBalanceSource = transaction.getSource().getBalance();
        long initialBalanceDestination = transaction.getDestination().getBalance();

        balanceManager.changeBalance(transaction);
        assertEquals(initialBalanceSource - transaction.getAmount(), transaction.getSource().getBalance());
        assertEquals(initialBalanceDestination + transaction.getAmount(), transaction.getDestination().getBalance());
    }
}
