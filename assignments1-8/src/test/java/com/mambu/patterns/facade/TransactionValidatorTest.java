package com.mambu.patterns.facade;

import com.mambu.patterns.cor.model.Account;
import com.mambu.patterns.cor.model.Transaction;
import com.mambu.patterns.cor.testdata.AccountTestData;
import com.mambu.patterns.cor.testdata.TransactionTestData;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertFalse;

public class TransactionValidatorTest {
    TransactionValidator transactionValidator = new TransactionValidator();
    TransactionTestData transactionTestData = new TransactionTestData();
    AccountTestData accountTestData = new AccountTestData();

    @Test
    public void testIsValidInactiveSource(){
        Account inactiveAccount = accountTestData.getInactiveAccount();
        Transaction transaction = transactionTestData.getValidTransaction();
        transaction.setSource(inactiveAccount);

        assertFalse(transactionValidator.isValid(transaction));
    }

    @Test
    public void testIsValidInactiveDestination(){
        Account inactiveAccount = accountTestData.getInactiveAccount();
        Transaction transaction = transactionTestData.getValidTransaction();
        transaction.setDestination(inactiveAccount);

        assertFalse(transactionValidator.isValid(transaction));
    }

    @Test
    public void testIsValidNegativeAmount(){
        Transaction transaction = transactionTestData.getValidTransaction();
        transaction.setAmount(-4);
        assertFalse(transactionValidator.isValid(transaction));
    }

    @Test
    public void testIsValidZeroAmount(){
        Transaction transaction = transactionTestData.getValidTransaction();
        transaction.setAmount(0);
        assertFalse(transactionValidator.isValid(transaction));
    }

    @Test
    public void testIsValidBalanceLessThanAmount(){
        Transaction transaction = transactionTestData.getValidTransaction();
        transaction.getSource().setBalance(transaction.getAmount() - 100);
        assertFalse(transactionValidator.isValid(transaction));
    }

    @Test
    public void testIsValidLimitLessThanAmount(){
        Transaction transaction = transactionTestData.getValidTransaction();
        transaction.getSource().setTransactionLimit(transaction.getAmount() - 100);
        assertFalse(transactionValidator.isValid(transaction));
    }
}
