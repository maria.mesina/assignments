package com.mambu.patterns.strategy;

import com.mambu.oop.utils.TextProcessor;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.params.provider.ValueSource;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.params.provider.Arguments.arguments;

public class TextProcessorStrategyTest {
    private static final String testString = "text. This is a text placeholder. text";
    private static final String word = "text";
    private static final TextProcessor textProcessor = new TextProcessor();

    private TextProcessorStrategy textProcessorStrategy;

    @Test
    public void testReverseWordInTextStartSuccess(){
        String expectedResultString = textProcessor.reverseWordInText(testString, word);
        TextParser strategyReplaceAll = new ReplaceAllOccurrences(word);
        textProcessorStrategy = new TextProcessorStrategy(strategyReplaceAll);

        String actualResultString = textProcessorStrategy.process(testString);

        assertEquals(expectedResultString, actualResultString);
    }

    @Test
    public void testReverseWordInTextEmptyText(){
        String expectedResultString = textProcessor.reverseWordInText("", word);

        TextParser strategyReplaceAll = new ReplaceAllOccurrences(word);
        textProcessorStrategy = new TextProcessorStrategy(strategyReplaceAll);

        String actualResultString = textProcessorStrategy.process("");

        assertEquals(expectedResultString, actualResultString);
    }

    @Test
    public void testReverseWordInTextEmptyWord(){
        String expectedResultString = textProcessor.reverseWordInText(testString, "");
        TextParser strategyReplaceAll = new ReplaceAllOccurrences("");
        textProcessorStrategy = new TextProcessorStrategy(strategyReplaceAll);

        String actualResultString = textProcessorStrategy.process(testString);

        assertEquals(expectedResultString, actualResultString);
    }

    @Test
    public void testReverseWordInTextNotPresentWord(){
        String word = "slack";
        String expectedResultString = textProcessor.reverseWordInText(testString, word);

        TextParser strategyReplaceAll = new ReplaceAllOccurrences(word);
        textProcessorStrategy = new TextProcessorStrategy(strategyReplaceAll);

        String actualResultString = textProcessorStrategy.process(testString);

        assertEquals(expectedResultString, actualResultString);
    }

    @Test
    public void testReverseWordInTextNullText(){
        TextParser strategyReplaceAll = new ReplaceAllOccurrences(word);
        textProcessorStrategy = new TextProcessorStrategy(strategyReplaceAll);

        NullPointerException exception = Assertions.assertThrows(NullPointerException.class,
                ()-> textProcessorStrategy.process(null));

        assertNotNull(exception);
    }

    @Test
    public void testReverseWordInTextNullWord(){
        TextParser strategyReplaceAll = new ReplaceAllOccurrences(null);
        textProcessorStrategy = new TextProcessorStrategy(strategyReplaceAll);

        NullPointerException exception = Assertions.assertThrows(NullPointerException.class,
                ()-> textProcessorStrategy.process(testString));

        assertNotNull(exception);
    }

    @ParameterizedTest
    @ValueSource(ints = {0, 6, 16, 33, 35})
    public void testReverseWordInTextStartIndex(int startIndex){
        String expectedResult = textProcessor.reverseWordInText(testString, word, startIndex);
        TextParser strategyReplaceFrom = new ReplaceOccurrencesFrom(word, startIndex);
        textProcessorStrategy = new TextProcessorStrategy(strategyReplaceFrom);

        String actualResult = textProcessorStrategy.process(testString);

        assertEquals(expectedResult, actualResult);
    }

    @Test
    public void testReverseWordInTextStartIndexEmptyText(){
        TextParser strategyReplaceFrom = new ReplaceOccurrencesFrom(word, 0);
        textProcessorStrategy = new TextProcessorStrategy(strategyReplaceFrom);

        String actualResult = textProcessorStrategy.process("");

        assertEquals(0, actualResult.length());
    }

    @Test
    public void testReverseWordInTextStartIndexEmptyWord(){
        TextParser strategyReplaceFrom = new ReplaceOccurrencesFrom("", 0);
        textProcessorStrategy = new TextProcessorStrategy(strategyReplaceFrom);

        String actualResult = textProcessorStrategy.process(testString);

        assertEquals(testString, actualResult);
    }

    @Test
    public void testReverseWordInTextStartIndexNotPresentWord(){
        String word = "slack";
        TextParser strategyReplaceFrom = new ReplaceOccurrencesFrom(word, 0);
        textProcessorStrategy = new TextProcessorStrategy(strategyReplaceFrom);

        String actualResultString = textProcessorStrategy.process(testString);
        assertEquals(testString, actualResultString);
    }

    @Test
    public void testReverseWordInTextStartIndexNullText(){
        TextParser strategyReplaceFrom = new ReplaceOccurrencesFrom(word, 0);
        textProcessorStrategy = new TextProcessorStrategy(strategyReplaceFrom);

        NullPointerException exception = Assertions.assertThrows(NullPointerException.class,
                ()-> textProcessorStrategy.process(null));

        assertNotNull(exception);
    }

    @Test
    public void testReverseWordInTextStartIndexNullWord(){
        TextParser strategyReplaceFrom = new ReplaceOccurrencesFrom(null, 0);
        textProcessorStrategy = new TextProcessorStrategy(strategyReplaceFrom);

        NullPointerException exception = Assertions.assertThrows(NullPointerException.class,
                ()-> textProcessorStrategy.process(testString));

        assertNotNull(exception);
    }

    @ParameterizedTest
    @ValueSource(ints = {-1, 40})
    public void testReverseWordInTextStartIndexBoundsExceptions(int startIndex){
        TextParser strategyReplaceFrom = new ReplaceOccurrencesFrom(word, startIndex);
        textProcessorStrategy = new TextProcessorStrategy(strategyReplaceFrom);

        IndexOutOfBoundsException exception = Assertions.assertThrows(IndexOutOfBoundsException.class,
                ()->{textProcessorStrategy.process(testString);}
        );

        assertNotNull(exception);
    }

    @ParameterizedTest
    @MethodSource("getPairOfValidIndexes")
    public void testReverseWordInTextStartEndIndex(int startIndex, int endIndex){
        TextParser strategyReplaceFromTo = new ReplaceOccurrencesFromTo(word, startIndex, endIndex);
        textProcessorStrategy = new TextProcessorStrategy(strategyReplaceFromTo);

        String expectedResultString = textProcessor.reverseWordInText(testString, word, startIndex, endIndex);

        String actualResultString = textProcessorStrategy.process(testString);

        assertEquals(expectedResultString, actualResultString);
    }

    private static Stream<Arguments> getPairOfValidIndexes() {
        return Stream.of(
                arguments(0, 0),
                arguments(0, word.length()),
                arguments(6, 16),
                arguments(16, 33),
                arguments(33, 33 + word.length()),
                arguments(35, testString.length()),
                arguments(testString.length() - word.length() - 1, testString.length())
        );
    }

    @Test
    public void testReverseWordInTextStartIndexEndIndexEmptyText(){
        TextParser strategyReplaceFromTo = new ReplaceOccurrencesFromTo(word, 0, 0);
        textProcessorStrategy = new TextProcessorStrategy(strategyReplaceFromTo);

        String actualResult = textProcessorStrategy.process("");

        assertEquals(0, actualResult.length());
    }

    @Test
    public void testReverseWordInTextStartIndexEndIndexEmptyWord(){
        TextParser strategyReplaceFromTo = new ReplaceOccurrencesFromTo("", 0, testString.length());
        textProcessorStrategy = new TextProcessorStrategy(strategyReplaceFromTo);

        String actualResult = textProcessorStrategy.process(testString);

        assertEquals(testString, actualResult);
    }

    @Test
    public void testReverseWordInTextStartEndIndexNullText(){
        TextParser strategyReplaceFromTo = new ReplaceOccurrencesFromTo(word, 0, 0);
        textProcessorStrategy = new TextProcessorStrategy(strategyReplaceFromTo);

        NullPointerException exception = Assertions.assertThrows(NullPointerException.class,
                ()-> textProcessorStrategy.process(null));

        assertNotNull(exception);
    }

    @Test
    public void testReverseWordInTextStartEndIndexNullWord() {
        TextParser strategyReplaceFromTo = new ReplaceOccurrencesFromTo(null, 0, testString.length());
        textProcessorStrategy = new TextProcessorStrategy(strategyReplaceFromTo);

        NullPointerException exception = Assertions.assertThrows(NullPointerException.class,
                () -> textProcessorStrategy.process(testString));

        assertNotNull(exception);
    }

    @Test
    public void testReverseWordInTextStartEndIndexNotPresentWord(){
        String word = "slack";
        TextParser strategyReplaceFromTo = new ReplaceOccurrencesFromTo(word, 0, testString.length());
        textProcessorStrategy = new TextProcessorStrategy(strategyReplaceFromTo);

        String actualResultString = textProcessorStrategy.process(testString);
        assertEquals(testString, actualResultString);
    }

    @ParameterizedTest
    @MethodSource("getPairOfInvalidIndexes")
    public void testReverseWordInTextStartEndIndexBoundsExceptions(int startIndex, int endIndex){
        TextParser strategyReplaceFromTo = new ReplaceOccurrencesFromTo(word, startIndex, endIndex);
        textProcessorStrategy = new TextProcessorStrategy(strategyReplaceFromTo);

        IndexOutOfBoundsException exception = Assertions.assertThrows(IndexOutOfBoundsException.class,
                ()->{textProcessorStrategy.process(testString);}
        );

        assertNotNull(exception);
    }

    private static Stream<Arguments> getPairOfInvalidIndexes() {
        return Stream.of(
                arguments(-1, 0),
                arguments(0, -1),
                arguments(testString.length() - word.length(), testString.length() + 1),
                arguments(40, 0),
                arguments(15, 0)
        );
    }
}

