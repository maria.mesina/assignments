package com.mambu;

import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class ListTestData {
    private static Random r = new Random();
    public static List<Integer> aRandomIntegerList(int size, int minVal, int maxVal){
        return r.ints(size, minVal, maxVal).boxed().collect(Collectors.toList());
    }

}
