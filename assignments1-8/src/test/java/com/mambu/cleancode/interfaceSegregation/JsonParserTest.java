package com.mambu.cleancode.interfaceSegregation;

import com.mambu.cleancode.Account;
import com.mambu.cleancode.interfaceSegregation.good.JsonParser;
import com.mambu.dataflow.serialization.InvalidInputException;
import org.junit.jupiter.api.Test;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;

import static org.junit.jupiter.api.Assertions.*;

public class JsonParserTest {
    private JsonParser jsonParser = new JsonParser();
    private final String path = "./src/test/resources/";

    @Test
    public void testParseSuccess() throws IOException {
        File validDictionary = new File(path+"validDictionary.json");

        HashMap<Object, Object> dictionary = jsonParser.parse(readFromFile(validDictionary));

        assertNotNull(dictionary);
    }

    @Test
    public void testParseInvalidDictionary() throws IOException {
        File invalidDictionary = new File(path+"invalidDictionary.json");

        InvalidInputException exception = assertThrows(InvalidInputException.class,
                ()->jsonParser.parse(readFromFile(invalidDictionary)));

        assertNotNull(exception);
    }

    @Test
    public void testParseNonexistentFile(){
        File nonexistent = new File(path+"nonexistent.json");

        FileNotFoundException exception = assertThrows(FileNotFoundException.class,
                ()->jsonParser.parse(readFromFile(nonexistent)));

        assertNotNull(exception);
    }

    @Test
    public void testValidateValidFile() throws IOException {
        File validDictionary = new File(path+"validDictionary.json");

        assertTrue(jsonParser.validate(readFromFile(validDictionary)));
    }

    @Test
    public void testValidateInvalidFile() throws IOException {
        File invalidDictionary = new File(path+"invalidDictionary.json");

        assertFalse(jsonParser.validate(readFromFile(invalidDictionary)));
    }

    @Test
    public void testValidateNonexistentFile(){
        File nonexistent = new File(path+"nonexistent.json");

        FileNotFoundException exception = assertThrows(FileNotFoundException.class,
                ()->jsonParser.validate(readFromFile(nonexistent)));

        assertNotNull(exception);
    }

    @Test
    public void testToFormatPOJO(){
        Account account = new Account("test@test.com", "test");

        String accountJson = jsonParser.toFormat(account);

        assertNotNull(accountJson);
    }

    @Test
    public void testToFormatRunnable(){
        Runnable runnable = ()->System.out.println("Hello world");
        String runnableJson = jsonParser.toFormat(runnable);
        assertNotNull(runnableJson);
        assertTrue(jsonParser.parse(runnableJson).isEmpty());
    }

    @Test
    public void testReadFromFileExistentFile() throws IOException {
        File existentFile = new File(path + "validDictionary.json");
        String expectedFileContent = readFromFile(existentFile);
        String actualFileContent = jsonParser.readFromFile(existentFile);

        assertEquals(expectedFileContent, actualFileContent);
    }

    @Test
    public void testReadFromFileNonexistentFile(){
        File nonexistent = new File(path+"nonexistent.json");

        FileNotFoundException exception = assertThrows(FileNotFoundException.class,
                ()->jsonParser.readFromFile(nonexistent));

        assertNotNull(exception);
    }

    private String readFromFile(File file) throws IOException {
        java.io.FileReader fileReader = new java.io.FileReader(file);
        BufferedReader bufferedReader = new BufferedReader(fileReader);
        String line;
        StringBuilder text = new StringBuilder();
        while((line = bufferedReader.readLine())!=null){
            text.append(line).append("\n");
        }

        return text.toString();
    }
}
