package com.mambu.cleancode.singleResponsability;

import com.mambu.cleancode.Account;
import com.mambu.cleancode.singleResponsability.good.AccountRepository;
import org.junit.jupiter.api.Test;

import java.util.NoSuchElementException;

import static org.junit.jupiter.api.Assertions.*;

public class AccountRepositoryTest {
    private AccountRepository accountRepository = new AccountRepository();

    @Test
    public void testAddAccountSuccess(){
        Account accountToAdd = new Account("test@test.com", "test");
        Account addedAccount = accountRepository.addAccount(accountToAdd);

        assertNotNull(addedAccount);

        addedAccount = accountRepository.findAccountById(addedAccount.getId());
        assertNotNull(addedAccount);
    }

    @Test
    public void testAddAccountNull(){
        Account accountToAdd = null;

        NullPointerException exception = assertThrows(NullPointerException.class,
                ()-> accountRepository.addAccount(accountToAdd));
        assertNotNull(exception);
    }

    @Test
    public void testDeleteAccountSuccess(){
        Account accountToAdd = new Account("test@test.com", "test");
        Account addedAccount = accountRepository.addAccount(accountToAdd);

        assertTrue(accountRepository.deleteAccount(addedAccount));
        NoSuchElementException exception = assertThrows(NoSuchElementException.class,
                ()-> accountRepository.findAccountById(addedAccount.getId()));
        assertNotNull(exception);
    }

    @Test
    public void testDeleteAccountNonexistent(){
        Account accountToAdd = new Account("test@test.com", "test");
        assertFalse(accountRepository.deleteAccount(accountToAdd));
    }
}
