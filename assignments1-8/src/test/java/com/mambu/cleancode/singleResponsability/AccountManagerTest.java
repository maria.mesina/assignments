package com.mambu.cleancode.singleResponsability;

import com.mambu.cleancode.singleResponsability.good.AccountManager;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class AccountManagerTest {
    private AccountManager accountManager = new AccountManager();

    @Test
    public void testRegisterInvalidEmail(){
        String email = "invalid.com";
        String password = "password";

        IllegalArgumentException exception = assertThrows(IllegalArgumentException.class,
                ()->accountManager.register(email, password));

        assertNotNull(exception);
    }

    @Test
    public void testRegisterInvalidPassword(){
        String email = "test@test.com";
        String password = "pass";

        IllegalArgumentException exception = assertThrows(IllegalArgumentException.class,
                ()->accountManager.register(email, password));

        assertNotNull(exception);
    }

    @Test
    public void testRegisterSuccess(){
        String email = "test@test.com";
        String password = "password1Y";

        assertTrue(accountManager.register(email, password));
    }

    @Test
    public void testDeleteAccountSuccess(){
        String email = "test@test.com";
        String password = "password1Y";
        accountManager.register(email, password);

        assertTrue(accountManager.deleteAccount(1));
    }
}
