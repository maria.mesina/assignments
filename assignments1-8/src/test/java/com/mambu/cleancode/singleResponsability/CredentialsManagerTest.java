package com.mambu.cleancode.singleResponsability;

import com.mambu.cleancode.singleResponsability.good.CredentialsManager;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class CredentialsManagerTest {
    private CredentialsManager credentialsManager = new CredentialsManager();

    @Test
    public void testValidateEmailWithoutAtSign(){
        String email = "invalid-email.com";

        assertFalse(credentialsManager.validateEmail(email));
    }

    @Test
    public void testValidateEmailWithoutDot(){
        String email = "invalid-email@com";

        assertFalse(credentialsManager.validateEmail(email));
    }

    @Test
    public void testValidateEmailWithoutDomain(){
        String email = "invalid-email";

        assertFalse(credentialsManager.validateEmail(email));
    }

    @Test
    public void testValidateEmailWithoutUsername(){
        String email = "@gmail.com";

        assertFalse(credentialsManager.validateEmail(email));
    }

    @Test
    public void testValidateEmailWithDigits(){
        String email = "valid23@gmail.com";

        assertTrue(credentialsManager.validateEmail(email));
    }

    @Test
    public void testValidateEmailWithSpecialChars(){
        String email = "invalid-email#$@gmail.com";

        assertFalse(credentialsManager.validateEmail(email));
    }

    @Test
    public void testValidatePasswordLessThan8Symbols(){
        String password = "abc";

        assertFalse(credentialsManager.validatePassword(password));
    }

    @Test
    public void testValidatePassword8Symbols(){
        String password = "abcdefgh";

        assertFalse(credentialsManager.validatePassword(password));
    }

    @Test
    public void testValidatePasswordNoDigits(){
        String password = "abcdefghaaa";

        assertFalse(credentialsManager.validatePassword(password));
    }

    @Test
    public void testValidatePasswordNoUpperCase(){
        String password = "abcdefgha23a";

        assertFalse(credentialsManager.validatePassword(password));
    }

    @Test
    public void testValidatePasswordSuccess(){
        String password = "abcdefgha1A";

        assertTrue(credentialsManager.validatePassword(password));
    }
}
