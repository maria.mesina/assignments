package com.mambu.cleancode.dry;

import com.mambu.ListTestData;
import com.mambu.cleancode.dry.good.ArrayUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class ArrayUtilsTest {
    private ArrayUtils<Integer> arrayUtils = new ArrayUtils<>();
    private List<Integer> list;

    @BeforeEach
    private void setup(){
        list = ListTestData.aRandomIntegerList(20, Integer.MIN_VALUE, Integer.MAX_VALUE);
    }

    @Test
    public void testGetMaxNElementsNLessThanListSize(){
        List<Integer> maximum19Elements = arrayUtils.getMaxNElements(list, 19);

        Collections.sort(list);
        Collections.reverse(list);
        list.remove(list.size() - 1);
        assertEquals(list, maximum19Elements);
    }

    @Test
    public void testGetMaxNElementsNEqualsListSize(){
        List<Integer> maximum20Elements = arrayUtils.getMaxNElements(list, list.size());

        Collections.sort(list);
        Collections.reverse(list);
        assertEquals(list, maximum20Elements);
    }

    @Test
    public void testGetMaxNElementsNMoreThanListSize(){
        List<Integer> maximum21Elements = arrayUtils.getMaxNElements(list, list.size()+1);

        Collections.sort(list);
        Collections.reverse(list);
        assertEquals(list, maximum21Elements);
    }


    @Test
    public void testGetMaxNElementsNZero(){
        List<Integer> maximum0Elements = arrayUtils.getMaxNElements(list, 0);

        assertTrue(maximum0Elements.isEmpty());
    }

    @Test
    public void testGetMaxNElementsEmptyList(){
        list = new ArrayList<>();
        List<Integer> maximum0Elements = arrayUtils.getMaxNElements(list, 1);

        assertTrue(maximum0Elements.isEmpty());
    }

    @Test
    public void testGetMaxNElementsNullList(){
        list = null;
        NullPointerException exception = assertThrows(NullPointerException.class,
                ()-> arrayUtils.getMaxNElements(list, 1));

        assertNotNull(exception);
    }

    @Test
    public void testGetMinNElementsNLessThanListSize(){
        List<Integer> maximum19Elements = arrayUtils.getMinNElements(list, 19);

        Collections.sort(list);
        list.remove(list.size() - 1);
        assertEquals(list, maximum19Elements);
    }

    @Test
    public void testGetMinElementsNEqualsListSize(){
        List<Integer> maximum20Elements = arrayUtils.getMinNElements(list, list.size());

        Collections.sort(list);
        assertEquals(list, maximum20Elements);
    }

    @Test
    public void testGetMinNElementsNMoreThanListSize(){
        List<Integer> maximum21Elements = arrayUtils.getMinNElements(list, list.size()+1);

        Collections.sort(list);
        assertEquals(list, maximum21Elements);
    }

    @Test
    public void testGetMinNElementsNZero(){
        List<Integer> maximum0Elements = arrayUtils.getMinNElements(list, 0);

        assertTrue(maximum0Elements.isEmpty());
    }

    @Test
    public void testGetMinNElementsEmptyList(){
        list = new ArrayList<>();
        List<Integer> maximum0Elements = arrayUtils.getMinNElements(list, 1);

        assertTrue(maximum0Elements.isEmpty());
    }

    @Test
    public void testGetMinNElementsNullList(){
        list = null;
        NullPointerException exception = assertThrows(NullPointerException.class,
                ()-> arrayUtils.getMinNElements(list, 1));

        assertNotNull(exception);
    }
}
