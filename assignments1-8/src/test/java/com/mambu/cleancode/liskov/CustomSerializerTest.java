package com.mambu.cleancode.liskov;

import com.mambu.cleancode.liskov.good.CustomSerializer;
import org.junit.jupiter.api.Test;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class CustomSerializerTest {
    private CustomSerializer customSerializer = new CustomSerializer();
    private final String path = "./src/test/resources/com/mambu/cleancode/liskov/customSerializer/";

    @Test
    public void testSerializeSuccess() throws IOException, ClassNotFoundException {
        String filenameDestination = path + "serialize-success.txt";
        List<Integer> integersToWrite = generateList(20);
        customSerializer.writeIntegersToFile(filenameDestination, integersToWrite);

        List<Integer> readIntegers = readIntsFromFile(filenameDestination);

        assertEquals(integersToWrite, readIntegers);
    }

    @Test
    public void testSerializeNull() {
        String filenameDestination = path + "/nullList.txt";

        NullPointerException exception = assertThrows(NullPointerException.class,
                ()->customSerializer.writeIntegersToFile(filenameDestination, null));

        assertNotNull(exception);
    }

    @Test
    public void testDeserializeSuccess() throws IOException {
        String filenameDestination = path + "serialize-success.txt";
        List<Integer> integersToWrite = generateList(20);
        writeIntsToFile(filenameDestination, integersToWrite);

        List<Integer> readIntegers = customSerializer.readIntegersFromFile(filenameDestination);

        assertEquals(integersToWrite, readIntegers);
    }

    @Test
    public void testDeserializeNonexistentFile() {
        String filenameDestination = path + "/nonexistent.txt";

        FileNotFoundException exception = assertThrows(FileNotFoundException.class,
                ()->customSerializer.readIntegersFromFile(filenameDestination));

        assertNotNull(exception);
    }

    @Test
    public void testDeserializeNull() {
        NullPointerException exception = assertThrows(NullPointerException.class,
                ()->customSerializer.readIntegersFromFile(null));

        assertNotNull(exception);
    }

    private List<Integer> readIntsFromFile(String filename) throws IOException {
        FileReader fileReader = new FileReader(filename);
        BufferedReader bufferedReader = new BufferedReader(fileReader);
        List<Integer> ints = new ArrayList<>();

        String line;
        while((line = bufferedReader.readLine())!= null){
            ints.add(Integer.parseInt(line));
        }

        return ints;
    }

    private void writeIntsToFile(String filename, List<Integer> ints) throws IOException {
        String listStringRepresentation = ints.stream().map(Object::toString).collect(Collectors.joining("\n"));

        FileWriter fileWriter = new FileWriter(filename);
        BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
        bufferedWriter.write(listStringRepresentation);

        bufferedWriter.flush();
        bufferedWriter.close();
        fileWriter.close();
    }

    private List<Integer> generateList(int size){
        return Stream.generate(new Random()::nextInt).limit(size).collect(Collectors.toList());
    }
}
