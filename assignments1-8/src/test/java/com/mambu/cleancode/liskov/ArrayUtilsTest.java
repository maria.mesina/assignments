package com.mambu.cleancode.liskov;

import com.mambu.cleancode.liskov.good.CsvSerializer;
import com.mambu.cleancode.liskov.good.CustomSerializer;
import com.mambu.ListTestData;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ArrayUtilsTest {
    private ArrayUtils arrayUtils;
    private final String path = "./src/test/resources/com/mambu/cleancode/liskov/arrayUtils/";

    @Test
    public void testCustomSerializer() throws IOException {
        arrayUtils = new ArrayUtils(new CustomSerializer());

        String filenameDestination = path + "serialize-success.test";
        List<Integer> integersToWrite = ListTestData.aRandomIntegerList(20, Integer.MIN_VALUE, Integer.MAX_VALUE);

        arrayUtils.writeIntsToFile(filenameDestination, integersToWrite);
        List<Integer> readIntegers = arrayUtils.readIntsFromFile(filenameDestination);

        assertEquals(integersToWrite, readIntegers);
    }

    @Test
    public void testCsvSerializer() throws IOException {
        arrayUtils = new ArrayUtils(new CsvSerializer());

        String filenameDestination = path + "serialize-success.test";
        List<Integer> integersToWrite = ListTestData.aRandomIntegerList(20, Integer.MIN_VALUE, Integer.MAX_VALUE);

        arrayUtils.writeIntsToFile(filenameDestination, integersToWrite);
        List<Integer> readIntegers = arrayUtils.readIntsFromFile(filenameDestination);

        assertEquals(integersToWrite, readIntegers);
    }
}
