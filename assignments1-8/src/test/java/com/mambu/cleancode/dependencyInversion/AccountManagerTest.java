package com.mambu.cleancode.dependencyInversion;

import com.mambu.cleancode.Account;
import com.mambu.cleancode.dependencyInversion.good.AccountManager;
import com.mambu.cleancode.dependencyInversion.good.AccountRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.AdditionalAnswers;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class AccountManagerTest {
    @Mock
    AccountRepository mockedAccountRepository = mock(AccountRepository.class);
    AccountManager accountManager = new AccountManager(mockedAccountRepository);

    @Test
    public void testRegisterSuccess(){
        String email = "test@test.com";
        String password = "password1Y";
        accountManager.register(email, password);

        verify(accountManager.getAccountRepository(), times(1)).addAccount(any(Account.class));
    }

    @Test
    public void testDisplayAccountsSuccess(){
        accountManager.displayAccounts();
        verify(accountManager.getAccountRepository(), times(1)).getAccountsAsList();
    }
}
