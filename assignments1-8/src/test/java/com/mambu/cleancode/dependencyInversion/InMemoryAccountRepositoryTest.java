package com.mambu.cleancode.dependencyInversion;

import com.mambu.cleancode.Account;
import com.mambu.cleancode.dependencyInversion.good.InMemoryAccountRepository;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertFalse;

public class InMemoryAccountRepositoryTest {
    private InMemoryAccountRepository accountRepository = new InMemoryAccountRepository();

    @Test
    public void testAddAccountSuccess(){
        Account accountToAdd = new Account("test@test.com", "test");
        Account addedAccount = accountRepository.addAccount(accountToAdd);

        assertNotNull(addedAccount);
        assertTrue(accountRepository.getAccountsAsList().contains(addedAccount));
    }

    @Test
    public void testAddAccountNull(){
        Account accountToAdd = null;

        NullPointerException exception = assertThrows(NullPointerException.class,
                ()-> accountRepository.addAccount(accountToAdd));
        assertNotNull(exception);
    }

    @Test
    public void testDeleteAccountSuccess(){
        Account accountToAdd = new Account("test@test.com", "test");
        Account addedAccount = accountRepository.addAccount(accountToAdd);

        assertTrue(accountRepository.deleteAccount(addedAccount));
        assertFalse(accountRepository.getAccountsAsList().contains(addedAccount));
    }

    @Test
    public void testDeleteAccountNonexistent(){
        Account accountToAdd = new Account("test@test.com", "test");
        assertFalse(accountRepository.deleteAccount(accountToAdd));
    }
}
