package com.mambu.cleancode.openClosed;

import com.mambu.cleancode.openClosed.good.MovableObject;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class MovableObjectTest {
    private MovableObject movableObject = new MovableObject(0, 0);

    @Test
    public void testUpdatePositionNegativeOffset(){
        int offsetX = -10;
        int offsetY = -10;

        movableObject.updatePosition(offsetX, offsetY);

        assertEquals(offsetX, movableObject.getX());
        assertEquals(offsetY, movableObject.getY());
    }

    @Test
    public void testUpdatePositionZeroOffset(){
        int offsetX = 0;
        int offsetY = 0;

        movableObject.updatePosition(offsetX, offsetY);

        assertEquals(0, movableObject.getX());
        assertEquals(0, movableObject.getY());
    }

    @Test
    public void testUpdatePositionPositiveOffset(){
        int offsetX = 10;
        int offsetY = 10;

        movableObject.updatePosition(offsetX, offsetY);

        assertEquals(offsetX, movableObject.getX());
        assertEquals(offsetY, movableObject.getY());
    }

    @Test
    public void testUpdatePositionPositiveAndNegativeOffsets(){
        int offsetX = -10;
        int offsetY = 10;

        movableObject.updatePosition(offsetX, offsetY);

        assertEquals(offsetX, movableObject.getX());
        assertEquals(offsetY, movableObject.getY());
    }

    @Test
    public void testUpdatePosition2Updates(){
        int offsetX = -10;
        int offsetY = 10;

        movableObject.updatePosition(offsetX, offsetY);
        movableObject.updatePosition(offsetX, offsetY);

        assertEquals(2 * offsetX, movableObject.getX());
        assertEquals(2 * offsetY, movableObject.getY());
    }

    @Test
    public void testMoveToNegative(){
        int x = -30;
        int y = -4;

        movableObject.moveTo(x, y);

        assertEquals(x, movableObject.getX());
        assertEquals(y, movableObject.getY());
    }

    @Test
    public void testMoveToPositive(){
        int x = 30;
        int y = 4;

        movableObject.moveTo(x, y);

        assertEquals(x, movableObject.getX());
        assertEquals(y, movableObject.getY());
    }
}
