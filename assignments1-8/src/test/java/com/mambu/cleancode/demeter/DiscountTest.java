package com.mambu.cleancode.demeter;

import com.mambu.cleancode.demeter.good.Discount;
import com.mambu.cleancode.demeter.good.Product;
import com.mambu.cleancode.demeter.good.Transaction;
import com.mambu.cleancode.demeter.good.User;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class DiscountTest {
    private Discount discount = new Discount();

    @Test
    public void calculateDiscountPriceByTotalAmountSpentLessThan500(){
        Transaction transaction = new Transaction();
        transaction.addProduct(new Product("milk", 3.40, "dairy"));
        transaction.addProduct(new Product("bread", 3.50, "bread"));
        transaction.addProduct(new Product("beer", 5.60, "drinks"));

        User user = new User();
        user.addTransaction(transaction);
        user.addTransaction(transaction);

        double expectedDiscountPrice = transaction.getAmount() * (0.95);
        double actualDiscountPrice = discount.calculateDiscountPriceByTotalAmountSpent(transaction, user);

        assertEquals(expectedDiscountPrice, actualDiscountPrice);
    }

    @Test
    public void calculateDiscountPriceByTotalAmountSpentMoreThan500(){
        Transaction transaction = new Transaction();
        transaction.addProduct(new Product("milk", 220, "dairy"));
        transaction.addProduct(new Product("bread", 150, "bread"));
        transaction.addProduct(new Product("beer", 170, "drinks"));

        User user = new User();
        user.addTransaction(transaction);
        user.addTransaction(transaction);

        double expectedDiscountPrice = transaction.getAmount() * (0.90);
        double actualDiscountPrice = discount.calculateDiscountPriceByTotalAmountSpent(transaction, user);

        assertEquals(expectedDiscountPrice, actualDiscountPrice);
    }
}
