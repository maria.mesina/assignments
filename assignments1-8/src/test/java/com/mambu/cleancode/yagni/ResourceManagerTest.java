package com.mambu.cleancode.yagni;

import com.mambu.cleancode.yagni.good.Resource;
import com.mambu.cleancode.yagni.good.ResourceManager;
import org.junit.jupiter.api.Test;

import java.util.NoSuchElementException;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertFalse;

public class ResourceManagerTest {
    private ResourceManager resourceManager = new ResourceManager();

    @Test
    public void testAddResourceSuccess(){
        Resource addedResource = resourceManager.addResource("test");

        assertNotNull(addedResource);

        addedResource = resourceManager.getResource("test");
        assertNotNull(addedResource);
    }

    @Test
    public void testAddResourceNull(){
        NullPointerException exception = assertThrows(NullPointerException.class,
                ()-> resourceManager.addResource(null));
        assertNotNull(exception);
    }

    @Test
    public void testGetResourceSuccess(){
        Resource addedResource = resourceManager.addResource("test");
        assertNotNull(resourceManager.getResource("test"));
    }

    @Test
    public void testGetResourceNonexistent(){
        NoSuchElementException exception = assertThrows(NoSuchElementException.class,
                ()-> resourceManager.getResource("test"));
        assertNotNull(exception);
    }

    @Test
    public void testDeleteResourceSuccess(){
        Resource addedResource = resourceManager.addResource("test");

        assertTrue(resourceManager.deleteResource(addedResource));
        NoSuchElementException exception = assertThrows(NoSuchElementException.class,
                ()-> resourceManager.getResource("test"));
        assertNotNull(exception);
    }

    @Test
    public void testDeleteResourceNonexistent(){
        Resource resourceToAdd = new Resource("test");
        assertFalse(resourceManager.deleteResource(resourceToAdd));
    }
}
