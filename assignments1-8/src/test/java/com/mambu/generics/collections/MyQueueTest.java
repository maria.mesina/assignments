package com.mambu.generics.collections;

import com.mambu.generics.collections.MyQueue;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class MyQueueTest {
    private MyQueue<Integer> myQueue;

    @BeforeEach
    public void setup(){
        myQueue = new MyQueue<>();
    }

    @Test
    public void testAddOne(){
        Integer element = 13;

        myQueue.add(element);

        Integer retrievedElement = myQueue.get();
        assertEquals(element, retrievedElement);
    }

    @Test
    public void testAddContainerSize(){
        int size = 13;
        myQueue = new MyQueue<>(size);
        for(int i=0; i<size; ++i){
            myQueue.add(i);
        }

        for(int i=0; i<size; ++i) {
            assertEquals(i, myQueue.remove());
        }
    }

    @Test
    public void testAddMoreThanContainerSize(){
        int size = 4;
        myQueue = new MyQueue<>(size);
        for(int i=0; i<size*3; ++i){
            myQueue.add(i);
        }

        for(int i = 0; i<size*3; ++i){
            assertEquals(i, myQueue.get());
            myQueue.remove();
        }
    }

    @Test
    public void testIsEmptyTrue(){
        assertTrue(myQueue.isEmpty());
    }

    @Test
    public void testIsEmptyFalse(){
        myQueue.add(13);
        assertFalse(myQueue.isEmpty());
    }

    @Test
    public void testIsEmptyAfterRemoves(){
        myQueue.add(13);
        myQueue.add(24);

        myQueue.remove();
        myQueue.remove();

        assertTrue(myQueue.isEmpty());
    }

    @Test
    public void testInvalidCapacity(){
        IllegalArgumentException e = assertThrows(IllegalArgumentException.class,
                ()->myQueue = new MyQueue<>(-3));

        assertNotNull(e);
    }

    @Test
    public void testRemoveEmpty(){
        IndexOutOfBoundsException e = assertThrows(IndexOutOfBoundsException.class,
                ()->myQueue.remove());

        assertNotNull(e);
    }

    @Test
    public void testGetEmpty(){
        IndexOutOfBoundsException e = assertThrows(IndexOutOfBoundsException.class,
                ()->myQueue.get());

        assertNotNull(e);
    }

    @Test
    public void testRemoveEmptyAfterAdd(){
        int size = 3;
        for(int i=0; i<size; ++i){
            myQueue.add(i);
        }

        for(int i=size-1; i>=0; --i){
            myQueue.remove();
        }

        IndexOutOfBoundsException e = assertThrows(IndexOutOfBoundsException.class,
                ()->myQueue.remove());

        assertNotNull(e);
    }
}
