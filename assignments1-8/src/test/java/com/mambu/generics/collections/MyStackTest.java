package com.mambu.generics.collections;

import com.mambu.generics.collections.MyStack;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class MyStackTest {
    private MyStack<Integer> myStack;

    @BeforeEach
    public void setup(){
        myStack = new MyStack<>();
    }

    @Test
    public void testAddOne(){
        Integer element = 13;

        myStack.add(element);

        Integer retrievedElement = myStack.get();
        assertEquals(element, retrievedElement);
    }

    @Test
    public void testAddContainerSize(){
        int size = 13;
        myStack = new MyStack<>(size);
        for(int i=0; i<size; ++i){
            myStack.add(i);
        }

        for(int i=size-1; i>=0; --i) {
            assertEquals(i, myStack.remove());
        }
    }

    @Test
    public void testAddMoreThanContainerSize(){
        int size = 4;
        myStack = new MyStack<>(size);
        for(int i=0; i<size*3; ++i){
            myStack.add(i);
        }

        for(int i = size*3-1; i>=0; --i){
            assertEquals(i, myStack.get());
            myStack.remove();
        }
    }

    @Test
    public void testIsEmptyTrue(){
        assertTrue(myStack.isEmpty());
    }

    @Test
    public void testIsEmptyFalse(){
        myStack.add(13);
        assertFalse(myStack.isEmpty());
    }

    @Test
    public void testIsEmptyAfterRemoves(){
        myStack.add(13);
        myStack.add(24);

        myStack.remove();
        myStack.remove();

        assertTrue(myStack.isEmpty());
    }

    @Test
    public void testInvalidCapacity(){
        IllegalArgumentException e = assertThrows(IllegalArgumentException.class,
                ()->myStack = new MyStack<>(-3));

        assertNotNull(e);
    }

    @Test
    public void testRemoveEmpty(){
        IndexOutOfBoundsException e = assertThrows(IndexOutOfBoundsException.class,
                ()->myStack.remove());

        assertNotNull(e);
    }

    @Test
    public void testGetEmpty(){
        IndexOutOfBoundsException e = assertThrows(IndexOutOfBoundsException.class,
                ()->myStack.get());

        assertNotNull(e);
    }

    @Test
    public void testRemoveEmptyAfterAdd(){
        int size = 3;
        for(int i=0; i<size; ++i){
            myStack.add(i);
        }

        for(int i=size-1; i>=0; --i){
            myStack.remove();
        }

        IndexOutOfBoundsException e = assertThrows(IndexOutOfBoundsException.class,
                ()->myStack.remove());

        assertNotNull(e);
    }
}
