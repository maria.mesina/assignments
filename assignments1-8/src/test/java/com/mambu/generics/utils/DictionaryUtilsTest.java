package com.mambu.generics.utils;

import com.mambu.dataflow.serialization.InvalidInputException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;

public class DictionaryUtilsTest {
    private DictionaryUtils du;

    @BeforeEach
    public void setup(){
        du = new DictionaryUtils();
    }

    @Test
    public void readFromJsonSuccess() throws IOException {
        File file = new File("./src/test/resources/validDictionary.json");

        HashMap<String, List<String>> dict =  du.readFromJson(file);

        assertTrue(dict.containsKey("buche"));
        assertEquals(3, dict.get("buche").size());
        assertTrue(dict.containsKey("sinopsis"));
        assertEquals(1, dict.get("sinopsis").size());
        assertTrue(dict.containsKey("notoriu"));
        assertEquals(3, dict.get("notoriu").size());
    }

    @Test
    public void readFromJsonInvalidJson() throws IOException {
        File file = new File("./src/test/resources/invalidDictionary.json");

        InvalidInputException exception = assertThrows(InvalidInputException.class,
                ()->du.readFromJson(file));

        assertNotNull(exception);
    }

    @Test
    public void readFromEmptyJson() throws IOException {
        File file = new File("./src/test/resources/emptyDictionary.json");
        HashMap<String, List<String>> dict =  du.readFromJson(file);
        assertTrue(dict.isEmpty());
    }

    @Test
    public void readFromNonexistentFile() throws IOException {
        File file = new File("./src/test/resources/nonexistent.json");

        FileNotFoundException exception = assertThrows(FileNotFoundException.class,
                ()->du.readFromJson(file));

        assertNotNull(exception);
    }
}
