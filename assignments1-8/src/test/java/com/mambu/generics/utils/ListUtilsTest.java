package com.mambu.generics.utils;

import com.mambu.generics.model.User;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ListUtilsTest {
    ListUtils lu;

    @BeforeEach
    public void setup(){
        lu = new ListUtils();
    }

    @Test
    public void areEqualsSameListDistinctNumbers(){
        List<Integer> list1 = List.of(3, 2, 4, 1);
        boolean areEquals = lu.areEquals(list1, list1);
        assertTrue(areEquals);
    }

    @Test
    public void areEqualsSameListDuplicateNumbers(){
        List<Integer> list1 = List.of(3, 2, 4, 1, 2, 2, 3);
        boolean areEquals = lu.areEquals(list1, list1);
        assertTrue(areEquals);
    }

    @Test
    public void areEqualsTrueDifferentOrder(){
        List<Integer> list1 = List.of(3, 2, 4, 1, 2, 2, 3);
        List<Integer> list2 = new ArrayList<>(list1);
        Collections.shuffle(list2);

        boolean areEquals = lu.areEquals(list1, list2);
        assertTrue(areEquals);
    }

    @Test
    public void areEqualsFalseDifferentSizes(){
        List<Integer> list1 = List.of(3, 2, 4, 1, 2, 2, 3);
        List<Integer> list2 = new ArrayList<>(list1);
        list2.add(7);

        boolean areEquals = lu.areEquals(list1, list2);
        assertFalse(areEquals);
    }

    @Test
    public void areEqualsFalseDifferentContent(){
        List<Integer> list1 = List.of(3, 2, 4, 1, 2);
        List<Integer> list2 = List.of(5, 1, 4, 2, 9);
        boolean areEquals = lu.areEquals(list1, list2);
        assertFalse(areEquals);
    }

    @Test
    public void areEqualsTrueEmpty(){
        List<Integer> list1 = new ArrayList<>();
        List<Integer> list2 = new ArrayList<>();
        boolean areEquals = lu.areEquals(list1, list2);
        assertTrue(areEquals);
    }

    @Test
    public void areEqualsFalseFirstEmpty(){
        List<Integer> list1 = List.of(3, 2, 4, 1, 2);
        List<Integer> list2 = new ArrayList<>();
        boolean areEquals = lu.areEquals(list1, list2);
        assertFalse(areEquals);
    }

    @Test
    public void areEqualsFalseSecondEmpty(){
        List<Integer> list1 = List.of(3, 2, 4, 1, 2);
        List<Integer> list2 = new ArrayList<>();
        boolean areEquals = lu.areEquals(list2, list1);
        assertFalse(areEquals);
    }

    @Test
    public void areEqualsOneNull(){
        List<Integer> list1 = List.of(3, 2, 4, 1, 2);
        List<Integer> list2 = null;
        NullPointerException exception = assertThrows(NullPointerException.class,
                ()->lu.areEquals(list1, list2));
        assertNotNull(exception);
    }

    @Test
    public void areEqualsCustomClass(){
        User user1 = new User();
        user1.setId(3);
        User user2 = new User();
        user2.setId(3);

        List<User> userList1 = List.of(user1);
        List<User> userList2 = List.of(user2);

        boolean areEquals = lu.areEquals(userList1, userList2);
        assertTrue(areEquals);
    }
}
