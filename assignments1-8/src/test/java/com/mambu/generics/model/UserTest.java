package com.mambu.generics.model;

import com.mambu.generics.model.User;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class UserTest {
    private List<User> users;

    @BeforeEach
    public void setup(){
        users = new ArrayList<>();
    }

    @Test
    public void testCompareSuccess(){
        for(int i=4; i>0; --i){
            User user = new User();
            user.setId(i);
            user.setName(String.valueOf(i));

            users.add(user);
        }

        Collections.sort(users);

        long prevId = 0;
        for(int i=0; i<4; ++i){
            assertTrue(prevId < users.get(i).getId());
            prevId = users.get(i).getId();
        }
    }

    @Test
    public void testCompareToNull(){
        User user = new User();
        user.setId(1);
        users.add(user);
        users.add(null);

        NullPointerException exception = assertThrows(NullPointerException.class,
                ()->Collections.sort(users));

        assertNotNull(exception);
    }
}
