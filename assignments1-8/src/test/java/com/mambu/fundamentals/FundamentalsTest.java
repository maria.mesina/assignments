package com.mambu.fundamentals;

import org.junit.Test;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;


public class FundamentalsTest {
    FundamentalsMainClass fmc = new FundamentalsMainClass();

    @Test
    public void testCountOccurrencesSuccess(){
        int[] array = new int[]{3, 4, 3, 6, 1, 3};
        int element = 3;

        int occurrencesOfElement = fmc.countOccurrences(array, element);

        assertEquals(3, occurrencesOfElement);
    }

    @Test
    public void testCountOccurrencesInvalidInputException(){
        int[] array = null;
        int element = 3;

        NullPointerException exception = assertThrows(NullPointerException.class,
                ()-> fmc.countOccurrences(array, element));

        assertNotNull(exception);
    }

    @Test
    public void testFindElementSuccess(){
        int[] array = new int[] {3, 4, 3, 6, 1, 3};
        int element = 3;

        int index = fmc.findElement(array, element);

        assertEquals(index, 0);
    }

    @Test
    public void testFindElementNotPresent(){
        int[] array = new int[] {3, 4, 3, 6, 1, 3};
        int element = 7;

        int index = fmc.findElement(array, element);

        assertEquals(index, -1);
    }

    @Test
    public void testFindElementInvalidInputException(){
        int[] array = null;
        int element = 3;

        NullPointerException exception = assertThrows(NullPointerException.class,
                ()-> fmc.findElement(array, element));

        assertNotNull(exception);
    }

    @Test
    public void testReverseArraySuccess(){
        int[] array = new int[] {3, 4, 3, 6, 1, 3};

        int[] reverse = fmc.reverseArray(array);

        boolean isReverse = checkArraysAreReversed(array, reverse);

        assertTrue(isReverse);
    }

    @Test
    public void testReverseArrayInvalidInputException(){
        int[] array = null;

        NullPointerException exception = assertThrows(NullPointerException.class,
                ()-> fmc.reverseArray(array));

        assertNotNull(exception);
    }

    @Test
    public void testBubbleSort(){
        int[] array = new int[] {1, 3, 5, 2};

        int[] sorted = fmc.bubbleSort(array);
        Arrays.sort(array);
        assertArrayEquals(array, sorted);
    }

    @Test
    public void testBubbleSortInvalidInputException(){
        int[] array = null;

        NullPointerException exception = assertThrows(NullPointerException.class,
                ()-> fmc.bubbleSort(array));

        assertNotNull(exception);
    }

    private boolean checkArraysAreReversed(int[] array, int[] reversed){
        boolean equals = true;
        for(int i=0; i< array.length && equals; ++i){
            if (array[i] != reversed[reversed.length - 1 - i]){
                equals = false;
            }
        }

        return equals;
    }
}
