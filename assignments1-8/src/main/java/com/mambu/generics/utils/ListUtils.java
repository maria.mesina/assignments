package com.mambu.generics.utils;

import java.util.HashMap;
import java.util.List;

public class ListUtils {

    /*
    For this functionality to work correctly with lists of user-defined objects, you need to overwrite hashCode
    function of provided class, accordingly to javadoc specs.
     */
    public <T> boolean areEquals(List<T> first, List<T> second){
        HashMap<T, Integer> elements = new HashMap<T, Integer>();

        for(T element: first){
            int elementCount = elements.getOrDefault(element, 0);
            elements.put(element, elementCount+1);
        }

        for(T element: second){
            if (!elements.containsKey(element)){
                return false;
            }

            int elementCount = elements.get(element);
            if (elementCount == 1){
                elements.remove(element);
            }
            else{
                elements.put(element, elementCount - 1);
            }
        }

        return elements.isEmpty();
    }
}
