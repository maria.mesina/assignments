package com.mambu.generics.utils;

import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mambu.dataflow.serialization.InvalidInputException;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

public class DictionaryUtils {
    public HashMap<String, List<String>> readFromJson(File file) throws IOException {
        ObjectMapper objectMapper = new ObjectMapper();
        JsonNode node = objectMapper.readTree(file);
        for (Iterator<JsonNode> it = node.elements(); it.hasNext(); ) {
            JsonNode c = it.next();

            if(!c.isArray()){
                throw new InvalidInputException("Invalid Json format");
            }
        }

        return objectMapper.readValue(file, HashMap.class);
    }
}
