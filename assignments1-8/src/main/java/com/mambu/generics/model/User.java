package com.mambu.generics.model;

public class User implements Comparable<User>{
    private long id;
    private String name;
    private int birthYear;
    private String email;

    public void setId(long id) {
        if(id<=0){
            throw new IllegalArgumentException("Id was "+ id);
        }
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setBirthYear(int birthYear) {
        this.birthYear = birthYear;
    }

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public int getBirthYear() {
        return birthYear;
    }
    
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }


    @Override
    public int compareTo(User o) {
        if(this.id < o.id){
            return -1;
        }

        if(this.id == o.id){
            return 0;
        }

        return 1;
    }

    @Override
    public boolean equals(Object o){
        if (o == this)
            return true;
        if (!(o instanceof User))
            return false;
        User other = (User)o;
        return this.id == other.id;
    }

    @Override
    public int hashCode(){
        return (int)id;
    }
}
