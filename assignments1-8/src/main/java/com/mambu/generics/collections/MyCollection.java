package com.mambu.generics.collections;

public interface MyCollection<T> {
    void add(T element);
    T remove();
    T get();
    boolean isEmpty();
    void list();
}
