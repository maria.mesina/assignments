package com.mambu.generics.collections;

import java.util.Arrays;

public class MyStack<T> implements MyCollection<T>{
    private int capacity = 32;
    private int resize = 2;
    private Object[] container;
    private int head = 0;

    public MyStack(){
        this.container = new Object[this.capacity];
    }

    public MyStack(int capacity){
        if(capacity <= 0){
            throw new IllegalArgumentException("Illegal Argument! Size was " + capacity);
        }

        this.capacity = capacity;
        this.container = new Object[this.capacity];
    }

    @Override
    public void add(T element){
        if(head == capacity){
            this.resize();
        }
        container[head] = element;
        ++head;
    }

    @Override
    @SuppressWarnings("unchecked")
    public T remove(){
        if(isEmpty()){
            throw new IndexOutOfBoundsException("Stack was empty");
        }
        T temp = (T)container[head-1];
        head--;

        return temp;
    }

    @Override
    @SuppressWarnings("unchecked")
    public T get(){
        if(isEmpty()){
            throw new IndexOutOfBoundsException("Stack was empty");
        }
        return (T)container[head-1];
    }

    @Override
    public void list(){
        for(int i=head-1; i>=0; --i){
            System.out.println(container[i]);
        }
    }

    @Override
    public boolean isEmpty(){
        return (head == 0);
    }

    private void resize(){
        capacity = resize * capacity;
        container = Arrays.copyOf(container, capacity);
    }
}
