package com.mambu.dataflow.exceptions;

import com.mambu.dataflow.serialization.model.User;

import java.io.*;
import java.util.Arrays;

public class ExceptionGenerationExample {
    public static void main(String[] args){
        User[] users = new User[2];
        User user0 = new User();
        user0.setId(1234);
        user0.setUsername("Andrei");
        user0.setRoles(new String[]{"guest", "user"});
        users[0] = user0;
        displayFormattedUsers(users);

        computeAverage(new int[]{});

        parseLineOfInts(Arrays.toString(new int[]{2, 3, 1}), ", ");

        Connection connection = new Connection();
        connection.setHost("localhost");
        connection.setPort(1234);
        connection.setName("testBD");
        saveConnection(connection);
    }

    public static void displayFormattedUsers(User[] users){
        for(int i=0; i< users.length; ++i) {
            try {
                System.out.println(users[i].getUsername() + " is " + Arrays.toString(users[i].getRoles()));
            } catch (NullPointerException e) {
                System.err.println("Invalid user");
            }
        }
    }

    public static int computeAverage(int[] values){
        int sum = 0;
        try{
            for(int i=0; i<values.length; ++i){
                sum += values[i];
            }
            return sum / values.length;

        } catch (ArithmeticException e){
            System.err.println("Empty input");
            return 0;
        }

    }

    public static int[] parseLineOfInts(String line, String delimiter){
        String[] tokens = line.split(delimiter);
        int[] values = new int[tokens.length];
        for(int i=0; i < tokens.length; ++i){
            try {
                values[i] = Integer.parseInt(tokens[i]);
            } catch (NumberFormatException e){
                System.err.println("Invalid number " + values[i]);
            }
        }

        return values;
    }

    public static void saveConnection(Connection connection){
        FileOutputStream fos = null;
        ObjectOutputStream oos = null;
        try {
            fos = new FileOutputStream("./src/main/java/com/mambu/dataflow/exceptions/connectionConfig.txt");
            oos = new ObjectOutputStream(fos);

            oos.writeObject(connection);
            oos.flush();
        } catch (FileNotFoundException e) {
            System.err.println("File not found");
        } catch (NotSerializableException e){
            System.err.println("Provided parameter is not serializable");
        } catch (IOException e) {
            System.err.println("Cannot write to file");
        } finally {
            try {
                oos.close();
                fos.close();
            } catch (IOException | NullPointerException e) {
                System.err.println("Can't close file");
            }
        }
    }
}
