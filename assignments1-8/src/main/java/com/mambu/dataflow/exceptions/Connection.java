package com.mambu.dataflow.exceptions;

import java.security.InvalidParameterException;

public class Connection {
    private String host;
    private int port;
    private String name;
    private String username;
    private String password;

    public Connection(){}

    public void setHost(String host) {
        this.host = host;
    }

    public void setPort(int port) {
        if(port < 0){
            throw new InvalidConnectionString("Port cannot be negative");
        }
        this.port = port;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getHost() {
        return host;
    }

    public int getPort() {
        return port;
    }

    public String getName() {
        return name;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public String getConnectionString(){
        if(host == null || name == null){
            throw new InvalidConnectionString("Please, provide all connection parameters (host, port, name)!");
        }
        if ((username != null && password == null) || (username == null && password != null)){
            throw new InvalidConnectionString("Please, provide both username and password!");
        }

        if (username != null) {
            return host + ":" + port + "/" + name + "/" + username + ":" + password;
        }
        else{
            return host + ":" + port + "/" + name;
        }
    }
}
