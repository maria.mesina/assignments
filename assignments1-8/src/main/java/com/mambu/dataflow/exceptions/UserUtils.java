package com.mambu.dataflow.exceptions;

import java.security.InvalidParameterException;

public class UserUtils {
    public static void main(String[] args){
        Connection c = new Connection();
        MyDatabase database = new MyDatabase(c);
        try {
            database.connect();
            database.execute("select * from users");
        } catch (InvalidConnectionString e){
            System.err.println("Provided connection settings are not valid");
        } finally {
            try{
                database.close();
            } catch (IllegalStateException e){
                System.err.println("Trying to execute an operation on a database in invalid state");
            }
        }
    }
}
