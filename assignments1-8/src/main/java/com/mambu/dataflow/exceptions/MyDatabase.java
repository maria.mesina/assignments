package com.mambu.dataflow.exceptions;

public class MyDatabase {
    private boolean alive = false;
    private Connection connection;
    public boolean isAlive() {
        return alive;
    }

    public MyDatabase(Connection connection){
        this.connection = connection;
    }

    public void execute(String statement){
        if (!this.alive){
            throw new IllegalStateException("Database connection is not alive!");
        }
        System.out.println("Executing " + statement);
    }

    public void connect(){
        this.alive = true;
        System.out.println("Connected to database with string" + connection.getConnectionString());
    }

    public void close(){
        if (!this.alive){
            throw new IllegalStateException("Database connection is not alive!");
        }
        alive = false;
    }
}
