package com.mambu.dataflow.exceptions;

public class InvalidConnectionString extends RuntimeException{
    public InvalidConnectionString(String message){
        super(message);
    }
}
