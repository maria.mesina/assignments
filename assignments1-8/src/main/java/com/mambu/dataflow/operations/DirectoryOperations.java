package com.mambu.dataflow.operations;


import java.io.File;

public class DirectoryOperations {
    public int[] countFilesWithExtension(File currentDir, String[] extensions){
        int[] filesCount = new int[extensions.length];
        File[] files = currentDir.listFiles();

        for(int i=0; i<files.length; ++i){
            if (files[i].isDirectory()){
                int[] subDirectoryFilesCount = countFilesWithExtension(files[i], extensions);
                for(int j=0; j<extensions.length; ++j){
                    filesCount[j] += subDirectoryFilesCount[j];
                }
            }
            else{
                for(int j=0; j<extensions.length; ++j){
                    if(files[i].getName().endsWith(extensions[j])){
                        filesCount[j] += 1;
                    }
                }
            }
        }

        return filesCount;
    }
}
