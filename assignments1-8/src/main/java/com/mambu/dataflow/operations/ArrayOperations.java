package com.mambu.dataflow.operations;

import java.io.*;

import com.mambu.fundamentals.FundamentalsMainClass;

public class ArrayOperations {
    public int[] readArray(String path) throws IOException {
        FileReader fileReader = new FileReader(path);
        BufferedReader bufferedReader = new BufferedReader(fileReader);

        String line = bufferedReader.readLine();
        int size = Integer.parseInt(line);
        int[] array = new int[size];

        line = bufferedReader.readLine();
        String[] tokens = line.split(" ");

        for(int i=0; i<size && i<tokens.length; ++i) {
            array[i] = Integer.parseInt(tokens[i]);
        }

        bufferedReader.close();
        fileReader.close();

        return array;
    }

    public void writeArray(String path, int[] array) throws IOException {
        FundamentalsMainClass fmc = new FundamentalsMainClass();
        int[] reversedArray = fmc.reverseArray(array);

        FileWriter fileWriter = new FileWriter(path);
        BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);

        bufferedWriter.write(String.valueOf(reversedArray.length));
        bufferedWriter.newLine();
        for(int i=0; i<reversedArray.length - 1; ++i){
            bufferedWriter.write(String.valueOf(reversedArray[i]));
            bufferedWriter.write(' ');
        }

        bufferedWriter.write(String.valueOf(reversedArray[array.length-1]));
        bufferedWriter.flush();

        bufferedWriter.close();
        fileWriter.close();
    }
}
