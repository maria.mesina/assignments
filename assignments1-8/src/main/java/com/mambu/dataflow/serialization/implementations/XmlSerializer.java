package com.mambu.dataflow.serialization.implementations;

import com.mambu.dataflow.serialization.InvalidInputException;
import com.mambu.dataflow.serialization.interfaces.UserSerializationInterface;
import com.mambu.dataflow.serialization.model.User;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.File;

public class XmlSerializer implements UserSerializationInterface {
    @Override
    public void serialize(User user, File file) throws InvalidInputException {
        if (user == null || file == null){
            throw new NullPointerException();
        }

        try{
            JAXBContext context = JAXBContext.newInstance(User.class);
            Marshaller marshaller = context.createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            marshaller.marshal(user, file);
        } catch (JAXBException e){
            throw new InvalidInputException("Error occurred while deserializing", e);
        }

    }

    @Override
    public User deserialize(File file) throws InvalidInputException {
        if (file == null){
            throw new NullPointerException();
        }

        try{
            JAXBContext context = JAXBContext.newInstance(User.class);
            Unmarshaller unmarshaller = context.createUnmarshaller();
            return (User) unmarshaller.unmarshal(file);
        } catch (JAXBException e){
            throw new InvalidInputException("Error occurred while deserializing", e);
        }

    }
}
