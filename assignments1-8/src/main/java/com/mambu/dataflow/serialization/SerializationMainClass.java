package com.mambu.dataflow.serialization;

import com.mambu.dataflow.serialization.implementations.JsonSerializer;
import com.mambu.dataflow.serialization.implementations.ObjectSerializer;
import com.mambu.dataflow.serialization.implementations.XmlSerializer;
import com.mambu.dataflow.serialization.interfaces.UserSerializationInterface;
import com.mambu.dataflow.serialization.model.User;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.Scanner;

public class SerializationMainClass {
    public static void main(String[] args) throws IOException, ClassNotFoundException {
        User user = new User();
        user.setId(32);
        user.setUsername("Andrei");
        user.setRoles(new String[]{"guest", "user", "moderator"});

        UserSerializationInterface usi;
        String userChoice;
        Scanner scanner = new Scanner(System.in);

        System.out.println("Please, provide desired serialization implementation: [json]/[xml]/[object]");
        userChoice = scanner.nextLine();
        if (userChoice.equalsIgnoreCase("json")){
            usi = new JsonSerializer();
        }
        else if (userChoice.equalsIgnoreCase("xml")){
            usi = new XmlSerializer();
        }
        else{
            usi = new ObjectSerializer();
        }

        File file = new File("./src/main/java/com/mambu/dataflow/serialization/test.txt");
        usi.serialize(user, file);
        User readUser = usi.deserialize(file);
        System.out.println("Id: " + user.getId() + "\tUsername: " + readUser.getUsername());
        System.out.println(Arrays.toString(readUser.getRoles()));
    }
}
