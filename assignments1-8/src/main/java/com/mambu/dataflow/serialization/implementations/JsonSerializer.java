package com.mambu.dataflow.serialization.implementations;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.exc.MismatchedInputException;
import com.mambu.dataflow.serialization.InvalidInputException;
import com.mambu.dataflow.serialization.interfaces.UserSerializationInterface;
import com.mambu.dataflow.serialization.model.User;

import java.io.File;
import java.io.IOException;

public class JsonSerializer implements UserSerializationInterface {
    private ObjectMapper objectMapper = new ObjectMapper();

    @Override
    public void serialize(User user, File file) throws IOException {
        if(user == null || file == null){
            throw new NullPointerException("Provided user argument is null");
        }

        objectMapper.writeValue(file, user);
    }

    @Override
    public User deserialize(File file) throws IOException {
         try{
             return objectMapper.readValue(file, User.class);
         } catch (MismatchedInputException e){
             throw new InvalidInputException("An error occurred while deserializing", e);
         } catch(IllegalArgumentException e){
             throw new NullPointerException("Provided argument is null");
         }
    }
}
