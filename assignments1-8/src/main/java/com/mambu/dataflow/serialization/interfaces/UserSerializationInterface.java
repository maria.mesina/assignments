package com.mambu.dataflow.serialization.interfaces;

import com.mambu.dataflow.serialization.InvalidInputException;
import com.mambu.dataflow.serialization.model.User;

import javax.xml.bind.JAXBException;
import java.io.File;
import java.io.IOException;

public interface UserSerializationInterface {
    void serialize(User user, File file) throws IOException, InvalidInputException;
    User deserialize(File file) throws IOException, InvalidInputException;
}
