package com.mambu.dataflow.serialization.implementations;

import com.mambu.dataflow.serialization.InvalidInputException;
import com.mambu.dataflow.serialization.interfaces.UserSerializationInterface;
import com.mambu.dataflow.serialization.model.User;

import java.io.*;

public class ObjectSerializer implements UserSerializationInterface {
    @Override
    public void serialize(User user, File file) throws IOException {
        if (user == null || file == null){
            throw new NullPointerException();
        }

        FileOutputStream fos = new FileOutputStream(file);
        ObjectOutputStream oos = new ObjectOutputStream(fos);

        oos.writeObject(user);
        oos.flush();

        oos.close();
        fos.close();
    }

    @Override
    public User deserialize(File file) throws IOException, InvalidInputException {
        if (file == null){
            throw new NullPointerException();
        }

        try (FileInputStream fis = new FileInputStream(file);
             ObjectInputStream ois = new ObjectInputStream(fis)) {
            return (User) ois.readObject();
        } catch (ClassNotFoundException | ClassCastException e) {
            throw new InvalidInputException("An error occurred while deserializing", e);
        }
    }
}
