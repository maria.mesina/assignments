package com.mambu.lambdas.signup;

import com.mambu.generics.model.User;

import java.util.concurrent.SubmissionPublisher;

public class RegisterService {
    private SubmissionPublisher<User> publisher;

    public RegisterService() {
        publisher = new SubmissionPublisher<>();
    }

    public SubmissionPublisher<User> getPublisher() {
        return publisher;
    }

    public void register(User user){
        publisher.submit(user);
    }
}