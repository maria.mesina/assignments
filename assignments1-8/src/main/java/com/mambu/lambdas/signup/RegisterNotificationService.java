package com.mambu.lambdas.signup;

import com.mambu.generics.model.User;

import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.Flow.Subscription;
import java.util.concurrent.Flow.Subscriber;

public class RegisterNotificationService implements Subscriber<User> {
    private Subscription subscription;
    private EmailService emailService;
    private List<User> consumedElements = new LinkedList<>();

    public RegisterNotificationService(EmailService emailService) {
        this.emailService = emailService;
    }

    public Subscription getSubscription() {
        return subscription;
    }

    public EmailService getEmailService() {
        return emailService;
    }

    public List<User> getConsumedElements() {
        return consumedElements;
    }

    @Override
    public void onSubscribe(Subscription subscription) {
        this.subscription = subscription;
        subscription.request(1);
    }

    @Override
    public void onNext(User item) {
        this.consumedElements.add(item);
        emailService.sendEmail("Welcome!", item.getEmail(), "Thanks for joining us on our exciting journey." +
                " Find more about next steps visiting our website");
        subscription.request(1);
    }

    @Override
    public void onError(Throwable throwable) {
        System.err.println(throwable.getMessage());
        throwable.printStackTrace();
    }

    @Override
    public void onComplete() {
        System.out.println("Registration is closed now");
    }
}
