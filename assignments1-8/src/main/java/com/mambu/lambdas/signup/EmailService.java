package com.mambu.lambdas.signup;

public class EmailService {
    private String emailAddress;

    public EmailService(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public void sendEmail(String subject, String destination, String text){
        System.out.println("Sending email from: " + emailAddress + "to: +" + destination +
                "with subject: " + subject);
    }
}
