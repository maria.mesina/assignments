package com.mambu.lambdas.functions;

import java.util.List;
import java.util.stream.Collectors;

public class StringUtils {
    public List<String> filterWordsStartingWith(List<String> words, char character){
        return words.stream().filter(w->w.startsWith(String.valueOf(character))).collect(Collectors.toList());
    }

    public List<String> filterWordsEndingWith(List<String> words, char character){
        return words.stream().filter(w->w.endsWith(String.valueOf(character))).collect(Collectors.toList());
    }

    public List<String> filterWordsLongerThan(List<String> words, int length){
        if (length < 0){
            throw new IllegalArgumentException("Length was " + length);
        }
        return words.stream().filter(w->w.length() > length).collect(Collectors.toList());
    }

    public List<String> filterWordsContainingVowels(List<String> words, int minCount){
        return words.stream().filter(w->countVowels(w) >= minCount).collect(Collectors.toList());
    }

    private int countVowels(String string){
        return (int) string.toLowerCase().chars().
                filter(c-> c=='a'|| c=='e' || c=='i' || c=='o' || c=='u').count();
    }

    public String joinWords(List<String> words, String delimiter){
        return words.stream().map(w->w.substring(0, 1).toUpperCase() + w.substring(1).toLowerCase())
                .collect(Collectors.joining(delimiter));
    }

}
