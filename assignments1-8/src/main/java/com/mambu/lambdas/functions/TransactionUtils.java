package com.mambu.lambdas.functions;

import com.mambu.lambdas.Averager;
import com.mambu.lambdas.model.Transaction;

import java.time.LocalDate;
import java.time.YearMonth;
import java.util.*;
import java.util.stream.Collectors;

public class TransactionUtils {
    public double getAverage(List<Transaction> transactions){
        double average = transactions.stream()
                .flatMapToDouble(t->t.getProducts().stream().mapToDouble(p->p.getPrice()))
                .reduce(0, (partial, value)->partial + value / transactions.size());

        return average;
    }

    public List<Transaction> getTop3ByAmount(List<Transaction> transactions){
        return transactions.stream().
                sorted((x, y)->{
                    double tmp = x.getProducts().stream().mapToDouble(p->
                            p.getPrice()).sum() - y.getProducts().stream().mapToDouble(p-> p.getPrice()).sum();
                    if(tmp < 0) {
                        return 1;
                    }
                    if(tmp > 0) {
                        return -1;
                    }
                    return 0;
                })
                .limit(3).collect(Collectors.toList());
    }

    public double getAverageOfMonth(List<Transaction> transactions, YearMonth date){
        Optional<Averager> averager = transactions.stream()
                .filter(t->
                    {
                        YearMonth transactionYm = YearMonth.of(t.getDate().getYear(), t.getDate().getMonth());
                        return transactionYm.equals(date);
                    })
                .mapToDouble(t->t.getProducts().stream().mapToDouble(p->p.getPrice()).sum())
                .mapToObj(value->new Averager(value))
                .reduce((partial, value)->partial.apply(value));

        return averager.map(Averager::getAvg).orElse(0.0);
    }

    public Map<String, List<String>> groupTransactions(List<Transaction> transactions){
        return transactions.stream().collect(Collectors.groupingBy(t->String.valueOf(t.getUserId()),
                Collectors.mapping(t -> String.valueOf(t.getId()), Collectors.toList())));
    }

}
