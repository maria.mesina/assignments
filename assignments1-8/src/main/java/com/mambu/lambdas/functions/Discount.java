package com.mambu.lambdas.functions;

import com.mambu.lambdas.model.Product;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Discount {
    public List<Product> applyDiscount(List<Product> products, String category){
        Stream<Product> stream = products.stream().filter(p->p.getCategory().equals(category))
                .map(p-> {
                    Product c = new Product(p);
                    c.setPrice(p.getPrice() * 0.9);
                    return c;
                });
        return stream.collect(Collectors.toList());
    }
}