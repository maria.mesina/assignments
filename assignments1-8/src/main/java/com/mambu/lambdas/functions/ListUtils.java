package com.mambu.lambdas.functions;

import com.mambu.lambdas.Averager;
import com.mambu.lambdas.model.Product;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.DoubleStream;
import java.util.stream.Stream;

public class ListUtils {
    public int getMax(List<Integer> list){
        Optional<Integer> max = list.stream().max(Integer::compareTo);
        return max.orElse(0);
    }

    public int getMin(List<Integer> list) {
        Optional<Integer> min = list.stream().min(Integer::compareTo);
        return min.orElse(0);
    }

    public double getAvg(List<Integer> list) {
//        ArrayList<Double> avg = list.stream()
//                .map(x->
//                {
//                    ArrayList<Double> tmp = new ArrayList<>();
//                    tmp.add(x.doubleValue());
//                    tmp.add(1.0);
//                    return tmp;
//                })
//                .reduce((partial, value) ->
//                {
//                    ArrayList<Double> tmp = new ArrayList<>();
//                    tmp.add(partial.get(0) + value.get(0));
//                    tmp.add(partial.get(1) + value.get(1));
//                    return tmp;
//                }).orElse(new ArrayList<>());
//
//        return avg.isEmpty()? 0: avg.get(0) / avg.get(1);
        Optional<Averager> averager = list.stream().map(Averager::new).reduce(Averager::apply);
        return averager.map(Averager::getAvg).orElse(0.0);
    }

    public String iterateForEachIterable(List<Product> products){
        StringBuilder res = new StringBuilder();
        products.forEach(p->res.append(p.toString()).append("\n"));
        return res.toString();
    }

    public String iterateForEachStream(List<Product> products){
        StringBuilder res = new StringBuilder();
        products.stream().forEach(p->res.append(p.toString()).append("\n"));
        return res.toString();
    }

    public String iterateReduce(List<Product> products){
        List<String> s = products.stream().map(p->p.toString()).collect(Collectors.toList());
        return products.stream().map(p->p.toString()).
                reduce("", (partialResult, value)->partialResult.isEmpty()? value : partialResult +"\n"+ value);
    }

    public String iterateCollectors(List<Product> products){
        return products.stream().map(p->p.toString()).collect(Collectors.joining("\n"));
    }
}
