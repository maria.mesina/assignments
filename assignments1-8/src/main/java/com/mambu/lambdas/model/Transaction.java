package com.mambu.lambdas.model;

import com.mambu.generics.model.User;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

public class Transaction implements Comparable<Transaction>{
    private int id;
    private int userId;
    private List<Product> products = new LinkedList<>();
    private LocalDate date;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public List<Product> getProducts() {
        return products;
    }

    public void setProducts(List<Product> products) {
        this.products = products;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public void addProduct(Product product){
        this.products.add(product);
    }

    private double getAmount(){
        double sum = 0;
        for(int i=0; i<products.size(); ++i){
            sum += products.get(i).getPrice();
        }

        return sum;
    }

    @Override
    public int compareTo(Transaction o) {
        double thisSum = this.getAmount();
        double otherSum = o.getAmount();

        return Double.compare(thisSum, otherSum);
    }

    @Override
    public boolean equals(Object o) {
        if (o == this)
            return true;
        if (!(o instanceof Transaction))
            return false;

        Transaction other = (Transaction) o;
        return (this.id == other.id) && (this.userId == other.userId) && (this.products.equals(other.products));
    }
}
