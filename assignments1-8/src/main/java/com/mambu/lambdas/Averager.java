package com.mambu.lambdas;

import java.util.function.BinaryOperator;

public class Averager{
    private double sum = 0;
    private int cnt = 0;

    public Averager() {}

    public Averager(double x){
        this.sum = x;
        this.cnt = 1;
    }

    public double getAvg(){
        return sum / cnt;
    }

    public Averager apply(Averager other) {
        Averager result = new Averager();

        result.sum = this.sum + other.sum;
        result.cnt = this.cnt + other.cnt;

        return result;
    }
}
