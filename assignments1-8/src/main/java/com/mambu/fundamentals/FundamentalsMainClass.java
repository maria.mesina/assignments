package com.mambu.fundamentals;

public class FundamentalsMainClass {

    public int countOccurrences(int[] array, int element){
        int occurences = 0;

        for(int i=0; i<array.length; ++i){
            if (array[i] == element){
                occurences++;
            }
        }

        return occurences;
    }

    public int findElement(int[] array, int element){
        for(int i=0; i<array.length; ++i){
            if (array[i] == element){
                return i;
            }
        }

        return -1;
    }

    public int[] reverseArray(int[] array){
        int[] reversed = new int[array.length];

        for(int i=0; i<array.length; ++i){
            reversed[i] = array[array.length - 1 - i];
        }

        return reversed;
    }

    public int[] bubbleSort(int[] array){
        boolean swaps = true;
        int[] sortedArray = new int[array.length];
        System.arraycopy(array, 0, sortedArray, 0, array.length);

        for (int i=0; i<sortedArray.length && swaps; ++i){
            swaps = false;
            for(int j=0; j<sortedArray.length - 1; ++j){
                if (sortedArray[j] > sortedArray[j+1]){
                    swaps = true;
                    int temp = sortedArray[j];
                    sortedArray[j] = sortedArray[j+1];
                    sortedArray[j+1] = temp;
                }
            }
        }

        return sortedArray;
    }
}
