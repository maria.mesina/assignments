package com.mambu.patterns.strategy;

public class ReplaceAllOccurrences implements TextParser{
    private String word;

    public ReplaceAllOccurrences(String word){
        this.word = word;
    }

    @Override
    public String parse(String text) {
        String reversedWord = "";
        for(int i=word.length()-1; i>=0; --i){
            reversedWord += word.charAt(i);
        }

        String[] splitWords = text.split(word);
        String result = "";
        for(int i=0; i<splitWords.length - 1; ++i){
            result += splitWords[i];
            result += reversedWord;
        }

        result += splitWords[splitWords.length - 1];
        if (text.endsWith(word)){
            result += reversedWord;
        }

        return result;
    }
}
