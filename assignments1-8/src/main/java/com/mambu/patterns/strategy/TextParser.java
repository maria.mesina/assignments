package com.mambu.patterns.strategy;

public interface TextParser {
    String parse(String text);
}
