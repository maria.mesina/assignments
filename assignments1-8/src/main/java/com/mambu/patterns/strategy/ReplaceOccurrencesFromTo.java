package com.mambu.patterns.strategy;

public class ReplaceOccurrencesFromTo implements TextParser {
    private String word;
    private int startIndex;
    private int endIndex;

    public ReplaceOccurrencesFromTo(String word, int startIndex, int endIndex){

        this.word = word;
        this.startIndex = startIndex;
        this.endIndex = endIndex;
    }

    @Override
    public String parse(String text) {
        if (endIndex < 0 || endIndex > text.length() || endIndex < startIndex){
            throw new IndexOutOfBoundsException("Index out of bounds for value " + endIndex + " for endIndex");
        }

        if (word.length() == 0){
            return text;
        }

        String reversedWord = "";
        for(int i=word.length()-1; i>=0; --i){
            reversedWord += word.charAt(i);
        }

        String result = text.substring(0, startIndex);
        int lastIndex = startIndex;
        int newIndex;
        do {
            newIndex = text.indexOf(word, lastIndex);
            if(newIndex != -1 && newIndex + word.length() <= endIndex){
                result += text.substring(lastIndex, newIndex);
                result += reversedWord;
                lastIndex = newIndex + word.length();
                newIndex = text.indexOf(word, lastIndex);
            }
        } while(newIndex != -1 && newIndex + word.length() <= endIndex);

        result += text.substring(lastIndex);
        return result;
    }
}
