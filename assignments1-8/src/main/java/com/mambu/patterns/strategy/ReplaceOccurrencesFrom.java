package com.mambu.patterns.strategy;

public class ReplaceOccurrencesFrom implements TextParser{
    private String word;
    private int startIndex;

    public ReplaceOccurrencesFrom(String word, int startIndex){
        this.word = word;
        this.startIndex = startIndex;
    }

    @Override
    public String parse(String text) {
        if (word.length() == 0){
            return text;
        }

        String reversedWord = "";
        for(int i=word.length()-1; i>=0; --i){
            reversedWord += word.charAt(i);
        }

        String result = text.substring(0, startIndex);
        int lastIndex = startIndex;
        int newIndex;
        while((newIndex = text.indexOf(word, lastIndex)) != -1){
            result += text.substring(lastIndex, newIndex);
            result += reversedWord;
            lastIndex = newIndex + word.length();
        }

        result += text.substring(lastIndex);
        return result;
    }
}
