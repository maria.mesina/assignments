package com.mambu.patterns.strategy;

public class TextProcessorStrategy {
    private TextParser parser;

    public TextProcessorStrategy(TextParser parser){
        this.parser = parser;
    }

    public TextParser getParser() {
        return parser;
    }

    public String process(String text){
        return this.parser.parse(text);
    }
}
