package com.mambu.patterns.facade;

import com.mambu.patterns.cor.model.Transaction;

public class PaymentProcessor {
    public void pay(Transaction transaction){
        BalanceManager balanceManager = new BalanceManager();
        TransactionValidator transactionValidator = new TransactionValidator();

        if(transaction.getSource() == null || transaction.getDestination() == null){
            throw new NullPointerException("Source account or destination account not found");
        }

        if(transaction.getSource().equals(transaction.getDestination())){
            throw new IllegalArgumentException("Cannot transfer money between same account");
        }

        boolean  transactionValid = transactionValidator.isValid(transaction);
        if (!transactionValid){
            throw new IllegalArgumentException("Invalid transaction!");
        }

        balanceManager.changeBalance(transaction);
    }
}
