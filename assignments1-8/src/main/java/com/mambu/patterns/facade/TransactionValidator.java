package com.mambu.patterns.facade;

import com.mambu.patterns.cor.model.Account;
import com.mambu.patterns.cor.model.Transaction;

public class TransactionValidator {
    public boolean isValid(Transaction transaction){
        if(!transaction.getSource().isActive() || !transaction.getDestination().isActive()){
            return false;
        }

        if (transaction.getAmount() <= 0){
            return false;
        }

        if (transaction.getSource().getBalance() < transaction.getAmount()){
            return false;
        }

        if(transaction.getSource().getTransactionLimit() < transaction.getAmount()){
            return false;
        }

        return true;
    }
}
