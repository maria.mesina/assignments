package com.mambu.patterns.facade;

import com.mambu.patterns.cor.model.Account;
import com.mambu.patterns.cor.model.Transaction;

public class BalanceManager {
    public void changeBalance(Transaction transaction){
        transaction.getSource().setBalance(transaction.getSource().getBalance() - transaction.getAmount());
        transaction.getDestination().setBalance(transaction.getDestination().getBalance() + transaction.getAmount());
    }
}
