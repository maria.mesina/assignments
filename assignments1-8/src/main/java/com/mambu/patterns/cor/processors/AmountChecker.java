package com.mambu.patterns.cor.processors;

import com.mambu.patterns.cor.model.Transaction;

public class AmountChecker extends TransactionProcessor{
    @Override
    public boolean process(Transaction transaction) {
        return transaction.getAmount() > 0;
    }
}
