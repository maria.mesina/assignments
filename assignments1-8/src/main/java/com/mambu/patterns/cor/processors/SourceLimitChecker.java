package com.mambu.patterns.cor.processors;

import com.mambu.patterns.cor.model.Transaction;

public class SourceLimitChecker extends TransactionProcessor{
    @Override
    public boolean process(Transaction transaction) {
        return transaction.getSource().getTransactionLimit() >= transaction.getAmount();
    }
}
