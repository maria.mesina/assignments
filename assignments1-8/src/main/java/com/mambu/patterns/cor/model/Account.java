package com.mambu.patterns.cor.model;

public class Account {
    private long id;
    private String owner;
    private long balance;
    private long transactionLimit;
    private String currency;
    private boolean active;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public long getBalance() {
        return balance;
    }

    public void setBalance(long balance) {
        this.balance = balance;
    }

    public long getTransactionLimit() {
        return transactionLimit;
    }

    public void setTransactionLimit(long transactionLimit) {
        this.transactionLimit = transactionLimit;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    @Override
    public boolean equals(Object o){
        if (o == null) {
            return false;
        }

        if (!(o instanceof Account)) {
            return false;
        }

        Account other = (Account) o;
        return this.id == other.id &&
                this.owner.equals(other.owner) &&
                this.balance == other.balance &&
                this.active == other.active &&
                this.transactionLimit == other.transactionLimit &&
                this.currency.equals(other.currency);
    }
}
