package com.mambu.patterns.cor.processors;

import com.mambu.patterns.cor.model.Transaction;

public class SourceBalanceChecker extends TransactionProcessor{
    @Override
    public boolean process(Transaction transaction) {
        return transaction.getSource().getBalance() - transaction.getAmount() >= 0;
    }
}
