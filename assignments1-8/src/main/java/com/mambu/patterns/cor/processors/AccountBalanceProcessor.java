package com.mambu.patterns.cor.processors;

import com.mambu.patterns.cor.model.Transaction;

public class AccountBalanceProcessor extends TransactionProcessor{
    @Override
    public boolean process(Transaction transaction) {
        transaction.getSource().setBalance(transaction.getSource().getBalance() - transaction.getAmount());
        transaction.getDestination().setBalance(transaction.getDestination().getBalance() + transaction.getAmount());

        return true;
    }
}
