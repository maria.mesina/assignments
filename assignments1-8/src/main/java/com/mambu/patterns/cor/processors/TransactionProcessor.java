package com.mambu.patterns.cor.processors;

import com.mambu.patterns.cor.model.Transaction;

public abstract class TransactionProcessor {
    protected TransactionProcessor next;

    public void setNext(TransactionProcessor next) {
        this.next = next;
    }
    public TransactionProcessor getNext(){
        return next;
    }

    public boolean apply(Transaction transaction){
        boolean result = this.process(transaction);
        if(!result){
            return false;
        }

        if(this.next != null){
            return this.next.apply(transaction);
        }
        else{
            return true;
        }
    }

    public abstract boolean process(Transaction transaction);
}
