package com.mambu.patterns.entityManager.oracle;

import com.mambu.patterns.entityManager.interfaces.DatabaseTransaction;

public class OracleTransaction implements DatabaseTransaction {

    @Override
    public void begin() { }

    @Override
    public void commit() { }

    @Override
    public void rollback() { }
}
