package com.mambu.patterns.entityManager.interfaces;

public interface DatabaseTransaction {
    void begin();
    void commit();
    void rollback();
}
