package com.mambu.patterns.entityManager.mongodb;

import com.mambu.patterns.entityManager.interfaces.DatabaseDatatype;

public class MongoDatatype implements DatabaseDatatype {
    String content;

    public MongoDatatype(String c){
        this.content = c;
    }

    @Override
    public String getStringRepresentation() {
        return this.content;
    }
}
