package com.mambu.patterns.entityManager.oracle;

import com.mambu.patterns.entityManager.interfaces.DatabaseDatatype;

public class OracleDatatype implements DatabaseDatatype {
    String content;

    public OracleDatatype(String c){
        this.content = c;
    }

    @Override
    public String getStringRepresentation() {
        return content;
    }
}
