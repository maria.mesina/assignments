package com.mambu.patterns.entityManager.oracle;

import com.mambu.patterns.entityManager.interfaces.DatabaseFactory;
import com.mambu.patterns.entityManager.interfaces.DatabaseConnection;
import com.mambu.patterns.entityManager.interfaces.DatabaseTransaction;
import com.mambu.patterns.entityManager.interfaces.DatatypeConvertor;

public class OracleDatabaseFactory implements DatabaseFactory {
    @Override
    public DatabaseConnection createConnection(String connectionString) {
        return new OracleConnection(connectionString);
    }

    @Override
    public DatabaseTransaction createTransaction() {
        return new OracleTransaction();
    }

    @Override
    public DatatypeConvertor createConvertor() {
        return new OracleConvertor();
    }
}
