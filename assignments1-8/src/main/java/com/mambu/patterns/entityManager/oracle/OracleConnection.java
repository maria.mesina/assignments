package com.mambu.patterns.entityManager.oracle;

import com.mambu.patterns.entityManager.interfaces.DatabaseConnection;
import com.mambu.patterns.entityManager.interfaces.DatabaseDatatype;

public class OracleConnection implements DatabaseConnection {
    private final String connectionString;

    public OracleConnection(String connectionString){
        this.connectionString = connectionString;
    }

    @Override
    public DatabaseDatatype executeQuery(String query) {
        return new OracleDatatype(query);
    }
}
