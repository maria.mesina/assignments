package com.mambu.patterns.entityManager.interfaces;

public interface DatabaseDatatype {
    String getStringRepresentation();
}
