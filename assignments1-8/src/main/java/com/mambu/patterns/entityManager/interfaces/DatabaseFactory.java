package com.mambu.patterns.entityManager.interfaces;

public interface DatabaseFactory {
    DatabaseConnection createConnection(String connectionString);
    DatabaseTransaction createTransaction();
    DatatypeConvertor createConvertor();
}