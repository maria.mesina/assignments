package com.mambu.patterns.entityManager.interfaces;

public interface DatatypeConvertor {
    double convertToDouble(DatabaseDatatype data);
}
