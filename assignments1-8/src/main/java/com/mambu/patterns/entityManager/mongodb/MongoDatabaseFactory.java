package com.mambu.patterns.entityManager.mongodb;

import com.mambu.patterns.entityManager.interfaces.*;

public class MongoDatabaseFactory implements DatabaseFactory {
    @Override
    public DatabaseConnection createConnection(String connectionString) {
        return new MongoConnection(connectionString);
    }

    @Override
    public DatabaseTransaction createTransaction() {
        return new MongoTransaction();
    }

    @Override
    public DatatypeConvertor createConvertor() {
        return new MongoConvertor();
    }
}
