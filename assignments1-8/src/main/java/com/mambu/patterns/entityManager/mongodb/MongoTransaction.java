package com.mambu.patterns.entityManager.mongodb;

import com.mambu.patterns.entityManager.interfaces.DatabaseTransaction;

public class MongoTransaction implements DatabaseTransaction {
    @Override
    public void begin() { }

    @Override
    public void commit() { }

    @Override
    public void rollback() { }
}
