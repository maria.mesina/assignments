package com.mambu.patterns.entityManager;

import com.mambu.patterns.entityManager.interfaces.DatabaseFactory;
import com.mambu.patterns.entityManager.mongodb.MongoDatabaseFactory;
import com.mambu.patterns.entityManager.oracle.OracleDatabaseFactory;

public class DomainSpecificFactory {
    public DatabaseFactory getDomainSpecificFactory(String config){
        if(config.equals("mongodb")){
            return new MongoDatabaseFactory();
        }
        else if (config.equals("oracle")){
            return new OracleDatabaseFactory();
        } else{
            throw new IllegalArgumentException("Database " + config + "is not supported");
        }
    }
}
