package com.mambu.patterns.entityManager.mongodb;

import com.mambu.patterns.entityManager.interfaces.DatabaseDatatype;
import com.mambu.patterns.entityManager.interfaces.DatatypeConvertor;

public class MongoConvertor implements DatatypeConvertor {
    @Override
    public double convertToDouble(DatabaseDatatype data) {
        return Double.parseDouble(data.getStringRepresentation());
    }
}
