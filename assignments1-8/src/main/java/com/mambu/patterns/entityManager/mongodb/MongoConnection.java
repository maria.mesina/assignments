package com.mambu.patterns.entityManager.mongodb;

import com.mambu.patterns.entityManager.interfaces.DatabaseConnection;
import com.mambu.patterns.entityManager.interfaces.DatabaseDatatype;

public class MongoConnection implements DatabaseConnection {
    private final String connectionString;

    public MongoConnection(String connectionString){
        this.connectionString = connectionString;
    }

    @Override
    public DatabaseDatatype executeQuery(String query) {
        return new MongoDatatype(query);
    }
}
