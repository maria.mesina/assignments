package com.mambu.patterns.entityManager.oracle;

import com.mambu.patterns.entityManager.interfaces.DatabaseDatatype;
import com.mambu.patterns.entityManager.interfaces.DatatypeConvertor;

public class OracleConvertor implements DatatypeConvertor {
    @Override
    public double convertToDouble(DatabaseDatatype data) {
        return Double.parseDouble(data.getStringRepresentation());
    }
}
