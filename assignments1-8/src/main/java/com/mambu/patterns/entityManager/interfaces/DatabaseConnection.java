package com.mambu.patterns.entityManager.interfaces;

public interface DatabaseConnection {
    DatabaseDatatype executeQuery(String query);
}
