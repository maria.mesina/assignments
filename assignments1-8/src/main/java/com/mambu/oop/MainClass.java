package com.mambu.oop;

import com.mambu.oop.databases.implementations.MongoDbOperations;
import com.mambu.oop.databases.implementations.MySqlOperations;
import com.mambu.oop.databases.implementations.OracleOperations;
import com.mambu.oop.databases.interfaces.DatabaseOperations;
import com.mambu.oop.databases.model.Entity;
import com.mambu.oop.persons.implementations.Janitor;
import com.mambu.oop.persons.implementations.Professor;
import com.mambu.oop.persons.implementations.SecurityGuard;
import com.mambu.oop.persons.implementations.Student;
import com.mambu.oop.persons.parent.Person;
import com.mambu.oop.utils.TextProcessor;
import com.mambu.oop.vehicles.implementations.*;
import com.mambu.oop.vehicles.interfaces.Flyable;
import com.mambu.oop.vehicles.interfaces.Rechargeable;

import java.util.Scanner;

public class MainClass {
    public static void main(String[] args){
        createAndTestVehicles();
        createAndTestPersons();
        selectAndUseDatabase();
    }

    public static void createAndTestVehicles(){
        Vehicle[] vehicles = new Vehicle[7];
        Flyable[] flyables = new Flyable[2];
        Rechargeable[] rechargeables = new Rechargeable[2];

        Vehicle vehicle = new Vehicle("Maria", 140);
        vehicles[0] = vehicle;

        Car car = new Car("Ion", 160, 4);
        vehicles[1] = car;

        ElectricCar electricCar = new ElectricCar("Vasile", 150, 4, 75);
        vehicles[2] = electricCar;
        rechargeables[0] = electricCar;

        Bus bus = new Bus("CTP Iasi", 100, "7");
        vehicles[3] = bus;

        Trolleybus trolleybus = new Trolleybus("CTP Iasi", 80, "43C");
        vehicles[4] = trolleybus;
        rechargeables[1] = trolleybus;

        Airplane airplane = new Airplane("WizzAir", 270, 2);
        vehicles[5] = airplane;
        flyables[0] = airplane;

        JetAirplane jetAirplane = new JetAirplane("Elon Musk", 450, 2, "turbojet");
        vehicles[6] = jetAirplane;
        flyables[1] = jetAirplane;

        for(int i=0; i<vehicles.length; ++i){
            vehicles[i].accelerate();
            vehicles[i].brake();
        }

        for(int i=0; i< flyables.length; ++i){
            flyables[i].fly();
            flyables[i].land();
        }


        for(int i=0; i< rechargeables.length; ++i){
            rechargeables[i].recharge();
        }

    }

    public static void createAndTestPersons(){
        Professor testProfessor = new Professor("Mihai", "Zaharia", 1965, 1,
                "Distributed systems", "Associate professor");
        testProfessor.teach();
        testProfessor.gradeAssignment();

        Student testStudent = new Student("Maria", "Mesina", 1998, 2, 8.93,
                new String[]{"Distributed systems", "Databases", "OOP"});
        testStudent.study();
        testStudent.takeNotes();

        Janitor testJanitor = new Janitor("Bianca", "Talmasag", 1995, 3,
                new String[]{"broom", "gloves", "mop", "bucket"});
        testJanitor.clean();
        testJanitor.waterPlants();

        SecurityGuard testSecurityGuard = new SecurityGuard("Andrei", "Ionescu", 1984, 4,
                "A314BCN", "Middle");
        testSecurityGuard.monitor();
        testSecurityGuard.escort();

        Person[] persons = new Person[4];
        persons[0] = testProfessor;
        persons[1] = testStudent;
        persons[2] = testJanitor;
        persons[3] = testSecurityGuard;

        for(int i=0; i<persons.length; ++i){
            persons[i].introduceYourself();
            persons[i].walk();
            persons[i].think();
        }
    }

    public static void selectAndUseDatabase(){
        Scanner userInputScanner = new Scanner(System.in);
        System.out.println("Please, provide desired database implementation: [Oracle]/[MySql]/[MongoDb]");

        String userChoice;
        userChoice = userInputScanner.nextLine();

        DatabaseOperations dbOperations;

        if(userChoice.equalsIgnoreCase("Oracle")){
            dbOperations = new OracleOperations();
        }
        else if(userChoice.equalsIgnoreCase("MySql")){
            dbOperations = new MySqlOperations();
        }
        else{
            dbOperations = new MongoDbOperations();
        }

        Entity testEntity = new Entity();
        testEntity.setId(1234);
        testEntity.setProperty("Test entity");

        dbOperations.createEntity(testEntity);
        Entity testEntityCopy = dbOperations.readEntity(testEntity.getId());

        testEntity.setProperty("Another property");
        dbOperations.updateEntity(testEntity);

        dbOperations.deleteEntity(testEntity.getId());
    }
}
