package com.mambu.oop.persons.implementations;

import com.mambu.oop.persons.parent.Person;

public class SecurityGuard extends Person {
    private String licenseNumber;
    private String rank;

    public SecurityGuard(String name, String surname, int birthYear, long cnp,
                         String licenseNumber, String rank) {
        super(name, surname, birthYear, cnp);
        this.licenseNumber = licenseNumber;
        this.rank = rank;
    }

    public void setLicenseNumber(String licenseNumber) {
        this.licenseNumber = licenseNumber;
    }

    public void setRank(String rank) {
        this.rank = rank;
    }

    public String getLicenseNumber() {
        return licenseNumber;
    }

    public String getRank() {
        return rank;
    }

    @Override
    public void introduceYourself() {
        System.out.println("My name is " + this.getName() + " " + this.getSurname() +
                ". I'm a security guard.");
    }

    @Override
    public void walk() {
        System.out.println("I'm walking to security room");
    }

    @Override
    public void think() {
        System.out.println("I'm thinking about security");
    }

    public void monitor(){
        System.out.println("I'm monitoring the gate");
    }

    public void escort(){
        System.out.println("I escort this student off property");
    }
}
