package com.mambu.oop.persons.parent;

public abstract class Person {
    private String name;
    private String surname;
    private int birthYear;
    private long cnp;

    public Person(String name, String surname, int birthYear, long cnp){
        this.name = name;
        this.surname = surname;
        this.birthYear = birthYear;
        this.cnp = cnp;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public void setBirthYear(int birthYear){
        this.birthYear = birthYear;
    }

    public void setCnp(long cnp) {
        this.cnp = cnp;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public int getBirthYear() {
        return birthYear;
    }

    public long getCnp() {
        return cnp;
    }

    public abstract void introduceYourself();

    public abstract void walk();

    public abstract void think();
}
