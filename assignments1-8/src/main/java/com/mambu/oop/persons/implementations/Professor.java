package com.mambu.oop.persons.implementations;

import com.mambu.oop.persons.parent.Person;

import java.util.Random;

public class Professor extends Person {
    private String course;
    private String title;

    public Professor(String name, String surname, int birthYear, long cnp,
                     String course, String title) {
        super(name, surname, birthYear, cnp);
        this.course = course;
        this.title = title;
    }

    public void setCourse(String course) {
        this.course = course;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCourse() {
        return course;
    }

    public String getTitle(){
        return title;
    }

    @Override
    public void introduceYourself() {
        System.out.println("Hi, I'm " + this.title + " " + this.getName() + " " +
                this.getSurname() + ". I teach " + this.course);
    }

    @Override
    public void walk() {
        System.out.println("I'm walking to the university");
    }

    @Override
    public void think() {
        System.out.println("I'm thinking about " + this.course);
    }

    public void teach(){
        System.out.println("Welcome to the " + this.course + " course. " +
                "I hope you're ready to learn!");
    }

    public int gradeAssignment(){
        Random rand = new Random();

        return rand.nextInt(10) + 1;
    }
}
