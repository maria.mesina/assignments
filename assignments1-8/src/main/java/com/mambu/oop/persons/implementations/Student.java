package com.mambu.oop.persons.implementations;

import com.mambu.oop.persons.parent.Person;

public class Student extends Person {
    private double grade;
    private String[] enrolledCourses;

    public Student(String name, String surname, int birthYear, long cnp,
                   double grade, String[] enrolledCourses) {
        super(name, surname, birthYear, cnp);
        this.grade = grade;
        this.enrolledCourses = enrolledCourses;
    }

    public void setGrade(double grade) {
        this.grade = grade;
    }

    public void setEnrolledCourses(String[] enrolledCourses) {
        this.enrolledCourses = enrolledCourses;
    }

    public double getGrade() {
        return grade;
    }

    public String[] getEnrolledCourses() {
        return enrolledCourses;
    }

    @Override
    public void introduceYourself() {
        System.out.println("Hi, I'm " + this.getName() + " " + this.getName() +
                ". I'm a student");
    }

    @Override
    public void walk() {
        System.out.println("I'm going to the campus");
    }

    @Override
    public void think() {
        System.out.println("I'm thinking about my studies");
    }

    public void study(){
        System.out.println("I'm studying now");
    }

    public void takeNotes(){
        System.out.println("I'm taking notes");
    }
}
