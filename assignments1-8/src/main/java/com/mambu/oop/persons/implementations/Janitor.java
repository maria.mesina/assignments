package com.mambu.oop.persons.implementations;

import com.mambu.oop.persons.parent.Person;

public class Janitor extends Person {
    private String[] equipment;

    public Janitor(String name, String surname, int birthYear, long cnp,
                   String[] equipment) {
        super(name, surname, birthYear, cnp);
        this.equipment = equipment;
    }

    public void setEquipment(String[] equipment) {
        this.equipment = equipment;
    }

    public String[] getEquipment() {
        return equipment;
    }

    @Override
    public void introduceYourself() {
        System.out.println("My name is " + this.getName() + " " + this.getSurname() +
                ". I'm a janitor.");
    }

    @Override
    public void walk() {
        System.out.println("I'm walking towards janitor's closet");
    }

    @Override
    public void think() {
        System.out.println("I'm thinking about watering the plants");
    }

    public void clean(){
        System.out.println("I'm cleaning the desks");
    }

    public void waterPlants(){
        System.out.println("I'm watering the plants");
    }
}
