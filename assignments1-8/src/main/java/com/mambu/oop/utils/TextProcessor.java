package com.mambu.oop.utils;

public class TextProcessor {
    public String reverseWordInText(String text, String word){
        String reversedWord = "";
        for(int i=word.length()-1; i>=0; --i){
            reversedWord += word.charAt(i);
        }

        String[] splitWords = text.split(word);
        String result = "";
        for(int i=0; i<splitWords.length - 1; ++i){
            result += splitWords[i];
            result += reversedWord;
        }

        result += splitWords[splitWords.length - 1];
        if (text.endsWith(word)){
            result += reversedWord;
        }

        return result;
    }

    public String reverseWordInText(String text, String word, int startIndex){
        if (word.length() == 0){
            return text;
        }

        String reversedWord = "";
        for(int i=word.length()-1; i>=0; --i){
            reversedWord += word.charAt(i);
        }

        String result = text.substring(0, startIndex);
        int lastIndex = startIndex;
        int newIndex;
        while((newIndex = text.indexOf(word, lastIndex)) != -1){
            result += text.substring(lastIndex, newIndex);
            result += reversedWord;
            lastIndex = newIndex + word.length();
        }

        result += text.substring(lastIndex);
        return result;
    }

    public String reverseWordInText(String text, String word, int startIndex, int endIndex){
        if (endIndex < 0 || endIndex > text.length() || endIndex < startIndex){
            throw new IndexOutOfBoundsException("Index out of bounds for value " + endIndex + " for endIndex");
        }

        if (word.length() == 0){
            return text;
        }

        String reversedWord = "";
        for(int i=word.length()-1; i>=0; --i){
            reversedWord += word.charAt(i);
        }

        String result = text.substring(0, startIndex);
        int lastIndex = startIndex;
        int newIndex;
        do {
            newIndex = text.indexOf(word, lastIndex);
            if(newIndex != -1 && newIndex + word.length() <= endIndex){
                result += text.substring(lastIndex, newIndex);
                result += reversedWord;
                lastIndex = newIndex + word.length();
                newIndex = text.indexOf(word, lastIndex);
            }
        } while(newIndex != -1 && newIndex + word.length() <= endIndex);

        result += text.substring(lastIndex);
        return result;
    }
}
