package com.mambu.oop.vehicles.implementations;

public class Car extends Vehicle{
    private int noWheels;

    public Car(String owner, int maxSpeed, int noWheels){
        super(owner, maxSpeed);
        this.noWheels = noWheels;
    }

    public void setNoWheels(int noWheels) {
        this.noWheels = noWheels;
    }

    public int getNoWheels() {
        return noWheels;
    }

    @Override
    public void accelerate() {
        System.out.println("Car accelerates");
    }

    @Override
    public void brake() {
        System.out.println("Car brakes");
    }
}
