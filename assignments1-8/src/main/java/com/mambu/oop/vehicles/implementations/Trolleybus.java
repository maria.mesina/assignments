package com.mambu.oop.vehicles.implementations;

import com.mambu.oop.vehicles.interfaces.Rechargeable;

public class Trolleybus extends Bus implements Rechargeable {

    public Trolleybus(String owner, int maxSpeed, String route){
        super(owner, maxSpeed, route);
    }

    @Override
    public void accelerate() {
        System.out.println("Bus accelerates");
    }

    @Override
    public void brake() {
        System.out.println("Bus brakes");
    }

    @Override
    public void recharge() {
        System.out.println("Trolleybus is being charged");
    }

    @Override
    public void changeBatteries() {
        System.out.println("Changed batteries on trolleybus");
    }
}
