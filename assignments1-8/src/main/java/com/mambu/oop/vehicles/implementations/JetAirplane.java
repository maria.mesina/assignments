package com.mambu.oop.vehicles.implementations;

public class JetAirplane extends Airplane{
    private String jetEngine;

    public JetAirplane(String owner, int maxSpeed, int noWings, String jetEngine) {
        super(owner, maxSpeed, noWings);
        this.jetEngine = jetEngine;
    }

    public void setJetEngine(String jetEngine) {
        this.jetEngine = jetEngine;
    }

    public String getJetEngine() {
        return jetEngine;
    }

    @Override
    public void accelerate() {
        System.out.println("Jet Airplane accelerates");
    }

    @Override
    public void brake() {
        System.out.println("Jet Airplane brakes");
    }

    @Override
    public void land(){
        System.out.println("JetAirplane landed");
    }

    @Override
    public void fly(){
        System.out.println("JetAirplane flies");
    }
}
