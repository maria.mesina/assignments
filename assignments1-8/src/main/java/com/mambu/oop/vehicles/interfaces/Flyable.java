package com.mambu.oop.vehicles.interfaces;

public interface Flyable {
    void land();
    void fly();
}
