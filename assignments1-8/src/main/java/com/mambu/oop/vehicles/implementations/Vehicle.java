package com.mambu.oop.vehicles.implementations;

public class Vehicle{
    private String owner;
    private int maxSpeed;

    public Vehicle(String owner, int maxSpeed){
        this.owner = owner;
        this.maxSpeed = maxSpeed;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public void setMaxSpeed(int maxSpeed) {
        this.maxSpeed = maxSpeed;
    }

    public String getOwner() {
        return owner;
    }

    public int getMaxSpeed() {
        return maxSpeed;
    }

    public void accelerate() {
        System.out.println("Vehicle accelerates");
    }

    public void brake() {
        System.out.println("Vehicle brakes");
    }
}
