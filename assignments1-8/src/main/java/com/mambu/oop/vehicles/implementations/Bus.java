package com.mambu.oop.vehicles.implementations;

public class Bus extends Vehicle{
    private String route;

    public Bus(String owner, int maxSpeed, String route){
        super(owner, maxSpeed);
        this.route = route;
    }

    public void setRoute(String route) {
        this.route = route;
    }

    public String getRoute() {
        return route;
    }

    @Override
    public void accelerate() {
        System.out.println("Bus accelerates");
    }

    @Override
    public void brake() {
        System.out.println("Bus brakes");
    }
}
