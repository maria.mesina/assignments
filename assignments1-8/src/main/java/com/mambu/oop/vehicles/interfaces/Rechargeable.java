package com.mambu.oop.vehicles.interfaces;

public interface Rechargeable {
    void recharge();
    void changeBatteries();
}
