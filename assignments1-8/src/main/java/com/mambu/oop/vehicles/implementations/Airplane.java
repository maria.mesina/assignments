package com.mambu.oop.vehicles.implementations;

import com.mambu.oop.vehicles.interfaces.Flyable;

public class Airplane extends Vehicle implements Flyable {
    private int noWings;

    public Airplane(String owner, int maxSpeed, int noWings){
        super(owner, maxSpeed);
        this.noWings = noWings;
    }

    public void setNoWings(int noWings) {
        this.noWings = noWings;
    }

    public int getNoWings() {
        return noWings;
    }

    @Override
    public void accelerate() {
        System.out.println("Plane accelerates");
    }

    @Override
    public void brake() {
        System.out.println("Plane brakes");
    }

    @Override
    public void land() {
        System.out.println("Airplane landed");
    }

    @Override
    public void fly() {
        System.out.println("Airplane flies");
    }
}
