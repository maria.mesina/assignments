package com.mambu.oop.vehicles.implementations;

import com.mambu.oop.vehicles.interfaces.Rechargeable;

public class ElectricCar extends Car implements Rechargeable {
    private int batteryCapacity;

    public ElectricCar(String owner, int maxSpeed, int noWheels, int batteryCapacity){
        super(owner, maxSpeed, noWheels);
        this.batteryCapacity = batteryCapacity;
    }

    public void setBatteryCapacity(int batteryCapacity) {
        this.batteryCapacity = batteryCapacity;
    }

    public int getBatteryCapacity() {
        return batteryCapacity;
    }

    @Override
    public void accelerate() {
        System.out.println("Electric car accelerates");
    }

    @Override
    public void brake() {
        System.out.println("Electric car brakes");
    }

    @Override
    public void recharge() {
        System.out.println("Electric car is charging");
    }

    @Override
    public void changeBatteries() {
        System.out.println("Changed batteries on electric car");
    }
}
