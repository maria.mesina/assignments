package com.mambu.oop.databases.interfaces;

import com.mambu.oop.databases.model.Entity;

public interface DatabaseOperations {
    void createEntity(Entity entity);

    Entity readEntity(long id);

    void updateEntity(Entity entity);

    void deleteEntity(long id);
}
