package com.mambu.oop.databases.implementations;

import com.mambu.oop.databases.interfaces.DatabaseOperations;
import com.mambu.oop.databases.model.Entity;

public class MongoDbOperations implements DatabaseOperations {
    @Override
    public void createEntity(Entity entity) {
        System.out.println("Created entity " + entity.getId() + " in MongoDB");
    }

    @Override
    public Entity readEntity(long id) {
        System.out.println("Read entity " + id + " from MongoDB");
        Entity entity = new Entity();
        entity.setId(id);
        return entity;
    }

    @Override
    public void updateEntity(Entity entity) {
        System.out.println("Updated entity " + entity.getId() + " in MongoDB");
    }

    @Override
    public void deleteEntity(long id) {
        System.out.println("Deleted entity " + id + " from MongoDB");
    }
}
