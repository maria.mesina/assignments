package com.mambu.oop.databases.model;

public class Entity {
    private long id;
    private String property;

    public void setId(long id) {
        this.id = id;
    }

    public void setProperty(String property) {
        this.property = property;
    }

    public long getId() {
        return id;
    }

    public String getProperty() {
        return property;
    }
}
