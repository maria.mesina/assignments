package com.mambu.cleancode.openClosed.good;

public class MovableObject extends GameObject{

    public MovableObject(int x, int y){
        super(x, y);
    }

    public void updatePosition(int offsetX, int offsetY){
        this.x += offsetX;
        this.y += offsetY;
    }

    public void moveTo(int x, int y){
        this.x = x;
        this.y = y;
    }
}
