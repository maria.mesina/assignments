package com.mambu.cleancode.openClosed.bad;

public class GameObject {
    int x;
    int y;

    public void updatePosition(int x, int y){
        this.x += x;
        this.y += y;
    }

    public void render(){

    }
}
