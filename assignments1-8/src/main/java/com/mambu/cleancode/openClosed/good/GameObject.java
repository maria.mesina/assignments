package com.mambu.cleancode.openClosed.good;

public class GameObject {
    protected int x;
    protected int y;

    public GameObject(int x, int y){
        this.x = x;
        this.y = y;
    }

    public void render(){

    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }
}
