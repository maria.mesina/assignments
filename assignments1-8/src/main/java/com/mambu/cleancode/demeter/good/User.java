package com.mambu.cleancode.demeter.good;

import java.util.ArrayList;
import java.util.List;

public class User {
    private List<Transaction> transactions = new ArrayList<>();

    public List<Transaction> getTransactions() {
        return transactions;
    }

    public void addTransaction(Transaction transactionToAdd){
        transactions.add(transactionToAdd);
    }

    public double getTotalAmountSpent(){
        double totalAmountSpent = 0.0;
        for(Transaction t: transactions){
            totalAmountSpent += t.getAmount();
        }

        return totalAmountSpent;
    }
}
