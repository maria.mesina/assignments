package com.mambu.cleancode.demeter.good;

public class Discount {
    public double calculateDiscountPriceByTotalAmountSpent(Transaction transaction, User user){
        double totalSpentAmount = user.getTotalAmountSpent();

        double discountRate;
        if(totalSpentAmount<500){
            discountRate = 0.05;
        } else{
            discountRate = 0.1;
        }

        return transaction.getAmount() * (1-discountRate);
    }
}