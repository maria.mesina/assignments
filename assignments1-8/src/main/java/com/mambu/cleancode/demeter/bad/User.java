package com.mambu.cleancode.demeter.bad;

import java.util.ArrayList;
import java.util.List;

public class User {
    private List<Transaction> transactions = new ArrayList<>();

    public List<Transaction> getTransactions() {
        return transactions;
    }

    public void addTransaction(Transaction transactionToAdd){
        transactions.add(transactionToAdd);
    }
}
