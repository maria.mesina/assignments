package com.mambu.cleancode.demeter.bad;

public class Product {
    private int id;
    private String name;
    private double price;
    private String category;

    public Product() {}

    public Product(Product other){
        this.id = other.id;
        this.name = other.name;
        this.price = other.price;
        this.category = other.category;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    @Override
    public String toString() {
        return "Product{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", price=" + price +
                ", category='" + category + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (o == this)
            return true;
        if (!(o instanceof Product))
            return false;
        Product other = (Product) o;

        return (this.id == other.id) && this.name.equals(other.name) &&
                (this.category == other.category) && (this.price == other.price);
    }
}
