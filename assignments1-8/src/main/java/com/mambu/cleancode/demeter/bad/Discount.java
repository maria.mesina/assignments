package com.mambu.cleancode.demeter.bad;

import java.util.stream.DoubleStream;

public class Discount {
    public double calculateDiscountPriceByTotalSpentAmount(Transaction transaction, User user){
        double totalSpentAmount =
                user.getTransactions().stream().
                        flatMapToDouble(t-> t.getProducts().stream().
                                flatMapToDouble(p-> DoubleStream.of(p.getPrice())))
                .sum();

        double discountRate;
        if(totalSpentAmount<500){
            discountRate = 0.05;
        } else{
            discountRate = 0.1;
        }

        return transaction.getProducts().stream()
                .mapToDouble(p->p.getPrice() * (1-discountRate)).sum();
    }
}