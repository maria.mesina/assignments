package com.mambu.cleancode.demeter.good;

import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;

public class Transaction{
    private List<Product> products = new LinkedList<>();
    private LocalDate date;

    public List<Product> getProducts() {
        return products;
    }

    public void setProducts(List<Product> products) {
        this.products = products;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public void addProduct(Product product){
        this.products.add(product);
    }

    public double getAmount(){
        double sum = 0;
        for(int i=0; i<products.size(); ++i){
            sum += products.get(i).getPrice();
        }

        return sum;
    }
}
