package com.mambu.cleancode.dry.good;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class ArrayUtils <T extends Comparable<T>>{
    public List<T> getMaxNElements(List<T> list, int n) {
        List<T> sortedList = new ArrayList<>(list);
        sortedList.sort(Collections.reverseOrder());
        return getFirstNElements(sortedList, n);
    }

    public List<T> getMinNElements(List<T> list, int n){
        List<T> sortedList = new ArrayList<>(list);
        Collections.sort(sortedList);
        return getFirstNElements(sortedList, n);
    }

    private List<T> getFirstNElements(List<T> list, int n){
        return list.stream().limit(n).collect(Collectors.toList());
    }
}