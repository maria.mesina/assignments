package com.mambu.cleancode.dry.bad;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class ArrayUtils <T extends Comparable<T>>{
    List<T> getMaxNElements(List<T> list, int n){
        List<T> sortedList = new LinkedList<>(list);
        boolean swaps = true;

        for (int i=0; i<sortedList.size() && swaps; ++i){
            swaps = false;
            for(int j=0; j<sortedList.size()-i-1; ++j){
                if (sortedList.get(j).compareTo(sortedList.get(j+1)) < 0){
                    swaps = true;
                    T temp = sortedList.get(j);
                    sortedList.set(j, sortedList.get(j+1));
                    sortedList.set(j+1, temp);
                }
            }
        }

        List<T> maxNElements = new ArrayList<>();
        for(int i=0; i<n && i<sortedList.size(); ++i){
            maxNElements.add(sortedList.get(i));
        }

        return maxNElements;
    }

    List<T> getMinNElements(List<T> list, int n){
        List<T> sortedList = new LinkedList<>(list);
        boolean swaps = true;

        for (int i=0; i<sortedList.size() && swaps; ++i){
            swaps = false;
            for(int j=0; j<sortedList.size()-i-1; ++j){
                if (sortedList.get(j).compareTo(sortedList.get(j+1)) > 0){
                    swaps = true;
                    T temp = sortedList.get(j);
                    sortedList.set(j, sortedList.get(j+1));
                    sortedList.set(j+1, temp);
                }
            }
        }

        List<T> maxNElements = new ArrayList<>();
        for(int i=0; i<n && i<sortedList.size(); ++i){
            maxNElements.add(sortedList.get(i));
        }

        return maxNElements;
    }
}
