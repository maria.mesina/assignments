package com.mambu.cleancode.interfaceSegregation.bad;

import java.io.File;
import java.util.HashMap;

public interface Parser {
    HashMap<Object, Object> parse(String jsonString);
    boolean validate(String jsonString);
    String toFormat(Object object);

    String readFromFile(File file);
    String readFromWeb(String url);
}
