package com.mambu.cleancode.interfaceSegregation.good;

import java.io.File;
import java.io.IOException;

public interface FileReader {
    String readFromFile(File file) throws IOException;
}
