package com.mambu.cleancode.interfaceSegregation.good;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mambu.dataflow.serialization.InvalidInputException;
import com.mambu.dataflow.serialization.model.User;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.stream.Collectors;

public class JsonParser implements Parser, FileReader {
    private ObjectMapper objectMapper = new ObjectMapper();

    @Override
    public HashMap<Object, Object> parse(String jsonString) {
        try{
            return objectMapper.readValue(jsonString, new TypeReference<>() {
            });
        } catch (JsonProcessingException e){
            throw new InvalidInputException("Provided input is not a valid JSON");
        }
    }

    @Override
    public boolean validate(String jsonString) {
        try{
            objectMapper.readValue(jsonString, new TypeReference<>() {});
            return true;
        } catch (JsonProcessingException e){
            return false;
        }
    }

    @Override
    public String toFormat(Object object) {
        try{
            return objectMapper.writeValueAsString(object);
        } catch(JsonProcessingException e){
            throw new InvalidInputException("Can't convert to JSON provided input");
        }
    }

    @Override
    public String readFromFile(File file) throws IOException {
        java.io.FileReader fileReader = new java.io.FileReader(file);
        BufferedReader bufferedReader = new BufferedReader(fileReader);
        String line;
        StringBuilder text = new StringBuilder();
        while((line = bufferedReader.readLine())!=null){
            text.append(line).append("\n");
        }

        return text.toString();
    }
}
