package com.mambu.cleancode.interfaceSegregation.good;

import com.mambu.dataflow.serialization.InvalidInputException;

import java.io.File;
import java.util.HashMap;

public interface Parser {
    HashMap<Object, Object> parse(String jsonString) throws InvalidInputException;
    boolean validate(String jsonString) throws InvalidInputException;
    String toFormat(Object object);
}
