package com.mambu.cleancode.interfaceSegregation.good;

public interface WebReader {
    String readFromWeb(String url);
}
