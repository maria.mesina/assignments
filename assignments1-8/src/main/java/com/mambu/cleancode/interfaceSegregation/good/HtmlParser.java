package com.mambu.cleancode.interfaceSegregation.good;

import java.util.HashMap;

public class HtmlParser implements Parser, WebReader {

    @Override
    public HashMap<Object, Object> parse(String jsonString) {
        return null;
    }

    @Override
    public boolean validate(String jsonString) {
        return false;
    }

    @Override
    public String toFormat(Object object) {
        return null;
    }

    @Override
    public String readFromWeb(String url) {
        return "<h1>test</h1>";
    }
}
