package com.mambu.cleancode.interfaceSegregation.bad;

import java.io.File;
import java.util.HashMap;

public class JsonParser implements Parser{
    @Override
    public HashMap<Object, Object> parse(String jsonString) {
        return null;
    }

    @Override
    public boolean validate(String jsonString) {
        return false;
    }

    @Override
    public String toFormat(Object object) {
        return null;
    }

    @Override
    public String readFromFile(File file) {
        return null;
    }

    @Override
    public String readFromWeb(String url) {
        throw new UnsupportedOperationException("Operation not supported");
    }
}
