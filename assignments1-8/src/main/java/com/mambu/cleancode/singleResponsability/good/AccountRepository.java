package com.mambu.cleancode.singleResponsability.good;

import com.mambu.cleancode.Account;

import java.util.LinkedList;
import java.util.List;

public class AccountRepository {
    private static final List<Account> accounts = new LinkedList<>();
    private static long nextId = 1;

    public Account addAccount(Account accountToAdd){
        accountToAdd.setId(nextId);
        accounts.add(accountToAdd);
        nextId++;

        return accountToAdd;
    }

    public boolean deleteAccount(Account accountToDelete){
        return accounts.remove(accountToDelete);
    }

    public Account findAccountById(long accountId){
        return accounts.stream().filter(account -> account.getId() == accountId)
                .findAny().get();
    }
}
