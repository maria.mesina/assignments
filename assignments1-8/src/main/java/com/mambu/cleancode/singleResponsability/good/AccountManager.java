package com.mambu.cleancode.singleResponsability.good;

import com.mambu.cleancode.Account;

import java.util.NoSuchElementException;

public class AccountManager {
    private final AccountRepository accountRepository = new AccountRepository();
    private final CredentialsManager credentialsManager = new CredentialsManager();
    private final HashingAlgorithm hashingAlgorithm = new HashingAlgorithm();

    public boolean register(String email, String password){
        if(!credentialsManager.validateEmail(email)){
            throw new IllegalArgumentException("Invalid email format.");
        }
        if(!credentialsManager.validatePassword(password)){
            throw new IllegalArgumentException("Invalid password format! Please make sure your password is at least " +
                    "8 characters long and contains at least one uppercase letter and one digit.");
        }

        Account newAccount = new Account(email, hashingAlgorithm.calculateHash(password));
        accountRepository.addAccount(newAccount);
        return true;
    }

    public boolean deleteAccount(long accountId){
        Account accountToDelete;
        try {
            accountToDelete = accountRepository.findAccountById(accountId);
        } catch(NoSuchElementException e){
            return false;
        }
        return accountRepository.deleteAccount(accountToDelete);
    }
}
