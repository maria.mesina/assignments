package com.mambu.cleancode.singleResponsability.good;

import java.util.regex.Pattern;

public class CredentialsManager {
    public boolean validateEmail(String email){
        String regex = "^[A-Za-z0-9+_.-]+@(.+)[.](.+)$";
        Pattern pattern = Pattern.compile(regex);

        return pattern.matcher(email).matches();
    }

    public boolean validatePassword(String password){
        if(password.length() < 8){
            return false;
        }
        boolean digitPresent = false;
        boolean uppercasePresent = false;
        for(char character: password.toCharArray()){
            if(character >= '0' && character <= '9'){
                digitPresent = true;
            }
            if(character >= 'A' && character <= 'Z'){
                uppercasePresent = true;
            }
        }

        return digitPresent && uppercasePresent;
    }

}
