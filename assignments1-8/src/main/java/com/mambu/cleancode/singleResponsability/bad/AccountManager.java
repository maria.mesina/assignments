package com.mambu.cleancode.singleResponsability.bad;

import com.mambu.cleancode.Account;

import java.util.ArrayList;
import java.util.List;

public class AccountManager {
    List<Account> accounts = new ArrayList<>();
    private static long nextId = 1;

    public Account register(String email, String password){
        if(!validateEmail(email)){
            throw new IllegalArgumentException("Invalid email format");
        }
        if(!validatePassword(password)){
            throw new IllegalArgumentException("Invalid password format! Please make sure yout password contains " +
                    "at least one uppercase letter and one digit");
        }

        Account newAccount = new Account(email, calculateHash(password));
        newAccount.setId(nextId);
        accounts.add(newAccount);

        nextId++;
        return newAccount;
    }

    public void deleteAccount(long accountId){
        Account accountToDelete = accounts.stream()
                .filter(account -> account.getId() == accountId)
                .findAny().orElse(null);

        if(accountToDelete != null){
            accounts.remove(accountToDelete);
        }
    }

    private boolean validateEmail(String email){
        return email.contains("@") && email.contains(".");
    }

    private boolean validatePassword(String password){
        if(password.length() < 8){
            return false;
        }
        boolean digitPresent = false;
        boolean uppercasePresent = false;
        for(char character: password.toCharArray()){
            if(character >= '0' && character <= '9'){
                digitPresent = true;
            }
            if(character >= 'A' && character <= 'Z'){
                uppercasePresent = true;
            }
        }

        return digitPresent && uppercasePresent;
    }

    private String calculateHash(String password){
        return password;
    }
}
