package com.mambu.cleancode.dependencyInversion.good;

import com.mambu.cleancode.Account;

import java.util.List;

public class AccountManager {
    private final AccountRepository accountRepository;

    public AccountManager(AccountRepository accountRepository){
        this.accountRepository = accountRepository;
    }

    public void register(String email, String password){
        String passwordHash = password;
        Account newAccount = new Account(email, passwordHash);
        accountRepository.addAccount(newAccount);
    }

    public void displayAccounts(){
        List<Account> accounts = accountRepository.getAccountsAsList();
        accounts.forEach(System.out::println);
    }

    public AccountRepository getAccountRepository() {
        return accountRepository;
    }
}
