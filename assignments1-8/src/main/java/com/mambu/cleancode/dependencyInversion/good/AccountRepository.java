package com.mambu.cleancode.dependencyInversion.good;

import com.mambu.cleancode.Account;

import java.util.List;

public interface AccountRepository {
    Account addAccount(Account newAccount);
    boolean deleteAccount(Account account);
    List<Account> getAccountsAsList();
}
