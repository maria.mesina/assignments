package com.mambu.cleancode.dependencyInversion.bad;

import com.mambu.cleancode.Account;

import java.util.List;

public class AccountManager {
    private AccountRepository accountRepository = new AccountRepository();

    public void register(String email, String password){
        String passwordHash = password;
        Account newAccount = new Account(email, passwordHash);
        accountRepository.addAccount(newAccount);
    }

    public void listAccounts(){
        List<Account> accounts = accountRepository.getAccountsAsList();
        accounts.forEach(System.out::println);
    }
}
