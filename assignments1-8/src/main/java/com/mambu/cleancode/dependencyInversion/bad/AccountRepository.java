package com.mambu.cleancode.dependencyInversion.bad;

import com.mambu.cleancode.Account;

import java.util.LinkedList;
import java.util.List;

public class AccountRepository {
    private static final List<Account> accounts = new LinkedList<>();
    private static long nextId = 1;

    public Account addAccount(Account accountToAdd){
        accountToAdd.setId(nextId);
        accounts.add(accountToAdd);
        nextId++;

        return accountToAdd;
    }

    public void deleteAccount(Account accountToDelete){
        accounts.remove(accountToDelete);
    }

    public List<Account> getAccountsAsList(){
        return accounts;
    }
}
