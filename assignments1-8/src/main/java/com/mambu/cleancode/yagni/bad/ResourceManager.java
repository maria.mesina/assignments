package com.mambu.cleancode.yagni.bad;

import java.util.ArrayList;
import java.util.List;

public class ResourceManager {
    private List<Resource> resources = new ArrayList<>();

    public void addResource(String resourcePath, User requester, AccessType accessType){
        if(requester.getRole() == Role.GUEST){
            throw new UnsupportedOperationException("Please, log in");
        }

        Resource resourceToAdd = new Resource();
        resourceToAdd.setResourcePath(resourcePath);
        resourceToAdd.setOwner(requester);
        resourceToAdd.setAccessType(accessType);
        resources.add(resourceToAdd);
    }

    public Resource getResource(String resourcePath, User requester){
        Resource resource =
                resources.stream().filter(r-> r.getResourcePath().equals(resourcePath)).findFirst().orElse(null);

        if(resource == null){
            throw new IllegalArgumentException("Requested resource does not exist");
        }

        if(resource.getAccessType() == AccessType.PUBLIC ||
            requester.getRole() == Role.ADMIN){
            return resource;
        }

        if(resource.getAccessType() == AccessType.OWNER_GROUP &&
            resource.getOwner().getRole() == requester.getRole()){
            return resource;
        }

        throw new SecurityException("Forbidden!");
    }

    public boolean delete(Resource resourceToDelete, User requester){
        if(requester.getRole() != Role.ADMIN &&
            resourceToDelete.getOwner() != requester){
            throw new SecurityException("Forbidden!");
        }

        return resources.remove(resourceToDelete);
    }
}
