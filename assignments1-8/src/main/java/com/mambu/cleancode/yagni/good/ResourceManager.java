package com.mambu.cleancode.yagni.good;

import com.mambu.dataflow.serialization.InvalidInputException;

import java.util.ArrayList;
import java.util.List;

public class ResourceManager {
    private List<Resource> resources = new ArrayList<>();

    public List<Resource> getResources() {
        return resources;
    }

    public Resource addResource(String resourcePath){
        if (resourcePath == null){
            throw new NullPointerException("Resource path was null");
        }

        Resource resourceToAdd = new Resource(resourcePath);
        resources.add(resourceToAdd);

        return resourceToAdd;
    }

    public Resource getResource(String resourcePath){
        Resource resource =
                resources.stream().filter(r-> r.getResourcePath().equals(resourcePath)).findFirst().get();

        return resource;
    }

    public boolean deleteResource(Resource resourceToDelete){
        return resources.remove(resourceToDelete);
    }
}
