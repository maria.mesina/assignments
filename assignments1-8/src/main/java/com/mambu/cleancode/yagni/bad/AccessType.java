package com.mambu.cleancode.yagni.bad;

public enum AccessType {
    PUBLIC,
    OWNER_GROUP,
    PRIVATE
}
