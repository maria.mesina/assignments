package com.mambu.cleancode.yagni.bad;

public class User {
    private String passwordHash;
    private String email;

    private Role role;

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }
}
