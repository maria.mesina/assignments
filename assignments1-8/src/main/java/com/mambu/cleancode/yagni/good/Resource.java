package com.mambu.cleancode.yagni.good;

public class Resource {
    private String resourcePath;

    public Resource(String resourcePath){
        this.resourcePath = resourcePath;
    }

    public String getResourcePath() {
        return resourcePath;
    }

    public void setResourcePath(String resourcePath) {
        this.resourcePath = resourcePath;
    }
}
