package com.mambu.cleancode.yagni.bad;

public enum Role {
    GUEST,
    USER,
    ADMIN
}
