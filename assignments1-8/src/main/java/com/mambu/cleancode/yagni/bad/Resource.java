package com.mambu.cleancode.yagni.bad;

import com.mambu.cleancode.yagni.bad.AccessType;
import com.mambu.cleancode.yagni.bad.User;

public class Resource {
    private User owner;
    private String resourcePath;
    private AccessType accessType;

    public User getOwner() {
        return owner;
    }

    public void setOwner(User owner) {
        this.owner = owner;
    }

    public String getResourcePath() {
        return resourcePath;
    }

    public void setResourcePath(String resourcePath) {
        this.resourcePath = resourcePath;
    }

    public AccessType getAccessType() {
        return accessType;
    }

    public void setAccessType(AccessType accessType) {
        this.accessType = accessType;
    }
}
