package com.mambu.cleancode.liskov;

import com.mambu.cleancode.liskov.good.CustomSerializer;

import java.io.IOException;
import java.util.List;

public class ArrayUtils {
    private final CustomSerializer customSerializer;

    public ArrayUtils(CustomSerializer serializer){
        this.customSerializer = serializer;
    }

    public CustomSerializer getCustomSerializer() {
        return customSerializer;
    }

    public void writeIntsToFile(String filename, List<Integer> list) throws IOException {
        customSerializer.writeIntegersToFile(filename, list);
    }

    public List<Integer> readIntsFromFile(String filename) throws IOException {
        return customSerializer.readIntegersFromFile(filename);
    }
}
