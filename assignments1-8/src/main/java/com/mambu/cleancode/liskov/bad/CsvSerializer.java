package com.mambu.cleancode.liskov.bad;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

public class CsvSerializer extends CustomSerializer {
    @Override
    public void writeIntsToFile(File file, List<Integer> list) throws IOException {
        if(!(file.getName().endsWith(".csv"))){
            throw new IllegalArgumentException("Provided file is not a csv file!");
        }
        String listStringRepresentation = list.stream().map(Object::toString).collect(Collectors.joining(", "));
        writeStringToFile(file, listStringRepresentation);
    }
}
