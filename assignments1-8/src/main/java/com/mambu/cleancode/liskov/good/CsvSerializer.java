package com.mambu.cleancode.liskov.good;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class CsvSerializer extends CustomSerializer {
    @Override
    public void writeIntegersToFile(String filename, List<Integer> list) throws IOException {
        String listStringRepresentation = list.stream().map(Object::toString).collect(Collectors.joining(", "));
        File fileToWrite = new File(filename);
        writeStringToFile(fileToWrite, listStringRepresentation);
    }

    @Override
    public List<Integer> readIntegersFromFile(String filename) throws IOException {
        FileReader fileReader = new FileReader(filename);
        BufferedReader bufferedReader = new BufferedReader(fileReader);
        String line = bufferedReader.readLine();
        String[] stringIntegers = line.split(", ");
        return Arrays.stream(stringIntegers).map(Integer::parseInt).collect(Collectors.toList());
    }

}
