package com.mambu.cleancode.liskov.good;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class CustomSerializer {
    public void writeIntegersToFile(String filename, List<Integer> list) throws IOException {
        String listStringRepresentation = list.stream().map(Object::toString).collect(Collectors.joining("\n"));
        File fileToWrite = new File(filename);
        writeStringToFile(fileToWrite, listStringRepresentation);
    }

    public List<Integer> readIntegersFromFile(String filename) throws IOException {
        FileReader fileReader = new FileReader(filename);
        BufferedReader bufferedReader = new BufferedReader(fileReader);
        String line = "";
        List<Integer> ints = new ArrayList<>();
        while((line = bufferedReader.readLine())!= null){
            ints.add(Integer.parseInt(line));
        }

        return ints;
    }

    protected void writeStringToFile(File file, String string) throws IOException {
        FileWriter fileWriter = new FileWriter(file);
        BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
        bufferedWriter.write(string);

        bufferedWriter.flush();
        bufferedWriter.close();
        fileWriter.close();
    }
}
