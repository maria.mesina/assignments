package com.mambu.cleancode.liskov.bad;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class CustomSerializer {
    public void writeIntsToFile(File file, List<Integer> list) throws IOException {
        String listStringRepresentation = list.stream().map(Object::toString).collect(Collectors.joining("\n"));
        writeStringToFile(file, listStringRepresentation);
    }

    public List<Integer> readIntsFromFile(File file) throws IOException {
        FileReader fileReader = new FileReader(file);
        BufferedReader bufferedReader = new BufferedReader(fileReader);
        String line = "";
        List<Integer> ints = new ArrayList<>();
        while((line = bufferedReader.readLine())!= null){
            ints.add(Integer.parseInt(line));
        }

        return ints;
    }

    protected void writeStringToFile(File file, String string) throws IOException {
        FileWriter fileWriter = new FileWriter(file);
        BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
        bufferedWriter.write(string);

        bufferedWriter.flush();
        bufferedWriter.close();
        fileWriter.close();
    }
}
