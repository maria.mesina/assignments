package com.mambu.threads.forkjoin;

import java.util.List;
import java.util.concurrent.ForkJoinPool;

public class ForkJoinExecutor {
    public Integer findElementInList(List<Integer> list, int target){
        ForkJoinPool pool = new ForkJoinPool();
        FindElementProcessor findElement = new FindElementProcessor(list, 0, list.size(), target);
        pool.submit(findElement);
        return findElement.join();
    }

    public Long countWordInText(String text, String word){
        if(word.isEmpty()){
            return 0L;
        }

        if(word.contains(" ")){
            throw new IllegalArgumentException("Please provide only one word. Ward was " + word);
        }

        ForkJoinPool pool = new ForkJoinPool();
        text = text.toLowerCase();
        word = word.toLowerCase();
        String[] tokens = text.split("\\W+");

        CountWordProcessor countWord = new CountWordProcessor(tokens, 0, tokens.length, word);
        pool.submit(countWord);
        return countWord.join();
    }
}
