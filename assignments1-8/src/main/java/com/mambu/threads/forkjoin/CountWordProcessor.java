package com.mambu.threads.forkjoin;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.RecursiveTask;

import static java.lang.Math.min;

public class CountWordProcessor extends RecursiveTask<Long> {
    private String[] tokens;
    private String target;
    private int startIndex;
    private int endIndex;

    public CountWordProcessor(String[] tokens, int startIndex, int endIndex, String target){
        this.tokens = tokens;
        this.target = target;
        this.startIndex = startIndex;
        this.endIndex = endIndex;
    }


    @Override
    protected Long compute() {
        List<CountWordProcessor> tasks = new ArrayList<>();
        if(endIndex - startIndex - 1 > 100) {
            for(int i=0; i<(endIndex-startIndex)/100+1; ++i){
                CountWordProcessor subtask = new
                        CountWordProcessor(tokens, i*100, min(tokens.length, (i+1)*100), target);
                subtask.fork();
                tasks.add(subtask);
            }
            return combineResult(tasks);
        }
        else{
            return Arrays.stream(tokens, startIndex, endIndex).filter(w -> w.equals(target)).count();
        }
    }

    private Long combineResult(List<CountWordProcessor> tasks){
        Long result = 0L;
        for(int i=0; i<tasks.size(); ++i){
            result += tasks.get(i).join();
        }

        return result;
    }
}
