package com.mambu.threads.forkjoin;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.RecursiveTask;

import static java.lang.Math.min;

public class FindElementProcessor extends RecursiveTask<Integer> {
    private List<Integer> list;
    private int startIndex;
    private int endIndex;
    private int target;

    public FindElementProcessor(List<Integer> list, int startIndex, int endIndex, int target){
        this.list = list;
        this.startIndex = startIndex;
        this.endIndex = endIndex;
        this.target = target;
    }


    @Override
    public Integer compute() {
        if(endIndex - startIndex - 1 > 100){
            List<FindElementProcessor> tasks = new ArrayList<>();
            for(int i=0; i<(endIndex-startIndex)/100+1; ++i){
                FindElementProcessor subtask = new FindElementProcessor(this.list, startIndex+(i*100),
                        min(list.size(), startIndex+(i+1)*100), this.target);
                subtask.fork();
                tasks.add(subtask);
            }
            return combineResults(tasks);
        }
        else {
            int result = -1;
            for (int i = startIndex; i < endIndex && result == -1; ++i) {
                if (list.get(i) == this.target) {
                    result = i;
                }
            }
            return result;
        }
    }

    private Integer combineResults(List<FindElementProcessor> tasks){
        int result = Integer.MAX_VALUE;
        for(int i=0; i<tasks.size(); ++i){
            int newValue = tasks.get(i).join();
            if(newValue != -1 && newValue < result) {
                result = newValue;
            }
        }
        return result != Integer.MAX_VALUE? result:-1;
    }
}
