package com.mambu.threads.sync_collections;

import com.mambu.generics.collections.MyCollection;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class SynchronizedQueue<T> implements MyCollection<T> {
    private int capacity = 32;
    private int resize = 2;
    private T[] container;
    private int head = 0;
    private int tail = 0;

    public SynchronizedQueue() {
        this.container = (T[]) new Object[this.capacity];
    }

    public SynchronizedQueue(int capacity) {
        if (capacity <= 0) {
            throw new IllegalArgumentException("Illegal Argument! Size was " + capacity);
        }

        this.capacity = capacity;
        this.container = (T[]) new Object[this.capacity];
    }

    @Override
    public synchronized void add(T element) {
        if (head == capacity) {
            if (tail == 0) {
                resize();
            } else {
                head = 0;
            }
        }
        container[head] = element;
        head++;
    }

    @Override
    public synchronized T remove() {
        if (isEmpty()) {
            throw new IndexOutOfBoundsException("Queue was empty");
        }

        if (tail == capacity) {
            tail = 0;
        }

        T temp = container[tail];
        tail++;

        return temp;
    }

    @Override
    public synchronized T get() {
        if (isEmpty()) {
            throw new IndexOutOfBoundsException("Queue was empty");
        }

        if (tail == capacity) {
            tail = 0;
        }

        return container[tail];
    }

    @Override
    public synchronized void list() {
        if (head > tail) {
            for (int i = tail; i < head; ++i) {
                System.out.println(container[i]);
            }
        } else {
            for (int i = tail; i < capacity; ++i) {
                System.out.println(container[i]);
            }
            for (int i = 0; i < head; ++i) {
                System.out.println(container[i]);
            }
        }
    }

    @Override
    public boolean isEmpty() {
        return (head == tail);
    }

    public synchronized List<T> getALl(){
        List<T> result = new ArrayList<>();
        if(head>tail){
            result.addAll(Arrays.asList(container).subList(tail, head));
        }
        else{
            result.addAll(Arrays.asList(container).subList(tail, capacity));
            result.addAll(Arrays.asList(container).subList(0, head));
        }

        return result;
    }

    private void resize() {
        capacity = resize * capacity;
        container = Arrays.copyOf(container, capacity);
    }
}