package com.mambu.threads.sync_collections;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.SynchronousQueue;

import static java.lang.Math.min;

public class ProcessWords {
    private String text;
    SynchronizedQueue<String> words = new SynchronizedQueue<>();
    List<String> result = new ArrayList<>();

    public ProcessWords(String text) {
        this.text = text;
    }

    public Long computeAllCapitalCase(){
        String[] tokens = text.split("\\W+");
        ExecutorService executor = Executors.newFixedThreadPool(tokens.length/100+1);
        long startTime = System.nanoTime();
        for(int i=0; i<tokens.length/100 + 1; ++i){
            int finalI = i;
            executor.submit(()->{
                Arrays.stream(tokens, finalI*100, min(tokens.length, (finalI+1)*100))
                        .filter(w -> w.toLowerCase().charAt(0) != w.charAt(0))
                        .forEach(w->words.add(w));
            });
        }
        long endTime = System.nanoTime();
        executor.shutdown();
        return endTime - startTime;
    }

    public List<String> getResult(){
        return words.getALl();
    }
}
