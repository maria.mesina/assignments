package com.mambu.threads.sorting;

import java.util.List;
import java.util.Random;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class SortingUtils {
    List<Integer> list;
    ExecutorService executor;

    public SortingUtils(){}
    public SortingUtils(List<Integer> list, ExecutorService executor){
        this.list = list;
        this.executor = executor;
    }

    public List<Integer> getList() {
        return list;
    }

    public void setList(List<Integer> list) {
        this.list = list;
    }

    public ExecutorService getExecutor() {
        return executor;
    }

    public void setExecutor(ExecutorService executor) {
        this.executor = executor;
    }

    public List<Integer> generateRandomArray(int size){
        return Stream.generate(new Random()::nextInt).limit(size).collect(Collectors.toList());
    }

    public Long sortWithBubbleSort() throws ExecutionException, InterruptedException {
        BubbleSort bs = new BubbleSort();
        bs.setList(this.list);
        Future<Long> executionTime = executor.submit(bs);
        executor.shutdown();
        return executionTime.get();
    }

    public Long sortWithQuickSort() throws ExecutionException, InterruptedException {
        QuickSort qs = new QuickSort();
        qs.setList(this.list);
        Future<Long> executionTime = executor.submit(qs);
        executor.shutdown();
        return executionTime.get();
    }

    public Long sortWithMergeSort() throws ExecutionException, InterruptedException {
        MergeSort ms = new MergeSort();
        ms.setList(this.list);
        Future<Long> executionTime = executor.submit(ms);
        executor.shutdown();
        return executionTime.get();
    }
}
