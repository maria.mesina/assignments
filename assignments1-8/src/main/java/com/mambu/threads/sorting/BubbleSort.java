package com.mambu.threads.sorting;

import java.util.List;
import java.util.concurrent.Callable;

public class BubbleSort implements Callable<Long>{
    private List<Integer> list;

    public List<Integer> getList() {
        return list;
    }

    public void setList(List<Integer> list) {
        this.list = list;
    }

    public BubbleSort(){ }

    public void sort(){
        boolean swaps = true;

        for (int i=0; i<list.size() && swaps; ++i){
            swaps = false;
            for(int j=0; j<list.size()-i-1; ++j){
                if (list.get(j) > list.get(j+1)){
                    swaps = true;
                    int temp = list.get(j);
                    list.set(j, list.get(j+1));
                    list.set(j+1, temp);
                }
            }
        }
    }

    @Override
    public Long call() throws Exception {
        long startTime = System.nanoTime();
        sort();
        long endTime = System.nanoTime();
        return endTime - startTime;
    }
}
