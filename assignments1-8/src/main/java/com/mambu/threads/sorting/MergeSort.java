package com.mambu.threads.sorting;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

public class MergeSort implements Callable<Long> {
    private List<Integer> list;

    public List<Integer> getList() {
        return list;
    }

    public void setList(List<Integer> list) {
        this.list = list;
    }

    public MergeSort(){ }

    private void split(List<Integer> sorted, int l, int r){
        if(l<r){
            int m = (l+r)/2;
            split(sorted, l, m);
            split(sorted, m+1, r);
            merge(sorted, l, m, r);
        }
    }

    private void merge(List<Integer> partiallySorted, int l, int m, int r){
        int leftIndex = l;
        int rightIndex = m+1;
        List<Integer> sorted = new ArrayList<>(r-l+1);
        for(int i=0; i<r-l+1; ++i){
            sorted.add(partiallySorted.get(l+i));
        }

        int i = 0;
        while(rightIndex<=r && leftIndex<=m){
            if(partiallySorted.get(rightIndex) < partiallySorted.get(leftIndex)){
                sorted.set(i, partiallySorted.get(rightIndex));
                ++rightIndex;
            }
            else{
                sorted.set(i, partiallySorted.get(leftIndex));
                ++leftIndex;
            }
            ++i;
        }

        while(leftIndex <= m){
            sorted.set(i, partiallySorted.get(leftIndex));
            ++leftIndex;
            ++i;
        }

        while(rightIndex <= r){
            sorted.set(i, partiallySorted.get(rightIndex));
            ++rightIndex;
            ++i;
        }

        for(i=0; i<r-l+1; ++i){
            partiallySorted.set(l+i, sorted.get(i));
        }
    }

    public void sort(){
        split(this.list, 0, this.list.size()-1);
    }

    @Override
    public Long call() throws Exception {
        long startTime = System.nanoTime();
        sort();
        long endTime = System.nanoTime();
        return endTime - startTime;
    }
}
