package com.mambu.threads.sorting;

import java.util.Collections;
import java.util.List;
import java.util.concurrent.Callable;

public class QuickSort implements Callable<Long> {
    List<Integer> list;

    public QuickSort(){ }

    public List<Integer> getList() {
        return list;
    }

    public void setList(List<Integer> list) {
        this.list = list;
    }

    @Override
    public Long call() throws Exception {
        long startTime = System.nanoTime();
        Collections.sort(list);
        long endTime = System.nanoTime();
        return endTime - startTime;
    }
}
