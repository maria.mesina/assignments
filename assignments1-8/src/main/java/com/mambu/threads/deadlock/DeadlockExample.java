package com.mambu.threads.deadlock;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class DeadlockExample {
    private File source;
    private File destination;
    private FileCopy fileCopy;

    public DeadlockExample(String from, String to){
        source = new File(from);
        destination = new File(to);
        fileCopy = new FileCopy();
    }

    public boolean swapContents() throws InterruptedException{
        ExecutorService executor = Executors.newFixedThreadPool(2);
        List<Callable<Void>> tasks = new ArrayList<>();
        tasks.add(()->fileCopy.copyFile(source, destination));
        tasks.add(()->fileCopy.copyFile(destination, source));

        executor.invokeAll(tasks);
        boolean executed = executor.awaitTermination(2, TimeUnit.SECONDS);
        return executed;
    }
}
