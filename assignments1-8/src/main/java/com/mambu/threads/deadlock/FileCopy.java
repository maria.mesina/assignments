package com.mambu.threads.deadlock;

import java.io.*;

public class FileCopy{
    public FileCopy(){}

    public Void copyFile(File from, File to) throws IOException, InterruptedException {
        String content = "";

        synchronized (from){
            FileReader reader = new FileReader(from);
            BufferedReader bufferedReader = new BufferedReader(reader);

            String line = "";
            StringBuilder builder = new StringBuilder();
            while((line = bufferedReader.readLine()) != null){
                builder.append(line);
                builder.append("\n");
            }
            content = builder.deleteCharAt(builder.length()-1).toString();

            bufferedReader.close();
            reader.close();
            Thread.sleep(1000);
        }

        synchronized (to) {
            FileWriter writer = new FileWriter(to);
            BufferedWriter bufferedWriter = new BufferedWriter(writer);
            bufferedWriter.write(content);

            bufferedWriter.flush();
            bufferedWriter.close();
            writer.close();
            Thread.sleep(1000);
        }

        return null;
    }
}
