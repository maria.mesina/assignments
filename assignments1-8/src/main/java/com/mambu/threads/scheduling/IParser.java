package com.mambu.threads.scheduling;

import java.io.IOException;
import java.util.List;

public interface IParser {
    List<ScheduledTask> parseSchedule(String path) throws IOException;
}
