package com.mambu.threads.scheduling;

import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

public class ScheduledTask implements Comparable<ScheduledTask>{
    private LocalTime time;
    private Runnable function;
    private static final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("HH:mm:ss");

    public LocalTime getTime() {
        return time;
    }

    public void setTime(LocalTime time) {
        this.time = LocalTime.parse(formatter.format(time), formatter);
    }

    public Runnable getFunction() {
        return function;
    }

    public void setFunction(Runnable function) {
        this.function = function;
    }

    @Override
    public int compareTo(ScheduledTask o) {
        return this.time.compareTo(o.time);
    }
}
