package com.mambu.threads.scheduling;

import com.mambu.dataflow.serialization.InvalidInputException;

import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.PriorityQueue;
import java.util.Queue;

public class ScheduledExecutor {
    private Queue<ScheduledTask> taskQueue;
    private Monitor monitor;
    private static final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("HH:mm:ss");

    public ScheduledExecutor(){
        taskQueue = new PriorityQueue<>();
        monitor = new Monitor(taskQueue);
    }

    public void addTask(ScheduledTask task){
        if(LocalTime.now().format(formatter).compareTo(task.getTime().format(formatter)) < 0){
            taskQueue.add(task);
        }
        else{
            throw new InvalidInputException("Task time is earlier than current time!");
        }
    }

    public void start(){
        monitor.start();
    }

    public void shutdown(){
        monitor.active = false;
    }
}
