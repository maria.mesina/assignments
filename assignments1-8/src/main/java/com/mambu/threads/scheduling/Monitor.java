package com.mambu.threads.scheduling;

import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.util.Queue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Monitor extends Thread{
    private Queue<ScheduledTask> taskQueue;
    private ExecutorService executor;
    public boolean active;
    private static final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("HH:mm:ss");

    public Monitor(Queue<ScheduledTask> taskQueue){
        this.taskQueue = taskQueue;
        executor = Executors.newCachedThreadPool();
        active = true;
    }

    public void run(){
        while(active){
            ScheduledTask upcoming = taskQueue.peek();
            LocalTime currentTime = LocalTime.now();

            if(upcoming != null && upcoming.getTime().format(formatter).equals(currentTime.format(formatter))){
                executor.submit(upcoming.getFunction());
                taskQueue.poll();
            }

            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        executor.shutdown();
    }
}
