package com.mambu.threads.scheduling.bad;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.JsonNodeType;
import com.mambu.dataflow.serialization.InvalidInputException;
import com.mambu.threads.scheduling.IParser;
import com.mambu.threads.scheduling.ScheduledTask;

import java.io.File;
import java.io.IOException;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

public class Parser implements IParser {
    private final ObjectMapper objectMapper = new ObjectMapper();

    public List<ScheduledTask> parseSchedule(String path) throws IOException {
        File file = new File(path);
        List<ScheduledTask> tasks = new ArrayList<>();

        JsonNode node = objectMapper.readTree(file);
        if(node.getNodeType() != JsonNodeType.ARRAY){
            throw new InvalidInputException("Invalid Json format");
        }

        for (Iterator<JsonNode> it = node.elements(); it.hasNext(); ) {
            JsonNode c = it.next();

            ScheduledTask task = parseNode(c);
            tasks.add(task);
        }

        return tasks;
    }

    private ScheduledTask parseNode(JsonNode node){
        if(node.getNodeType() != JsonNodeType.OBJECT){
            throw new InvalidInputException("Invalid Json format");
        }

        if(node.get("function") == null || node.get("time") == null){
            throw new NullPointerException("Input does not contain field(s) function or time");
        }

        String functionName = node.get("function").asText();
        String timeString = node.get("time").asText();
        LocalTime time = LocalTime.parse(timeString);
        Runnable function = pickFunction(functionName);

        ScheduledTask task = new ScheduledTask();
        task.setFunction(function);
        task.setTime(time);

        return task;
    }

    private Runnable pickFunction(String functionName){
        return switch (functionName) {
            case "printTime" -> ()->System.out.println(LocalTime.now());
            case "printAnArray" -> ()->new Random().ints(20).forEach(System.out::println);
            case "printHelloWorld" -> ()->System.out.println("HelloWorld");
            default -> throw new NullPointerException("Function " + functionName + " was not recognized");
        };
    }
}
