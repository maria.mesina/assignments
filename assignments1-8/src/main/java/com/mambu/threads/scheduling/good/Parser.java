package com.mambu.threads.scheduling.good;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mambu.threads.scheduling.IParser;
import com.mambu.threads.scheduling.ScheduledTask;

import java.io.File;
import java.io.IOException;
import java.time.LocalTime;
import java.util.*;
import java.util.stream.Collectors;

public class Parser implements IParser {
    private final ObjectMapper objectMapper = new ObjectMapper();

    public List<ScheduledTask> parseSchedule(String path) throws IOException {
        File file = new File(path);
        List<ScheduledTask> tasks;
        tasks = objectMapper.readValue(file, new TypeReference<List<HashMap<String,String>>>() {}).stream()
                .map(object->{
                    ScheduledTask task = new ScheduledTask();
                    task.setTime(LocalTime.parse(object.get("time")));
                    task.setFunction(pickFunction(object.get("function")));
                    return task;
                }).collect(Collectors.toList());

        return tasks;
    }

    private Runnable pickFunction(String functionName){
        return switch (functionName) {
            case "printTime" -> ()->System.out.println(LocalTime.now());
            case "printAnArray" -> ()->new Random().ints(20).forEach(System.out::println);
            case "printHelloWorld" -> ()->System.out.println("HelloWorld");
            default -> throw new NullPointerException("Function " + functionName + " was not recognized");
        };
    }
}
